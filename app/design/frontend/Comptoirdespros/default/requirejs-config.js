var config = {

  
  // Paths defines associations from library name (used to include the library,
  // for example when using "define") and the library file path.
  paths: {
    'slick': 'js/slick.min',
    'attrchange': 'js/attrchange',
  	'jquery-migrate': 'js/jquery-migrate-1.2.1.min',
  	'bootstrap':'js/bootstrap.min',
    'custom-plugins':'js/custom-plugins',
    'tabjs':'js/jquery.tabs',
      'ellipsis':'js/jquery.ellipsis',
      'carouFredSel': 'js/jquery.carouFredSel-5.4.1-packed'
  },

  // Shim: when you're loading your dependencies, requirejs loads them all
  // concurrently. You need to set up a shim to tell requirejs that the library
  // (e.g. a jQuery plugin) depends on another already being loaded (e.g. depends
  // from jQuery).
  // Exports: if the library it's not AMD aware, you need to tell requirejs what 
  // to look to know the script loaded correctly. You can do this with an 
  // "exports" entry in your shim. The value must be a variable defined within
  // the library.
  shim: {
    'slick': {
      deps: ['jquery']
    },
      'attrchange': {
      deps: ['jquery']
    },
  	'bootstrap': {
  		deps: ["jquery"]
  		}, 
  	'jquery-migrate': {
  		deps: ["jquery"]
  		},
      'custom-plugins': {
      deps: ["jquery"]
      },
      'tabjs': {
      deps: ["jquery"]
      },
      'ellipsis': {
          deps: ["jquery"]
      },
      'carouFredSel': {
          deps: ["jquery"]
      }
  },

  deps: [
    "js/custom"
  ]

};
