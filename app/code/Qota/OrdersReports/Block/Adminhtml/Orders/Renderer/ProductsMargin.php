<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsMargin extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductName($row);
    }


    private function getProductName($order){

        $items = $order->getAllItems();
        $shipping_amount = $order->getShippingAmount();
        $final_shipping_price = number_format((float)$shipping_amount, 2, '.', '') ;
        if($items){
            if($order->getSku()){
                foreach($items as $itemId => $_item){
                    if($order->getSku()==$_item->getSku()){
                        if($_item->getStatus()!= 'Refunded')
                        {
                            // return $_item->getPrice()*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered();
                            return ($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered() ;
                        }
                        else
                        {
                            return 0.00;
                        }
                    }
                }
            }else{
                $total_qty = [];   
                $margin = [];   
                    foreach($items as $itemId => $_item){
                        if($_item->getStatus()!= 'Refunded')
                        {
                            $margin[][$itemId]= ( ($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered());
                            //$total_qty[][$itemId]= $_item->getBaseCost()*$_item->getQtyOrdered();
                            //price * qty - base cost * qty + shipping 
                        }
                        else
                        {
                            $margin[][$itemId]= 0.00;
                        }
                    }           
                $c=0;
                $html="";
                    $cc=0;
                    $total =0;
                    foreach($margin as $itm){
                        $final = number_format((float)$itm[$cc], 2, '.', '');
                        $html .= $final.'<br/>'; 
                        $total += $itm[$cc];
                        $cc++;
                    }
                    $finaltotal = $total + $final_shipping_price;
                    $html .= '<hr/>'; 
                    $html .= '<strong>'.number_format((float)$finaltotal, 2, '.', '').'</strong>';
                return $html;
            }
        }

    }

}