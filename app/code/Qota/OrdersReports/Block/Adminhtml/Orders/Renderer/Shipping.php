<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class Shipping extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getShippingAmount($row);
    }


    private function getShippingAmount($order){

        $shipping_price = $order->getShippingAmount() - $order->getShippingRefunded();
          
            $html = number_format((float)$shipping_price, 2, '.', '');
            return $html;
        
    }

}