<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsCost extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductPrice($row);
    }


    private function getProductPrice($order){

        $items = $order->getAllItems();
        if($items){

            $cost = [];  
                foreach($items as $itemId => $_item){ 
                    if($_item->getStatus()!= 'Refunded')
                    {                  
                        $cost[][$itemId]=number_format($_item->getBaseCost(),2);
                    }
                    else
                    {
                        $cost[][$itemId]= 0.00;
                    }
                }
                $html="";
                $total =0;
                    $cc=0;
                    foreach($cost as $itm){
                        $final = number_format((float)$itm[$cc], 2, '.', '');
                        $html .= $final.'<br/>';
                        $total += $final;
                        $cc++;
                    }
                    $html .= '<hr/>'; 
                    $html .= '<strong>'.number_format((float)$total, 2, '.', '').'</strong>';
                return $html;

                
        }
    }
}