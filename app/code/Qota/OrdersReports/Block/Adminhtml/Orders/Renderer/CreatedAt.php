<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class CreatedAt extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getCreatedAt($row);
    }


    private function getCreatedAt($order){

        $date = $order->getCreatedAt();
        //$final_shipping_price = number_format((float)$shipping_price, 2, '.', '');
        if(!empty($date))
        {
            //$html = $shipping_price;
            $html = date("M d, Y, h:i:s", strtotime($date));
            return $html;
        }
        else
        {
            return '';
        }
        
    }

}