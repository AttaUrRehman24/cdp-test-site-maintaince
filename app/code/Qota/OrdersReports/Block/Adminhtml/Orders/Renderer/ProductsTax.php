<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsTax extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductTax($row);
    }


    private function getProductTax($order){

        $html = number_format((float)$order->getTaxAmount() - $order->getBaseTaxRefunded(), 2, '.', '');
        return $html;                
        
    }

}