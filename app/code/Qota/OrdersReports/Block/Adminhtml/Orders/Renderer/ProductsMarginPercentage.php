<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsMarginPercentage extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductName($row);
    }


    private function getProductName($order){

        $items = $order->getAllItems();
        $shipping_amount = $order->getShippingAmount();
        $discount_amount = $order->getDiscountAmount();
        $final_shipping_price = number_format((float)$shipping_amount, 2, '.', '');
        if($items){
            if($order->getSku()){
                foreach($items as $itemId => $_item){
                    if($order->getSku()==$_item->getSku()){
                    /* // return $_item->getPrice()*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered(); */
                    return (($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered() )/($_item->getPrice()*$_item->getQtyOrdered())*100;
                    }
                }
            }else{
                $total_qty = [];   
                $margin = [];
                $price = [];
                $cost = [];
                $global_total = 0;
                $global_divisor = 0;       
                    foreach($items as $itemId => $_item){
                        if($_item->getPrice()*$_item->getQtyOrdered()!=0)
                        {
                            if($_item->getStatus()!= 'Refunded')
                            {
                                $margin[][$itemId]=((($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered()) /($_item->getPrice()*$_item->getQtyOrdered()))*100 ;

                                $global_total += (($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered()) ; 
                                $global_divisor += ($_item->getPrice()*$_item->getQtyOrdered());
                                /* $total_qty[][$itemId]= $_item->getBaseCost()*$_item->getQtyOrdered();
                                (price*qty + cost*qty + Shipping / price*qty)*100 */
                            }
                            else
                            {
                                $margin[][$itemId]= 0.00;
                            }

                        }
                        else
                        {
                            $margin[][$itemId] = 0.00;
                        }
                    }
                       
                $c=0;
                $html="";
                    $cc=0;
                    $total =0;
                    foreach($margin as $itm){
                        $final = number_format((float)$itm[$cc], 2, '.', '');
                        $html.=$final.'<br/>';
                        $total += $final;
                        $cc++;
                    }
                if($global_divisor!=0)
                {    
                    $finaltotal = ((($global_total - $discount_amount) + $final_shipping_price) / $global_divisor ) * 100; 
                }
                else
                {
                    $finaltotal = 0.00;
                }
                $html .= '<hr/>'; 
                $html .= '<strong>'.number_format((float)$finaltotal, 2, '.', '').'</strong>';
                return $html;
            }
        }

    }

}