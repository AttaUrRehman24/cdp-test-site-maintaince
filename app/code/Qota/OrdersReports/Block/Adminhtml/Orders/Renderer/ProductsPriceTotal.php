<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsPriceTotal extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductPrice($row);
    }


    private function getProductPrice($order){

        $items = $order->getAllItems();
        if($items){
            
                $html="";                
                    $html .= number_format((float)($order->getBaseTotalInvoiced() - $order->getBaseTaxInvoiced() - $order->getBaseShippingInvoiced() -$order->getBaseTotalRefunded() - $order->getBaseTaxRefunded() -$order->getBaseShippingRefunded()) * $order->getBaseToGlobalRate() , 2, '.', '');
                return $html;
        }
    }
}