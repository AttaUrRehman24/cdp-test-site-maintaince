<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class Discount extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getDiscountAmount($row);
    }


    private function getDiscountAmount($order){

        $shipping_price = $order->getDiscountAmount();
        $final_shipping_price = number_format((float)$shipping_price, 2, '.', '');
        if(!empty($shipping_price))
        {
            $html = $final_shipping_price;
           
            return $html;
        }
        else
        {
            return '';
        }
        
    }

}