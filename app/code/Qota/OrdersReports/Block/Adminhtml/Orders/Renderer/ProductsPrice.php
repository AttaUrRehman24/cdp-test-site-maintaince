<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsPrice extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductPrice($row);
    }


    private function getProductPrice($order){

        $items = $order->getAllItems();
        if($items){
            
            $price = [];  
                foreach($items as $itemId => $_item){
                    if($_item->getStatus()!= 'Refunded')
                    {                   
                        $price[][$itemId]= number_format($_item->getPrice(),2);
                    }
                    else
                    {
                        $price[][$itemId]= 0.00;
                    }
                    
                    
                }

                $html="";
                $total =0;
                    $cc=0;
                    foreach($price as $itm){
                        $final = number_format((float)$itm[$cc], 2, '.', '');
                        $html .= $final.'<br/>';
                        $total += $final;
                        $cc++;
                    }
                    $html .= '<hr/>'; 
                    $html .= '<strong>'.number_format((float)$order->getSubtotal(), 2, '.', '').'</strong>';
                return $html;
        }
    }
}