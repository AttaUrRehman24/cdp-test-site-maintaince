<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_orderCollectionFactory;
    protected $timezone;
    protected $options;
    protected $_countTotals = true;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Qota\OrdersReports\Block\Adminhtml\Orders\Options\Time $Time,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        array $data = []
    )
    {

        $this->_orderCollectionFactory=$orderCollectionFactory;
        $this->optionszone=$date;
        $this->options=$Time;
        parent::__construct($context,$backendHelper,$data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('qotaGrid');

        $this->setDefaultSort('increment_id', 'DESC');
        if ($this->hasData('default_filter')) {
            $this->setDefaultFilter($this->getData('default_filter'));
           // echo "dd";
        }
    }

    /**
     * @return $this
     */

    protected function _prepareCollection(){

        $collection=$this->_girdlist();


        $this->setCollection($collection);
		parent::_prepareCollection();
        return $this;
    }


    /**
     * @return $this
     */

    protected function _girdlist(){
        $collection=  $this->_orderCollectionFactory->create();



        $collection->addAttributeToFilter('state', array('nin' =>[\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, \Magento\Sales\Model\Order::STATE_NEW, \Magento\Sales\Model\Order::STATE_CLOSED , \Magento\Sales\Model\Order::STATE_CANCELED]));
        $collection->addAttributeToFilter('total_invoiced',['neq'=>'']);
        // $d =$collection->getSelect()->__toString();
        // print_r($d);

        // exit;
        return $collection;
    }


    protected function _prepareColumns()
    {
        //print_r($this->options->toOptionArray());

        $this->addColumn('increment_id', ['header' => __('Order ID #'), 'index' => 'increment_id', 'filter_index' => 'main_table.increment_id']);
        $this->addColumn('Products', ['header' => __(' Product Name'),'filter'=>false,  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Products']);
        $this->addColumn('ProductsCost', ['header' => __('Cost'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsCost']);
        $this->addColumn('ProductsPrice', ['header' => __('Price'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsPrice']);
        $this->addColumn('ProductsQty', ['header' => __('Qty'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsQty']);
        $this->addColumn('ProductsSubTotal', ['header' => __('Sub Total'), 'filter'=>false,'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsPriceTotal']);

        $this->addColumn('ProductsShipping', ['header' => __('Shipping'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Shipping']);
        //$this->addColumn('ProductsShipping', ['header' => __('Shipping'), 'filter'=>false, 'index' => 'shipping_amount', 'filter_index' => 'main_table.shipping_amount']);
        $this->addColumn('ProductsDiscount', ['header' => __('Discount Amount'), 'filter'=>false, 'index' => 'discount_amount', 'renderer' =>'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Discount']);
       // $this->addColumn('ProductsTax', ['header' => __('Tax'), 'index' => 'base_tax_amount','values'=>' main_table.base_tax_refunded', 'filter'=>false, 'filter_index' => 'main_table.base_tax_amount']);
        $this->addColumn('ProductsTax', ['header' => __('Tax'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsTax']);

        //$this->addColumn('GrandTotal', ['header' => __('Grand Total'), 'filter'=>false, 'index' => 'base_grand_total', 'filter_index' => 'main_table.base_grand_total']);
        $this->addColumn('ProductsMargin', ['header' => __('Margin'),'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsMargin']);
        $this->addColumn('ProductsMarginPercentage', ['header' => __('Margin %'), 'filter'=>false, 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsMarginPercentage']);
        $this->addColumn('ProductsSku', ['header' => __('Sku'),  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsSku','filter_condition_callback' => [$this, '_filterCollectionSku']]);
        //$this->addColumn('ProductsCoupon', ['header' => __('Coupon Code'), 'index' => 'coupon_code', 'filter_index' => 'main_table.coupon_code']);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status','type'=>'options','options'=>$this->options->toOptionStatus(),'filter_index' => 'main_table.status']);
        // $this->addColumn('created_at', ['header' => __('Created Date'), 'index' => 'created_at', 'type' => 'date','renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\CreatedAt','filter_condition_callback' => [$this, '_filterCollectionTimeForStarting']]);
        $this->addColumn('created_at', ['header' => __('Created Date'), 'index' => 'created_at','type'=>'datetime','filter_index' => 'main_table.created_at']);
       // $this->addColumn('updated_at', ['header' => __('Updated Date'), 'index' => 'updated_at','type'=>'date','filter_index' => 'main_table.updated_at']);
        $this->addColumn('ReportFilter', ['header' => __('Month/Year'),'type'=>'options','options'=>$this->options->toOptionReport(),'filter_condition_callback' => [$this, '_filterCollectionCurrentDate']]);


        $this->addExportType($this->getUrl('*/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
        return parent::_prepareColumns();

    }

    // protected function _addColumnFilterToCollection($column)
    // {
    //     if ($this->getCollection()) {
    //         $this->getCollection()-> $collection->addAttributeToFilter('status', array('nin' =>['new','canceled']));
    //     }
    //     return parent::_addColumnFilterToCollection($column);
    // }

    protected function _filterCollection($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());


            $d=$collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'name',
                $value
            );
            return $this;
    }

    protected function _filterCollectionSku($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'sku',
                $value
            );
            return $this;
    }

    protected function _filterCollectionCurrentDate($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->addFieldToFilter('created_at',['gteq'=>$value]);
//            print_r($collection->getselect()->__toString());
             return $this;
    }

    protected function _filterCollectionPrice($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'price',
                $value
            );
            return $this;
    }


    protected function _filterCollectionMargin($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'price',
                $value
            );

            return $this;
    }

    protected function _filterCollectionTimeForStarting($collection, $column)
    {
            //$today= date("Y-m-d H:i:s");
            $value = $column->getFilter()->getValue();

            if(!empty($value))
            {
                $collection->addFieldToFilter('main_table.created_at',$value);
                return $this;
            }
            else
            {
                return $this;
            }
    }

    protected function _filterCollectionTimeForEnding($collection, $column)
    {
            $today= date("Y-m-d H:i:s");
            $value = trim($column->getFilter()->getValue());
            $collection->addFieldToFilter('main_table.updated_at',['from'=>$value,'to'=>$today]);
            return $this;
    }


    protected function _filterCollectionManufacturer($collection, $column)
    {
        $value = trim($column->getFilter()->getValue());
       $d=$collection->join(
            ["soi" => "sales_order_item"],
        'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable")',array('sku', 'name','price'))->join(
            ["cpie" => "catalog_product_index_eav"],
            'soi.product_id = cpie.entity_id',array('value','entity_id','attribute_id')
        )->addFieldToFilter(
            'attribute_id','173'
        )->addFieldToFilter(
            'value',$value
        );

        return $this;
   }

     public function getTotals()
    {
        $totals = new \Magento\Framework\DataObject;
        // $fields = array(
        //     'price' => 0
        // );
        // foreach ($this->_girdlist() as $item) {
        //     foreach($fields as $field=>$value){
        //         if($field == "tax") {
        //             $fields[$field] += $item->getData('TaxAmount');
        //         }
        //     }
        // }
        //First column in the grid


        return $totals;
    }

}
