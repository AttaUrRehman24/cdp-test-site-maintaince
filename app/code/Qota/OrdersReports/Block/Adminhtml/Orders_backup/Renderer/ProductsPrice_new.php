<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsPrice extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductPrice($row);
    }


    private function getProductPrice($order){

        $items = $order->getAllItems();
        if($items){
            
                foreach($items as $itemId => $_item){                   
                    return number_format($_item->getPrice(),2);
                }
        }
    }
}