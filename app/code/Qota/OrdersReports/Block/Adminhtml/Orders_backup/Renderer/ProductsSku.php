<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsSku extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
//print_r($this->getFilter());
		return $this->getProductName($row);
    }


    private function getProductName($order){

        if($order->getSku()){
            return $order->getSku();
        }else{

       
        $items = $order->getAllItems();



        if($items){ 
            $total_qty = [];   
            $sku = [];   
                foreach($items as $itemId => $_item){                    
                    $sku[][$itemId]= $_item->getSku();
                }           
            $c=0;
            $html="";               
                $cc=0;
                foreach($sku as $itm){
                    $html.=$itm[$cc].'<br/>';
                    $cc++;
                }                
            return $html;
        }

    }
    }

}