<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;


class Manufacturer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;
    protected $_Product;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Catalog\Model\Product $Product,
        array $data = []
    )
    {
        $this->_Product=$Product;
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
       
		return $this->getProductName($row);
    }


    private function getProductName($order){

        $items = $order->getAllItems();
        if($items){ 
            $manufacturer = []; 
                foreach($items as $itemId => $_item){
                    $product=$this->_Product->load($_item->getId());
                         $manufacturer[][$itemId]=$product->getData('manufacturer');
                }           
            $html="";
               
                $cc=0;
                foreach($manufacturer as $itm){
                    $html.=$itm[$cc].'<br/>';
                    $cc++;
                }
                

            return $html;


        }else{
            return 'Empty';
        }
    }

}