<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class Products extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
       
		return $this->getProductName($row);
    }


    private function getProductName($order){
        $pname = $order->getName();
        $items = $order->getAllItems();
        if($items){ 
            if($pname){
                foreach($items as $itemId => $_item){    
                    if($pname==$_item->getName()){
                        return $_item->getName();
                    }                
                } 
            }else{


            $total_qty = [];   
            $sku = [];   
                foreach($items as $itemId => $_item){                    
                    $name[][$itemId]= $_item->getName();
                }           
            $c=0;
            $html="";               
                $cc=0;
                foreach($name as $itm){
                    $html.=$itm[$cc].'<br/>';
                    $cc++;
                }                
            return $html;
        }

        }
    }

}