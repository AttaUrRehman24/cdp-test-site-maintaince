<?php
namespace Qota\Popup\Model;
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'qota_popup_post';

    protected $_cacheTag = 'qota_popup_post';

    protected $_eventPrefix = 'qota_popup_post';

    protected function _construct()
    {
        $this->_init('Qota\Popup\Model\ResourceModel\Post');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}