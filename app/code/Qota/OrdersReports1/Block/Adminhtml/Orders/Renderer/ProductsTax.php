<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsTax extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductTax($row);
    }


    private function getProductTax($order){

        $items = $order->getAllItems();
        if($items){

            $tax = [];  
                foreach($items as $itemId => $_item){                   
                    $tax[][$itemId]=number_format($_item->getTaxAmount(),2);
                }
                $html="";
                    $cc=0;
                    foreach($tax as $itm){
                        $html.=$itm[$cc].'<br/>';
                        $cc++;
                    }
                return $html;

                
        }
    }

}