<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsQty extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductQty($row);
    }


    private function getProductQty($order){

        $items = $order->getAllItems();
        if($items){

            $qty = [];  
                foreach($items as $itemId => $_item){                   
                    $qty[][$itemId]=number_format($_item->getQtyOrdered(),2);
                }
                $html="";
                    $cc=0;
                    foreach($qty as $itm){
                        $html.=$itm[$cc].'<br/>';
                        $cc++;
                    }
                return $html;

                
        }
    }

}