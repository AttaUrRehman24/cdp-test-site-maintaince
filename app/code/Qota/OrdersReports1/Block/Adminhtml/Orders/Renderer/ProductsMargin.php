<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsMargin extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductName($row);
    }


    private function getProductName($order){

        $items = $order->getAllItems();
        if($items){
            if($order->getSku()){
                foreach($items as $itemId => $_item){
                    if($order->getSku()==$_item->getSku()){
                    // return $_item->getPrice()*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered();
                    return ($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered();
                    }
                }
            }else{
                $total_qty = [];   
                $margin = [];   
                    foreach($items as $itemId => $_item){
                        $margin[][$itemId]= ( ($_item->getPrice())*$_item->getQtyOrdered() - $_item->getBaseCost()*$_item->getQtyOrdered());
                        //$total_qty[][$itemId]= $_item->getBaseCost()*$_item->getQtyOrdered();
                    }           
                $c=0;
                $html="";
                    $cc=0;
                    foreach($margin as $itm){
                        $html.=$itm[$cc].'<br/>';
                        $cc++;
                    }
                return $html;
            }
        }

    }

}