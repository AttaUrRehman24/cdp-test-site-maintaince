<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Renderer;

class ProductsCost extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
		return $this->getProductPrice($row);
    }


    private function getProductPrice($order){

        $items = $order->getAllItems();
        if($items){

            $cost = [];  
                foreach($items as $itemId => $_item){                   
                    $cost[][$itemId]=number_format($_item->getBaseCost(),2);
                }
                $html="";
                    $cc=0;
                    foreach($cost as $itm){
                        $html.=$itm[$cc].'<br/>';
                        $cc++;
                    }
                return $html;

                
        }
    }
}