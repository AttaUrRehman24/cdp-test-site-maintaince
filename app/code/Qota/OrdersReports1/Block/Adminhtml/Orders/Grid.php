<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_orderCollectionFactory;
    protected $timezone;
    protected $options;
    protected $_countTotals = true;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Qota\OrdersReports\Block\Adminhtml\Orders\Options\Time $Time,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        array $data = []
    )
    {

        $this->_orderCollectionFactory=$orderCollectionFactory;
        $this->optionszone=$date;
        $this->options=$Time;
        parent::__construct($context,$backendHelper,$data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('qotaGrid');

        $this->setDefaultSort('increment_id', 'DESC');
        if ($this->hasData('default_filter')) {
            $this->setDefaultFilter($this->getData('default_filter'));
        }
    }

    /**
     * @return $this
     */

    protected function _prepareCollection(){
        $collection=  $this->_orderCollectionFactory->create();
//        $collection->addAttributeToFilter('status', ['in' =>['commande_livree','complete','holded','payment_review','pendig_payment_3xsansfrais','pending_paypal','processing']]);
        //$collection->addAttributeToFilter('status', ['neq' =>'canceled']);
        //$collection->addAttributeToFilter('status', ['neq' =>'closed']);
        //$collection->addAttributeToFilter('status', ['neq' =>'fraud']);

        // $collection->addAttributeToFilter('status', array('neq' =>'canceled'));
        // $collection->addAttributeToFilter('status', array('neq' =>'fraud'));
        // $collection->addAttributeToFilter('status', array('neq' =>'pending'));
        // $collection->addAttributeToFilter('status', array('neq' =>'pendingpayment'));
        // $collection->addAttributeToFilter('status', array('neq' =>'pending_payment'));
        // $collection->addAttributeToFilter('status', array('neq' =>'closed'));
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    /**
     * @return $this
     */


    protected function _prepareColumns()
    {
        //print_r($this->options->toOptionArray());



        $this->addColumn('increment_id', ['header' => __('Order ID #'), 'index' => 'increment_id', 'filter_index' => 'main_table.increment_id']);
        $this->addColumn('Products', ['header' => __(' Product Name'),   'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Products']);
        $this->addColumn('ProductsPrice', ['header' => __('Price'),  'index' => 'price_new', 'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsPrice']);
        $this->addColumn('ProductsCost', ['header' => __('Cost'), 'index' => 'cost',  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsCost']);
        $this->addColumn('ProductsQty', ['header' => __('Qty'),  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsQty']);
        $this->addColumn('ProductsMargin', ['header' => __('Margin'),'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsMargin']);
        $this->addColumn('ProductsMarginPercentage', ['header' => __('Margin %'),'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsMarginPercentage']);
        $this->addColumn('ProductsTax', ['header' => __('Tax'),  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsTax']);
        $this->addColumn('ProductsSku', ['header' => __('Sku'),  'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\ProductsSku','filter_condition_callback' => [$this, '_filterCollectionSku']]);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status','type'=>'options','options'=>$this->options->toOptionStatus(),'filter_index' => 'main_table.status']);
        $this->addColumn('created_at', ['header' => __('Created Date'), 'index' => 'created_at','type'=>'date','filter_index' => 'main_table.created_at']);
        $this->addColumn('updated_at', ['header' => __('Updated Date'), 'index' => 'updated_at','type'=>'date','filter_index' => 'main_table.updated_at']);
        // $this->addColumn('manufacturer', ['header' => __('Manufacturer'),'type'=>'options','options'=>$this->options->toOptionAttrArray(),'filter_condition_callback' => [$this, '_filterCollectionManufacturer'],'renderer' => 'Qota\OrdersReports\Block\Adminhtml\Orders\Renderer\Manufacturer']);

        $this->addExportType($this->getUrl('*/*/exportExcel', ['_current' => true]),__('Excel XML'));

 //       $this->addlistingToolbar();

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
        return parent::_prepareColumns();
    }


    protected function _filterCollection($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $d=$collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'name',
                $value
            );
            return $this;
    }

    protected function _filterCollectionSku($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'sku',
                $value
            );
            return $this;
    }

    protected function _filterCollectionPrice($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'price',
                $value
            );
            return $this;
    }


    protected function _filterCollectionMargin($collection, $column)
    {
            $value = trim($column->getFilter()->getValue());
            $collection->join(
                ["soi" => "sales_order_item"],
            'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable") ',
            array('sku', 'name','price'))->addFieldToFilter(
                'price',
                $value
            );
            return $this;
    }

   


    protected function _filterCollectionManufacturer($collection, $column)
    {
        $value = trim($column->getFilter()->getValue());
       $d=$collection->join(
            ["soi" => "sales_order_item"],
        'main_table.entity_id = soi.order_id AND soi.product_type in ("simple","downloadable")',array('sku', 'name','price'))->join(
            ["cpie" => "catalog_product_index_eav"],
            'soi.product_id = cpie.entity_id',array('value','entity_id','attribute_id')
        )->addFieldToFilter(
            'attribute_id','173'
        )->addFieldToFilter(
            'value',$value
        );
        return $this;
   }

     public function getTotals()
    {
        $totals = new \Magento\Framework\DataObject;
        
        $fields = array(
            'increment_id' => 0,
            'base_subtotal' => 0,
            'price' => 0
        );

      //  $final_q=$this->getCollection()->join();
        foreach ($this->getCollection() as $item) {
            
            foreach($fields as $field=>$value){
                /* print_r($fields);
                exit(); */
               
                if($field == "price") {
                    $fields[$field] += $item->getData("base_subtotal");
                    //print_r($item->getData());
                    //exit;
                   $abc = $item->getData("base_subtotal");
                   $final = number_format((float)$abc, 2, '.', '');
                   $sum[] = $final;
                  
               }
            }
            
        }
        $result =  array_sum($sum);

        $totals->setItem($fields);
        //First column in the grid
       
    //     exit;
        
        $totals->setData($fields);

        return $totals;
    }




}
