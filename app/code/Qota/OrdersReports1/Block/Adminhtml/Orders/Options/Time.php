<?php

namespace Qota\OrdersReports\Block\Adminhtml\Orders\Options;

class Time implements \Magento\Framework\Option\ArrayInterface
{
    protected $timezone;
    protected $_eavConfig;


    public function __construct(
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        \Magento\Eav\Model\Config $eavConfig
    ){
        $this->timezone=$date;
        $this->_eavConfig=$eavConfig;
    }

    public function toOptionArray(){

        $date=$this->timezone->date()->format('Y-m-d H:i:s');
        $time = $this->timezone->scopeTimeStamp();
        $d = new \Zend_Date($time, \Zend_Date::TIMESTAMP);

       $day= date("Y-m-d H:i:s ",strtotime('-1 day', time()));
       $week= date("Y-m-d H:i:s",strtotime('-1 week', time()));
       $month= date("Y-m-d H:i:s",strtotime('-1 month', time()));
       $year=  date("Y-m-d H:i:s",strtotime('-1 year', time()));

        $options=[];
        $options=[$day=>__('Day'),$week=>__('Week'),$month=>__('Month'),$year=>__('Year')];
        return $options;

    }

    public function toOptionAttrArray(){

        $attribute = $this->_eavConfig->getAttribute('catalog_product', 'manufacturer');
        $opt = $attribute->getSource()->getAllOptions();
        $options=[];
        foreach ($opt as $key => $value) {
            $options[$value['value']]=$value['label'];
        }
        return $options;
    }


    public function toOptionStatus(){

//        $opt= array(''=>'Select Options','canceled'=>'Annulée','closed'=>'Terminée','commande_livree'=>'Livré','complete'=>'Envoyée','fraud'=>'Bloquée','holded'=>'en Anomalie','lengow_technical_error'=>'Lengow Technical Error','payment_review'=>'Vérif. en cours','paypal_canceled_reversal'=>'PayPal Canceled Reversal','paypal_reversed'=>'PayPal Reversed','pendig_payment_3xsansfrais'=>'En attente de paiement 3x sans frais','pending'=>'En attente de paiement', 'pendingpayment'=>'En attente de paiement','pending_payment'=>'En attente de paiement','pending_paypal'=>'Pending PayPal','processing'=>'Validée');
  
        $opt= array(''=>'Select Options','closed'=>'Terminée','commande_livree'=>'Livré','complete'=>'Envoyée','fraud'=>'Bloquée','holded'=>'en Anomalie','lengow_technical_error'=>'Lengow Technical Error','payment_review'=>'Vérif. en cours','paypal_canceled_reversal'=>'PayPal Canceled Reversal','paypal_reversed'=>'PayPal Reversed','pendig_payment_3xsansfrais'=>'En attente de paiement 3x sans frais','pending'=>'En attente de paiement', 'pendingpayment'=>'En attente de paiement','pending_payment'=>'En attente de paiement','pending_paypal'=>'Pending PayPal','processing'=>'Validée');

        return $opt;

    }



}
