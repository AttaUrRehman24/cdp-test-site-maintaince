<?php

namespace Qota\OrdersReports\Controller\Adminhtml\Orders;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportExcel extends \Magento\Backend\App\Action
{
    protected $_fileFactory;

    public function execute()
    {

        try{

        $fileName   = 'modele.xls';

        $content    = $this->_view->getLayout()->createBlock('\Qota\OrdersReports\Block\Adminhtml\Orders\Grid')->getCsvFile();
        

        $file = fopen('/home/www/comptoirdespros.com/var/'.$content['value'],"r");
        $csv_final_content = array();
        $i = 0;
        $csv_head = '';
        $headers = [];
		
        while(! feof($file))
        {              
            $excel_content = fgetcsv($file);	
			if($i == 0) {
                $headers = $excel_content;
            }
            $order_id = $excel_content[0];
            $product_name = explode('<br/>',$excel_content[1]);
            //$product_name = implode('',$product_name);
            $margin = explode('<br/>',$excel_content[2]);
            //$margin = implode(',"',$margin);
            $sku = explode('<br/>',$excel_content[3]);
            //$sku = implode(',',$sku);
            $created_date = $excel_content[4];
            $updated_date = $excel_content[5];
         //   $manufacturer  = $excel_content[6];            
            $status = $excel_content[6];


            if($i != 0){
                if($order_id==""){
                }else{
                $csv_final_content[0] = $headers;
                
                foreach($product_name as $key => $value){
                    if($value != ""){						
						$csv_final_content[] = array(
							$order_id,
                            (isset($product_name[$key]) ? trim($product_name[$key]) : ''),
                            (isset($margin[$key]) ? trim($margin[$key]) : ''),
                            (isset($sku[$key]) ? trim($sku[$key]) : ''),
                            $created_date,
                            $updated_date,                            
                            $status,
						);
                    }
                }

                // $csv_final_content[] = array(
                //     			$order_id,
                //                 (isset($product_name) ? ($product_name) : ''),
                //                 (isset($margin) ? ($margin) : ''),
                //                 (isset($sku) ? ($sku) : ''),
                //                 $created_date,
                //                 $updated_date,                            
                //                 $status
                //     		);

                      
                    }
            }else{
                $csv_head = $excel_content;
            }
            $i++;
          }
        //   echo '<pre>';
        //   print_r($csv_final_content);
        //   exit();            
			$date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
          	$this->convert_to_excel($csv_final_content, $date . '.xls', '\t');
        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }
    }

    function convert_to_excel($input_array, $output_file_name, $delimiter)
	{
        $temp_memory = fopen("php://memory", 'w');
        foreach ($input_array as $line)
        {
            
            //print_r($line);


            fputcsv($temp_memory, $line,"\t");
        }
       // exit;
        //fclose($temp_memory);
		// $temp_memory = fopen('php://memory', 'w');
		// // loop through the array
		// foreach ($input_array as $line) {
		// // use the default csv handler
		// fputcsv($temp_memory, $line, $delimiter);
		// }
		fseek($temp_memory, 0);
        // modify the header to be CSV format
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=" . $output_file_name);  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
		// output the file to be downloaded
		fpassthru($temp_memory);
	}

    function convert_to_csv($input_array, $output_file_name, $delimiter)
	{
		$temp_memory = fopen('php://memory', 'w');
		// loop through the array
		foreach ($input_array as $line) {
		// use the default csv handler
		fputcsv($temp_memory, $line, $delimiter);
		}

		fseek($temp_memory, 0);
		// modify the header to be CSV format
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
		// output the file to be downloaded
		fpassthru($temp_memory);
	}

    public function execute1()
    {
        $this->_view->loadLayout(false);

        $fileName = 'account.xls';

        $exportBlock = $this->_view->getLayout()->createBlock('\Qota\Account\Block\Adminhtml\Lines\Grid');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->_fileFactory = $objectManager->create('Magento\Framework\App\Response\Http\FileFactory');


        return $this->_fileFactory->create(
            $fileName,
            $exportBlock->getExcelFile(),
            DirectoryList::VAR_DIR
        );
    }
}