<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Qota\CustomAttrOrderlist\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $setup->getConnection()
        ->addColumn(
                    $setup->getTable('sales_order'),
                            'Payment_failed',
                            ['type'=>Table::TYPE_INTEGER,
                            'size'=>11,
                            'nullable'=>true,
                            'comment'=>'Payment Failed',
                            "default"=>0]
                   );
        $setup->endSetup();
    }

}
