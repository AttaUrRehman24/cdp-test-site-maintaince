<?php

namespace Qota\CustomAttrOrderlist\Controller\Payment;

use \Magento\Framework\Validator\Exception;

class Failed extends \Paybox\Epayment\Controller\Payment
{
    public function execute()
    {
        try {
            $session = $this->getSession();
            $paybox = $this->getPaybox();

            // Retrieves params
            $params = $paybox->getParams(false, false);
            if ($params === false) {
                return $this->_404();
            }

            // Load order
            $order = $this->_getOrderFromParams($params);

            if (is_null($order) || is_null($order->getId())) {
                return $this->_404();
            }

            // Payment method
            $order->getPayment()->getMethodInstance()->onPaymentFailed($order);
            $order->setPayment_failed('1')-save();

            // Set quote to active
            $this->_loadQuoteFromOrder($order)->setIsActive(true)->save();

            // Cleanup
            $session->unsCurrentPbxepOrderId();

            $message = sprintf('Order %d: Customer is back from Verifone e-commerce payment page. Payment refused by Verifone e-commerce (%d).', $order->getIncrementId(), $params['error']);
            $this->logDebug($message);

            $message = __('Payment refused by Verifone e-commerce.');
            $this->_messageManager->addError($message);

            // redirect
            $this->_redirectResponse($order, false /* is success ? */);
        } catch (\Exception $e) {

            $this->logDebug(sprintf('failureAction: %s', $e->getMessage()));
        }

        // Redirect to cart
        $this->_redirect('checkout/cart');
    }
}
