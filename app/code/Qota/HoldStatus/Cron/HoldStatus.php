<?php

namespace Qota\HoldStatus\Cron;
use Magento\Sales\Model\Order;


class HoldStatus 
{
    protected $logger;
    protected $collectionFactory;
    protected $_order;
    const ADMIN_RESOURCE = 'Magento_Sales::hold';
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Sales\Api\OrderManagementInterface $orderManagementInterface){
        
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->_order = $orderManagementInterface;

    }
    
    public function execute()
    {
      
        $this->SalesList();
       
           
    }

    public function SalesList(){
        
        $ThirtyMints = date("Y-m-d H:i:s", strtotime("+30 minutes"));
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('increment_id');                     
        $collection->addAttributeToSelect('created_at'); 
        $collection->addAttributeToSelect('entity_id'); 
        $collection->addAttributeToSelect('Payment_failed'); 
        $collection->getSelect()->join(["sop"=>"sales_order_payment"],'main_table.entity_id = sop.parent_id',array('method'))
       ->where("status='processing' AND method='pbxep_threetime' AND Payment_failed=0");      

        if(count($collection)){
            foreach ($collection as $order) {
                if($order->getEntityId()){
                try {
                       $this->_order->hold($order->getEntityId());
                       $this->failed($order->getEntityId());
                       $this->logger->addInfo(__($order->getEntityId().'__You put the order on hold.'));
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                       $this->logger->addInfo($e->getMessage());
                    } catch (\Exception $e) {
                       $this->logger->addInfo(__('You have not put the order on hold.'));
                    }
                    

               }
                 
            }  
            
        }else{
            $this->logger->addInfo('Empty_______');  
            
        }
    }

    
 private function failed($Id){
        
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('sales_order'); //gives table name with 
            $sales_order_status_history = $resource->getTableName('sales_order_status_history'); //gives table name with 
            $sql = "Update `" . $tableName . "` Set payment_failed=1  where entity_id =".$Id;
            $connection->query($sql);                
            $sales_order_status_history_sql ="Insert into `".$sales_order_status_history."` (parent_id,is_customer_notified,comment,status,entity_name) values('".$Id."','1','Holded cron updated','holded','order')"; 
            $connection->query($sales_order_status_history_sql);
            
    }
  
}