<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class CreditInvoice extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $order;
    protected $_resource;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Sales\Model\OrderFactory $order,        
        array $data = []
    )
    {
        $this->order = $order;
        $this->_resource = $resource;
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
   		$orderStatus = $row->getstate();
	   	$grandTotal = $row->getcredit_grand_total();
   		//	$order_id = $row->getInvoiceCollection();        
    	//  $html=  $this->inv($order_id);
   		$order_id= $this->order->create()->loadByIncrementId($row->getIncrementId());

    	$invoice_id ='';

    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$order_load = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($row->getIncrementId());
		$payment_code = $row->getPaymentMethod();

         foreach ($order_id->getInvoiceCollection() as $invoice)
        {
        	if ($grandTotal > '0') {        		
        		$invoice_id = 'AV'.$invoice->getIncrementId();        		
        	}
        	else{
        		$invoice_id = '';
        	}
        }

		return $invoice_id;
    }



}