<?php

namespace Qota\Account\Block\Adminhtml\Lines\Renderer;

class CreditMontant extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{

    protected $_coreHelper = null;
    protected $_resource;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    )
    {
        $this->_resource = $resource;
        parent::__construct($context, $jsonEncoder, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        
        $orderId = $row->getcredit_order_id();
        $actions = '';

        $connection  = $this->_resource->getConnection();
        $tableName   = $connection->getTableName('sales_creditmemo');
        // SELECT DATA 
        $fields = array('grand_total', 'subtotal', 'discount_amount', 'shipping_amount');
        $sql = $connection->select()                      
                        ->from($tableName, $fields) // to select some particular fields                  
                        ->where('order_id = ?', $orderId);                  
        $result = $connection->fetchAll($sql);
        $grandTotalKey = 'grand_total';
        $grandTotal = array_sum(array_column($result,$grandTotalKey));
        $subTotalKey = 'subtotal';
        $subTotal = array_sum(array_column($result,$subTotalKey));
        $discountAmountKey = 'discount_amount';
        $discountAmount = array_sum(array_column($result,$discountAmountKey));
        $shippingAmountKey = 'shipping_amount';
        $shippingAmount = array_sum(array_column($result,$shippingAmountKey));

        if($grandTotal > '0'){
            $actions .= number_format($grandTotal,2,'.','').'<br>';
            $actions .= number_format($subTotal,2,'.','').'<br>';
            if($discountAmount > 0){
                $actions .= number_format($discountAmount,2,'.','').'<br>';
            }
            $actions .= number_format($shippingAmount,2,'.','').'<br>';
            $actions .= number_format((($grandTotal/1.2)*0.2),2,'.','');
        }else{
            $actions .= '<br>';
        }
        return $actions;
    }
}