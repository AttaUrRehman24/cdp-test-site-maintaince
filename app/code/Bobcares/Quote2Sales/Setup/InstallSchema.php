<?php

/**
 * Created on 22nd Oct.
 * Bobcares Quote2Sales Schema Setup.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Bobcares Quote2Sales Schema Setup.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * To set up the schema interface
     * 
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
        ->newTable($installer->getTable('quote2sales_quotes'))
        ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Entity Id')
        ->addColumn('request_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true], 'Request Id')
        ->addColumn('quote_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => false], 'Quote Id')
        ->addColumn('quote_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Quote Date')
        ->addColumn('quote_item', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Quote Item')
        ->addColumn('subtotal', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Subtotal')
        ->addColumn('quote_total', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Quote Total')
        ->addColumn('user_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'User Id')
        ->addColumn('customer_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true], 'CustomerFirstName')
        ->addColumn('customer_email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Email ID')
        ->addColumn('seller_comment', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Seller Comment')
        ->addColumn('is_custom_shipping_applied', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Is custom shipping applied')
        ->addColumn('custom_shipping_amount', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Custom shipping amount')
        ->setComment('quote2sales_quotes');
        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()
        ->newTable($installer->getTable('quote2sales_requests'))
        ->addColumn('request_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Request Id')
        ->addColumn('customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Customer Id')
        ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Created at')
        ->addColumn('name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'CustomerFirstName')
        ->addColumn('email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Email ID')
        ->addColumn('phone', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'MobileNumber')
        ->addColumn('comment', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Comments')
        ->addColumn('status', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => 'waiting'], 'Status')
        ->addColumn('product_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Product Id')
        ->addColumn('seller_comment', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Seller Comments')
        ->setComment('quote2sales_requests');
        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()
        ->newTable($installer->getTable('quote2sales_requests_status'))
        ->addColumn('status_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Entity Id')
       ->addColumn('requests_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Quote Id')        
        ->addColumn('quote_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Request Id')
        ->addColumn('order_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'default' => null], 'Order Id')
        ->addColumn('status', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => 'waiting'], 'Status')
        ->setComment('quote2sales_requests_status');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }

}