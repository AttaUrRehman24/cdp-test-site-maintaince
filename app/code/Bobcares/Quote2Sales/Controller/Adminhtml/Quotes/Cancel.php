<?php

/**
 * Bobcares Quote2Sales Controller 
 * for cancelling the quotes.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Adminhtml\Quotes;

/**
 * Bobcares Quote2Sales Controller 
 * for cancelling the quotes..
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Cancel extends \Bobcares\Quote2Sales\Controller\Adminhtml\Quote {

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Bobcares_Quote2Sales::cancel';

    /**
     * Cancel Quote
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {

        $resultRedirect = $this->resultRedirectFactory->create();

        $quoteId = $this->quotes->getParam('quote_id');
        $quote = $this->quoteFactory->load($quoteId);
        $requestId = $this->requestStatusModelQuote->getRequestId($quoteId, NULL);
        
        foreach ($requestId as $value) {
            $_requestId = $value['requests_id'];
        }        
        
        //If the quote exists, cancel the quote 
        //and delete it from the database
        if ($quote) {
            try {
                $requests = $this->requestModelQuote->cancelQuotes($quoteId);
                
                if ($requestId) {

                    //update request status table
                    $requestStatus = $this->requestStatusModelQuote->updateStatus("Canceled Quote", $quoteId);

                    //request table update
                    $requestUpdate = $this->requestStatusUpdateQuote->updateRequestStatus("Waiting", $_requestId, NULL);
                }

                $this->messageManager->addSuccess(__('You canceled the quote.'));
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Sorry. Couldn\'t cancel the quote.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            }
        }

        return $resultRedirect->setPath('quote2sales/quotes/quotes');
    }

}
