<?php

/**
 * Bobcares Quote2Sales Controller 
 * for saving the quotes.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Create;

/**
 * Bobcares Quote2Sales Controller 
 * for saving the quotes.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Save extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultRedirect;
    
    /**
     * @var \Magento\Customer\Model\Session 
     */
    protected $customerSession;
    
    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'contact/email/sender_email_identity';

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Bobcares\Quote2Sales\Model\RequestFactory $db
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Convert $convert
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Bobcares\Quote2Sales\Model\Email $quoteEmail
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item\Option\Collection $itemCollection
     * @param \Bobcares\Quote2Sales\Model\Quotes $requestModelQuote
     * @param \Bobcares\Quote2Sales\Model\RequestStatus $requestStatusModelQuote
     * @param \Bobcares\Quote2Sales\Model\Request $requestStatusUpdateQuote
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Catalog\Helper\Product $productHelper, 
    \Magento\Framework\Escaper $escaper, 
    \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
    \Bobcares\Quote2Sales\Model\RequestFactory $db, 
    \Magento\Framework\Controller\Result\Redirect $resultRedirect, 
    \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, 
    \Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Convert $convert, 
    \Magento\Quote\Model\QuoteFactory $quoteFactory, 
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Framework\Registry $coreRegistry,
    \Bobcares\Quote2Sales\Model\Email $quoteEmail,
    \Magento\Quote\Model\ResourceModel\Quote\Item\Option\Collection $itemCollection,
    \Bobcares\Quote2Sales\Model\Quotes $requestModelQuote,
    \Bobcares\Quote2Sales\Model\RequestStatus $requestStatusModelQuote,
    \Bobcares\Quote2Sales\Model\Request $requestStatusUpdateQuote,
    \Magento\Customer\Model\Customer $customerData

    ) {
        parent::__construct($context);
        $productHelper->setSkipSaleableCheck(true);
        $this->escaper = $escaper;
        $this->resultPageFactory = $resultPageFactory;
        $this->requestFactory = $db;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultRedirect = $resultRedirect;
        $this->convert = $convert;
        $this->quoteFactory = $quoteFactory;
        $this->customerSession = $customerSession;
        $this->_coreRegistry = $coreRegistry;
        $this->quoteEmail = $quoteEmail;
        $this->itemCollection = $itemCollection;
        $this->requestModelQuote = $requestModelQuote;
        $this->requestStatusModelQuote = $requestStatusModelQuote;
        $this->requestStatusUpdateQuote = $requestStatusUpdateQuote;
        $this->customerData = $customerData;
    }

    /**
     * Saving quote and create order
     *
     * @return \Magento\Backend\Model\View\Result\Forward|\Magento\Backend\Model\View\Result\Redirect
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        try {

            // check if the creation of a new customer is allowed
            if (!$this->_authorization->isAllowed('Magento_Customer::manage') && !$this->_getSession()->getCustomerId() && !$this->_getSession()->getQuote()->getCustomerIsGuest()
            ) {
                return $this->resultForwardFactory->create()->forward('denied');
            }

            $this->convert->_getOrderCreateModel()->getQuote()->setCustomerId($this->_getSession()->getCustomerId());
            $this->convert->_processActionData('save');
            $quote_id = $this->convert->_getOrderCreateModel()->getQuote()->getId();
            $orderArray = $this->getRequest()->getPost('order');
            $sellerComment = $orderArray['comment']['customer_note'];
            $requestId = $this->customerSession->getMyValue();

            $this->updateQuotesTable($quote_id, $sellerComment, $requestId);
            $resultRedirect->setUrl($this->getUrl('quote2sales/quotes/info' . '/quote_id/' . $quote_id));
            $this->messageManager->addSuccess(__('You created the Quote.'));
        } catch (\Exception $e) {
            $message = $e->getMessage();

            //Add error message
            if (!empty($message)) {
                $this->messageManager->addError($message);
            }

            $resultRedirect->setPath('quote2sales/*/');
        }

        return $resultRedirect;
    }

    /**
     * @ Adds new quote data into the quotes table.
     * @param int $quoteId  : Quote ID
     * @param unknown $sellerComment : Seller comments
     */
    public function updateQuotesTable($quoteId, $sellerComment, $requestId) {

        $resultRedirect = $this->resultRedirectFactory->create();

        $quote = $this->quoteFactory->create()->load($quoteId);

        $quoteDate = $quote['created_at'];
        $subTotal = $quote['subtotal'];
        $total = $quote['grand_total'];
        $id = $this->convert->_getSession()->getCustomerId(); //customer id

        $details = $this->itemCollection->addItemFilter($quoteId);
        $itemsData = $quote->getAllItems();

        $productNames = null;

        //fetch each item name
        foreach ($itemsData as $itemIds => $itemValue) {
            $productNames .= $itemValue->getName() . "</br>";
        }

        // If custom shipping method is applied, saving the flag and set amount in the quote2sales_quotes table
        if ($quote->getShippingAddress()->getShippingMethod() === 'customshipprice_customshipprice') {
            $customShippingSet = 1;
            $customShipPriceSet = $quote->getShippingAddress()->getShippingAmount();
        } else {
            $customShippingSet = 0;
            $customShipPriceSet = 0;
        }

        $customerInstance = $this->customerData->load($id);
        $customer = $customerInstance->getName();
        $customerEmail = $customerInstance->getEmail();

        $sendQuoteEmail = $this->quoteEmail->sendEmail($quoteId, $sellerComment, $id);
        $requests = $this->requestModelQuote->insertQuotes($requestId, $quoteId, $quoteDate, $productNames, $subTotal, $total, $id, $customer, $customerEmail, $sellerComment, $customShippingSet, $customShipPriceSet);

        //If the request id is null then check in session
        if ($requestId == NULL) {
            $requestId = $this->_getSession()->getDelQuoteRequestId();
        }

        //If there is a request id (creating quote based on request) then update the table
        if ($requestId != NULL) {
            $requestStatus = $this->requestStatusModelQuote->insertStatus($requestId, $quoteId);
            $requestUpdate = $this->requestStatusUpdateQuote->updateRequestStatus("Converted To Quote", $requestId, $sellerComment, NULL);
        }

        $this->_getSession()->clearStorage();


        return $this->resultRedirectFactory->create()->setPath('quote2sales/quotes/info');
    }

    /**
     * Initialize order model instance
     *
     * @return \Magento\Sales\Api\Data\OrderInterface|false
     */
    public function _initQuote() {

        $quote_id = $this->convert->_getOrderCreateModel()->getQuote()->getId();
        $quote = $this->quoteFactory->create()->load($quote_id);

        //If there is no quote id then display an error message
        if (empty($quote_id)) {
            $this->_getSession()->addError($this->__('This quote no longer exists.'));
            $this->_redirect('*/*/');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }

        $this->_coreRegistry->register('sales_quote', $quote);
        $this->_coreRegistry->register('current_quote', $quote);
        return $quote;
    }

}
