<?php

/**
 * Bobcares Quote2Sales Controller 
 * for emailing the quotes.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Adminhtml\Quotes;

/**
 * Bobcares Quote2Sales Controller 
 * for emailing the quotes.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Email extends \Bobcares\Quote2Sales\Controller\Adminhtml\Quote {

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Bobcares_Quote2Sales::email';

    /**
     * Email Quote
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {

        $resultRedirect = $this->resultRedirectFactory->create();

        $quoteId = $this->quotes->getParam('quote_id');
        $quote = $this->quoteModel->loadByQuoteId($quoteId);
        $customerId = $quote[0]['user_id'];
        $sellerComment = $quote[0]['seller_comment'];

        //If the quote exists, send email with the quuote details.
        if ($quote) {
            try {
                $sendQuoteEmail = $this->quoteEmail->sendEmail($quoteId, $sellerComment, $customerId);
                $this->messageManager->addSuccess(__('The quote has been emailed.'));
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Could not email the quote.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            }
        }

        return $resultRedirect->setPath('quote2sales/quotes/quotes');
    }

}
