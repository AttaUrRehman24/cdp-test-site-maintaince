<?php

/**
 * Created on 21st Oct 2016.
 * Bobcares_Quote2Sales
 * Admin grid for Quotes Display.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Adminhtml\Quotes;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Bobcares_Quote2Sales 
 * Admin grid for Quotes Display.
 * @category    Bobcares
 * @author      BDT
 */
class Quotes extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();

        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('Bobcares_Quote2Sales::quotes');

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Bobcares'), __('Bobcares'));
        $resultPage->addBreadcrumb(__('Quote2Sales'), __('Quotes'));

        //Set the header title of grid
        $resultPage->getConfig()->getTitle()->prepend(__('Quotes'));

        return $resultPage;
    }

    /**
     * Check permission via ACL resource
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Bobcares_Quote2Sales::quotes');
    }

}
