<?php

/**
 * Created on 12th April 2017.
 * Bobcares_Quote2Sales
 * Action controller for View Quotes.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Adminhtml\Quotes;

/**
 * Bobcares_Quote2Sales
 * Action controller for View Quotes
 * @category    Bobcares
 * @author      BDT
 */
class Info extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Framework\View\Result\PageFactory $resultPageFactory,
    \Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Create\Save $quoteSave
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->quoteSave = $quoteSave;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {
        
        //$this->quoteSave->_initQuote();
        $resultRedirect = $this->resultRedirectFactory->create();
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();

        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('Bobcares_Quote2Sales::quotes');

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Bobcares'), __('Bobcares'));
        $resultPage->addBreadcrumb(__('Quote2Sales'), __('View Quotes'));

        //Set the header title.
        $resultPage->getConfig()->getTitle()->prepend(__('View Quotes'));

        return $resultPage;
    }

}
