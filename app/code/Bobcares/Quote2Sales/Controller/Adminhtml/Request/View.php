<?php

/**
 * Created on 14th Nov 2016.
 * Bobcares_Quote2Sales
 * Action controller for view.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Adminhtml\Request;

/**
 * Bobcares_Quote2Sales
 * Action controller for view
 * @category    Bobcares
 * @author      BDT
 */
class View extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();

        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('Bobcares_Quote2Sales::requests');

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Bobcares'), __('Bobcares'));
        $resultPage->addBreadcrumb(__('Quote2Sales'), __('View Request'));

        //Set the header title.
        $resultPage->getConfig()->getTitle()->prepend(__('View Request'));

        return $resultPage;
    }

}
