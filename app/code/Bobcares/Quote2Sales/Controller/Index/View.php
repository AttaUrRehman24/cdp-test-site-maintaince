<?php

/**
 * Created on 29th May 2017.
 * Bobcares Quote2Sales View Controller Action.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Controller\Index;

/**
 * Bobcares Quote2Sales View Controller Action.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class View extends \Magento\Customer\Controller\AbstractAccount {

    public function execute() {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
