<?php

/**
 * Created on 21st Oct 2016.
 * Bobcares_Quote2Sales
 * Admin grid Action for Quotes Display.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Bobcares\Quote2Sales\Block\Adminhtml\Quote2Sales\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;

/**
 * Bobcares_Quote2Sales
 * Admin grid Action for Quotes Display.
 * @category    Bobcares
 * @author      BDT
 */
class QuoteActions extends Column {

    /** Url path */
    const URL_PATH_INFO = 'quote2sales/quotes/info';

    /** @var UrlBuilder */
    protected $actionUrlBuilder;

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlBuilder $actionUrlBuilder
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
    ContextInterface $context, UiComponentFactory $uiComponentFactory, UrlBuilder $actionUrlBuilder, UrlInterface $urlBuilder, array $components = [], array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['quote_id'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl(self::URL_PATH_INFO, ['quote_id' => $item['quote_id']]),
                        'label' => __('View')
                    ];
                }
            }
        }
        return $dataSource;
    }

}
