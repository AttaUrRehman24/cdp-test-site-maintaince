<?php

/**
 * Bobcares Quote2Sales Block class.
 * @ category    Bobcares
 * @ package     Bobcares_Quote2Sales
 * @ author      BDT
 */

namespace Bobcares\Quote2Sales\Block;

/**
 * Bobcares Quote2Sales Block.
 * @ category    Bobcares
 * @ package     Bobcares_Quote2Sales
 * @ author      BDT
 */
class Requests extends \Magento\Framework\View\Element\Template {

    protected $quotesCollectionFactory;
    
    protected $request; 

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, 
    \Bobcares\Quote2Sales\Model\ResourceModel\Quote2Sales\CollectionFactory $quotesCollectionFactory, 
    \Bobcares\Quote2Sales\Model\Request $request,
    \Magento\Customer\Model\Session $session,
    \Bobcares\Quote2Sales\Model\RequestStatus $requestStatus,

    array $data = array()
    ) {
        parent::__construct($context, $data);
        $this->quotesCollectionFactory = $quotesCollectionFactory;
        $this->request = $request;
        $this->session = $session;
        $this->requestStatus = $requestStatus;

        $customer_id = $this->session->getCustomer()->getId(); 
        
        //get collection of data 
        $requests =$this->request->getCollection()
                  ->addFieldToFilter('customer_id', array('eq' => $customer_id));
        $this->setCollection($requests);
    }
            
    /**
     * Function for the pager.
     * @return $this
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                            'Magento\Theme\Block\Html\Pager', 'quote2sales.requests.history.pager'
                    )->setCollection(
                    $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }
        return $this;
    }

    /**
     * Function to call the model call for getting all the saved requests for the logged in user.
     * @return $requests
     */
    public function getRequestsCollection() {
        $customerId = $this->session->getId();
        $requests = $this->request->getRequests($customerId);
        return $requests;
    }

    /**
     * @return string
     * method for get pager html.
     */
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
    
    /**
     * Function to get customer session.
     * @return $session
     */
    public function getCustomerSession() {
        return $this->session;
    }
}
