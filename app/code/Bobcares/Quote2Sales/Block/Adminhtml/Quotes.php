<?php

/**
 * Updated on 13th April 2017.
 * Bobcares Quote2Sales Quotes Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml;

use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Magento\Eav\Model\AttributeDataFactory;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Bobcares Quote2Sales Quotes Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Quotes extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
    protected $request;
    protected $request1;
    protected $_order;
    
    /**
     * @var AddressRenderer
     */
    protected $addressRenderer;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * Group service
     *
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Bobcares\Quote2Sales\Model\QuotesFactory $db
     * @param \Bobcares\Quote2Sales\Model\RequestFactory $db1
     * @param \Magento\Sales\Model\Order $order
     * @param AddressRenderer $addressRenderer
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\ToOrderAddress $quoteToOrderAddress
     * @param \Magento\Quote\Model\Quote\Item\OptionFactory $quoteItemOption
     * @param array $data
     * @return type
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, 
    \Magento\Framework\Registry $registry, 
    \Magento\Framework\App\Request\Http $request, 
    \Bobcares\Quote2Sales\Model\QuotesFactory $db, 
    \Bobcares\Quote2Sales\Model\RequestFactory $db1, 
    \Magento\Sales\Model\Order $order, 
    \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
    \Magento\Sales\Helper\Admin $adminHelper,
    \Magento\Quote\Model\Quote $quoteFactory,
    \Bobcares\Quote2Sales\Model\Quotes $quote,
    \Bobcares\Quote2Sales\Controller\Adminhtml\Quotes\Create\Save $quoteSave,
    \Bobcares\Quote2Sales\Block\Adminhtml\Quote\View\Items $quoteItems,
    \Magento\Quote\Model\Quote\Address\ToOrderAddress $quoteToOrderAddress,
    \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
    \Magento\Customer\Model\Customer $customerSession,
    \Bobcares\Quote2Sales\Model\Email $quoteEmail,
    \Magento\Backend\Model\Session\Quote $sessionQuote,
    array $data = []
    ) {
        $this->quotes = $request;
        $this->_quoteFactory = $db;
        $this->_requestFactory = $db1;
        $this->request = $request;
        $this->order = $order;
        $this->quote = $quote;
        $this->coreRegistry = $registry;
        $this->_adminHelper = $adminHelper;
        $this->quoteFactory = $quoteFactory;
        $this->addressRenderer = $addressRenderer;
        $this->quoteSave = $quoteSave;
        $this->quoteItems = $quoteItems;
        $this->quoteToOrderAddressConverter = $quoteToOrderAddress;
        $this->groupRepository = $groupRepository;
        $this->customerSession = $customerSession;
        $this->quoteEmail = $quoteEmail;
        $this->sessionQuote = $sessionQuote;

        return parent::__construct($context,$registry,$adminHelper);
    }
    
    /**
     * Function to get quote details.
     * @return type
     */
    public function getQuotes() {

        $quoteId = $this->quotes->getParam('quote_id');
        $requestModelQuote = $this->quote->loadByQuoteId($quoteId);
        return $requestModelQuote;
    }
    
    /**
     *  Function to get the quotes collection
     * @return type
     */
    public function getQuoteFactory() {
       return $this->quoteFactory;
    }

    /** Function for getting the store name 
     * from the configuration.
     */

    public function getStoreName() {
        return $this->_storeManager->getStore()->getName();
    }    
    
    /**
     * Function to get customer group.
     * @return type
     */
    public function groupRepository() {

        return $this->groupRepository;
    }
    
    /**
     * Function to get customer session.
     * @return type
     */
    public function getCustomerSession() {
        return $this->customerSession;
    }

    /**
     * Get items html
     *
     * @return string
     */
    public function getItemsHtml() {
        return $this->getChildHtml('quote_items');

    }    

    /**
     * Returns string with formatted address
     *
     * @param Address $address
     * @return null|string
     */
    public function getFormattedAddress($address) {

        //Convert the quote address to order address incase the 
        //address is of type quote.
        if ($address instanceof \Magento\Quote\Model\Quote\Address) {
            $address = $this->quoteToOrderAddressConverter->convert($address);
        }

        //Display the exception in case sales order adress is not provided.
        if (!$address instanceof \Magento\Sales\Model\Order\Address) {
            throw new \Exception(__('Expected instance of \Magento\Sales\Model\Order\Address, got ' . get_class($address)));
        }

        return $this->addressRenderer->format($address, 'html');
    }
    
    /**
     * Get URL to edit the customer.
     *
     * @return string
     */
    public function getCustomerViewUrl() {
        
        $quote = $this->getQuotes();
        $customerId = $quote[0]['user_id'];

        $quotesFactory = $this->getQuoteFactory()->load($customerId);
        if ($quotesFactory->getCustomerIsGuest() || !$customerId) {
            return '';
        }

        return $this->getUrl('customer/index/edit', ['id' => $customerId]);
    }
    
    /**
     * @desc This function is used for redirecting the call
     * @return type call the mail function with the object id
     */
    public function getQuoteToOrderUrl() {
        
        // Fetching the current quote ID, assigning it to the default store,fetting its customer ID. Fetching a session quote object
        // and setting this quote as the current session quote.
        $_quotes = $this->getQuotes();
        $_customer = $this->getCustomerSession()->load($_quotes[0]['user_id']);
        $customerId = $_customer->getId();
        $this->sessionQuote->setQuoteId($this->quotes->getParam('quote_id'));

        //Generating the URL for the Magento's default order creation page, sending the customer_id as a parameter        
        return $this->getUrl('sales/order_create/index', ['customer_id' => $customerId]);
    }

}
