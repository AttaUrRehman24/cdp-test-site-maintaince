<?php
/**
 * Updated on 4th Nov 2017.
 * Bobcares Quote2Sales Quotes Items Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote;

/**
 * Updated on 4th Nov 2017.
 * Bobcares Quote2Sales Quotes Items Block.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Totals extends \Bobcares\Quote2Sales\Block\Quote\Totals {
    
    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
            
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->quotes = $request;
        $this->quoteFactory = $quoteFactory;

        parent::__construct($context, $registry, $request,$quoteFactory,$adminHelper);
    }

    /**
     * Format total value based on quote currency.
     *
     * @param \Magento\Framework\DataObject $total
     * @return string
     */
    public function formatValue($total) {
        if (!$total->getIsFormated()) {
            return $this->_adminHelper->displayPrices($this->getQuote(), $total->getBaseValue(), $total->getValue());
        }
        return $total->getValue();
    }

    /**
     * Initialize quote totals array
     *
     * @return $this
     */
    protected function _initTotals() {
        
        /**
         * Add subtotals
         */
        $this->_totals = [];
        $this->_totals['subtotal'] = new \Magento\Framework\DataObject(
                [
            'code' => 'subtotal',
            'value' => $this->getSource()->getSubtotal(),
            'base_value' => $this->getSource()->getBaseSubtotal(),
            'label' => __('Subtotal'),
                ]
        );

        /**
         * Add shipping
         */
        if (!$this->getSource()->getIsVirtual() && ((double) $this->getSource()->getShippingAmount() ||
                $this->getSource()->getShippingAddress()->getShippingDescription())
        ) {
            $this->_totals['shipping'] = new \Magento\Framework\DataObject(
                    [
                'code' => 'shipping',
                'value' => $this->getSource()->getShippingAddress()->getShippingAmount(),
                'base_value' => $this->getSource()->getShippingAddress()->getBaseShippingAmount(),
                'label' => __('Shipping & Handling'),
                    ]
            );
        }

        /**
         * Add discount
         */
        if ((double) $this->getSource()->getShippingAddress()->getDiscountAmount() != 0) {
            if ($this->getSource()->getShippingAddress()->getDiscountDescription()) {
                $discountLabel = __('Discount (%1)', $this->getSource()->getShippingAddress()->getDiscountDescription());
            } else {
                $discountLabel = __('Discount');
            }
            $this->_totals['discount'] = new \Magento\Framework\DataObject(
                    [
                'code' => 'discount',
                'value' => $this->getSource()->getShippingAddress()->getDiscountAmount(),
                'base_value' => $this->getSource()->getShippingAddress()->getBaseDiscountAmount(),
                'label' => $discountLabel,
                    ]
            );
        }

        /**
         * Add Grand total
         */
        $this->_totals['grand_total'] = new \Magento\Framework\DataObject(
                [
            'code' => 'grand_total',
            'strong' => true,
            'value' => $this->getSource()->getGrandTotal(),
            'base_value' => $this->getSource()->getBaseGrandTotal(),
            'label' => __('Grand Total'),
            'area' => 'footer',
                ]
        );

        return $this;
    }
}
