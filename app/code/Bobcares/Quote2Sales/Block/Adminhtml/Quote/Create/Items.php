<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\Create;

use Magento\Quote\Model\Quote\Item;
use Magento\Framework\App\ObjectManager;

/**
 * Adminhtml sales order create items block
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Items extends \Magento\Sales\Block\Adminhtml\Order\Create\Items {

    /**
     * Contains button descriptions to be shown at the top of accordion
     *
     * @var array
     */
    protected $_buttons = [];

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Sales\Model\AdminOrder\Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Model\Session\Quote $sessionQuote, \Magento\Sales\Model\AdminOrder\Create $orderCreate, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, array $data = []
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->_sessionQuote = $sessionQuote;
        $this->_orderCreate = $orderCreate;
        parent::__construct($context, $sessionQuote, $orderCreate, $priceCurrency);
    }

    /**
     * Define block ID
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('sales_order_create_items');
        $this->loadSelectedItemsToQuote();
    }

    /**
     * Accordion header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText() {
        return __('Items Quoted');
    }

    /**
     * Returns all visible items
     *
     * @return Item[]
     */
    public function getItems() {
        return $this->getQuote()->getAllVisibleItems();
    }

    /**
     * Add button to the items header
     *
     * @param array $args
     * @return void
     */
    public function addButton($args) {
        $this->_buttons[] = $args;
    }

    /**
     * Render buttons and return HTML code
     *
     * @return string
     */
    public function getButtonsHtml() {
        $html = '';
        
        // Make buttons to be rendered in opposite order of addition. This makes "Add products" the last one.
        $this->_buttons = array_reverse($this->_buttons);
        foreach ($this->_buttons as $buttonData) {
            $html .= $this->getLayout()->createBlock(
                            'Magento\Backend\Block\Widget\Button'
                    )->setData(
                            $buttonData
                    )->toHtml();
        }

        return $html;
    }

    /**
     * Return HTML code of the block
     *
     * @return string
     */
    protected function _toHtml() {
        if ($this->getStoreId()) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * @desc Function add the quote item while creating quote 
     */
    protected function loadSelectedItemsToQuote() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productIdToAdd = $objectManager->create('Bobcares\Quote2Sales\Model\Request')->getCollection()
                        ->addFieldToselect('product_id')
                        ->addFieldToFilter('request_id', ((int) $this->getRequest()->getParam('request_id')))
                        ->getFirstItem()->getData('product_id');

        /* If the product specific RFQ is genrated */
        if ($productIdToAdd) {
            $this->getQuote()->removeAllItems();
            $this->getQuote()->save();
            $product = $this->getModel('catalog/product')->load($productIdToAdd);
            $quoteItem = $this->getQuote()->addProduct($product);
            $this->getQuote()->collectTotals();
            $this->getQuote()->save();
        }
    }
}