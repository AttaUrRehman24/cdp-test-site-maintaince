<?php

/**
 * Added on 11th Oct 2017.
 * Bobcares Quote2Sales Order view messages.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Block\Adminhtml\Quote\View;

/**
 * Bobcares Quote2Sales Order view messages.
 * @category    Bobcares
 * @package     Bobcares_Quote2Sales
 * @author      BDT
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('quote2sales_quote_view_tabs');
        $this->setDestElementId('quote2sales_quote_view');
        $this->setTitle(__('Quotes View'));
    }

}
