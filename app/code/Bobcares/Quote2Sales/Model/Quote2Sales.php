<?php

/**
 * Quote2Sales Grid Model.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model;

/**
 * Quote2Sales Model
 *
 * @method \Bobcares\Quote2Sales\Model\Resource\Page _getResource()
 * @method \Bobcares\Quote2Sales\Model\Resource\Page getResource()
 */
class Quote2Sales extends \Magento\Framework\Model\AbstractModel {

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {


        $this->_init('Bobcares\Quote2Sales\Model\ResourceModel\Quote2Sales');
    }

}