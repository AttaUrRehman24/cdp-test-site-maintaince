<?php

/**
 * Updated on 14th April, 2017
 * Quote2Sales Resource Collection.
 * @category    Bobcares
 * @author      BDT
 */

namespace Bobcares\Quote2Sales\Model\ResourceModel\Quotes;

/**
 * Quote2Sales Resource Collection.
 * @category    Bobcares
 * @author      BDT
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct() {
       
        $this->_init('Bobcares\Quote2Sales\Model\Quotes', 'Bobcares\Quote2Sales\Model\ResourceModel\Quotes');
    }
}
