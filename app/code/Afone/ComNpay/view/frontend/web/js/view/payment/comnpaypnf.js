define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'comnpaypnf',
                component: 'Afone_ComNpay/js/view/payment/method-renderer/comnpaypnf-method'
            }
        );
        return Component.extend({});
    }
);