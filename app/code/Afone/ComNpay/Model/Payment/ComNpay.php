<?php


namespace Afone\ComNpay\Model\Payment;

class ComNpay extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "comnpay";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}