<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Megamenu
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Megamenu\Controller\Adminhtml\Megamenu;

use Magento\Framework\Controller\ResultFactory;

/**
 * Action Save
 */
class Save extends \Magestore\Megamenu\Controller\Adminhtml\Megamenu
{
    protected $_categoryFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magestore\Megamenu\Helper\Image $imageHelper,
        \Magento\Framework\App\CacheInterface $cacheInterface,
        \Magento\Framework\App\Cache\TypeListInterface $typeListInterface,
        \Magento\Config\Model\ResourceModel\Config $configResoure,
        \Magestore\Megamenu\Model\ResourceModel\Megamenu\CollectionFactory $collectionFactory,

        \Magento\Catalog\Model\Category $categoryFactory
    )
    {
        parent::__construct($context, $imageHelper, $cacheInterface, $typeListInterface, $configResoure, $collectionFactory);
        $this->_imageHelper =$imageHelper;
        $this->_cacheInterface = $cacheInterface;
        $this->_typeListInterface = $typeListInterface;
        $this->_configResoure = $configResoure;
        $this->_collectionFactory = $collectionFactory;

        $this->_categoryFactory = $categoryFactory;
    }

    public function getSubCategories($catId){
        $cat = $this->_categoryFactory->load($catId);
        $subcats = $cat->getChildren();
        $subcategories = array();
        foreach(explode(',', $subcats) as $subCatid){
            $_subCategory = $this->_categoryFactory->load($subCatid);
            if($_subCategory->getIsActive()) {
                $subcategories[] = array('id'=>$_subCategory->getId(),'name'=>$_subCategory->getName());
            }
        }
        return $subcategories;
    }

    public function getCategory($categoryId)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryData = $_objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);

        return $categoryData;
    }

    /**
     * Execute action
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($data = $this->getRequest()->getPostValue()) {
            /**
             * @var \Magestore\Megamenu\Model\Megamenu $model
             */
            $model = $this->_objectManager->create('Magestore\Megamenu\Model\Megamenu');
            if ($id = $this->getRequest()->getParam('megamenu_id')) {
                $model->load($id);
            }
            if (isset($data['stores']) && is_array($data['stores'])) {
                $data['stores'] = implode(',', $data['stores']);
            }
            $model->setData($data);

            try {
                $this->_imageHelper->mediaUploadImage(
                    $model,
                    'item_icon',
                    \Magestore\Megamenu\Model\Megamenu::MEGAMENU_ICON_PATH,
                    $makeResize = true
                );
                $this->_typeListInterface->cleanType('config');
                $this->_typeListInterface->cleanType('full_page');
                if(isset($data["categories"])) {
                    $data["categories"] = explode(', ', $data["categories"]);
                    $arrSubCategoriesID = array();
                    $arrSubCategoriesLevel = array();
                    $check = true;

                    foreach ($data["categories"] as $value){
                        $category = $this->getCategory($value);
                        $arrSubCategoriesLevel[] = $category->getLevel();
                    }
                    $minLevel = min($arrSubCategoriesLevel);
                    $maxLevel = max($arrSubCategoriesLevel);
                    if($maxLevel>$minLevel+4){
                        $check = false;
                    }

                    foreach ($data["categories"] as $value){
                        $category = $this->getCategory($value);
                        if($category["is_active"]==1) {
                            $check = $this->check($minLevel, $category, $data["categories"], $check);
                        } else {
                            $subCategories = $this->getSubCategories($value);
                            foreach($subCategories as $value){
                                $arrSubCategoriesID[] = $value["id"];
                            }
                            foreach ($arrSubCategoriesID as $errorID) {
                                if (in_array($errorID, $data["categories"])) {
                                    $check = false;
                                    break;
                                }
                            }
                        }
                    }
                    if($check != false) {
                        $model->save()->saveItem()->setConfigIds();
                        $this->_typeListInterface->cleanType('config');
                        $this->_typeListInterface->cleanType('full_page');

                        $this->messageManager->addSuccess(__('The item has been saved.'));
                        $this->_getSession()->setFormData(false);
                    } else {
                        $this->messageManager->addError(__('Something went wrong while saving the record.'));
                    }
                }

                if ($this->getRequest()->getParam('back') === 'edit') {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        [
                            'megamenu_id' => $model->getId(),
                            '_current' => true,
                        ]
                    );
                } elseif ($this->getRequest()->getParam('back') === 'new') {
                    return $resultRedirect->setPath(
                        '*/*/new',
                        ['_current' => true]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addException($e, __('Something went wrong while saving the record.'));
            }

            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath(
                '*/*/edit',
                ['megamenu_id' => $this->getRequest()->getParam('megamenu_id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }

    function check($minLevel, $category, $data, $check)
    {
        if ($category["level"] == $minLevel+1){
            $parentCategoryID = $category->getParentId();
            if (!in_array($parentCategoryID, $data)) {
                $check = false;
            }
        }
        if($category["level"] != $minLevel) {
            $parentCategoryID = $category->getParentId();
            if (!in_array($parentCategoryID, $data)) {
                $check = false;
            } else {
                $parentCategory = $this->getCategory($parentCategoryID);
                if($check == true) {
                    $this->check($minLevel, $parentCategory, $data, $check);
                }
            }
        }
        return $check;
    }

    public function xlog($message = 'null')
    {
        $log = print_r($message, true);
        \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Psr\Log\LoggerInterface')
            ->debug($log);
    }
}
