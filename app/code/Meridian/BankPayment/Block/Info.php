<?php
namespace Meridian\BankPayment\Block;

/**
 * Class Info
 * @package Meridian\BankPayment\Block
 */
class Info extends \Magento\Payment\Block\Info
{
    use \Meridian\BankPayment\Helper\Account;

    /**
     * @var string
     */
    protected $_template = 'info.phtml';

    /**
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('Meridian_BankPayment::info/pdf.phtml');
        return $this->toHtml();
    }

    /**
     * @return mixed
     */
    public function getAccounts()
    {
        return $this->getMethod()->getAccounts();
    }

    /**
     * @return mixed
     */
    public function getShowBankAccountsInPdf()
    {
        return $this->getMethod()->getConfigData('show_bank_accounts_in_pdf');
    }

    /**
     * @return mixed
     */
    public function getShowCustomTextInPdf()
    {
        return $this->getMethod()->getConfigData('show_customtext_in_pdf');
    }
}
