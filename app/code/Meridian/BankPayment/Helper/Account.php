<?php
namespace Meridian\BankPayment\Helper;


trait Account
{
    /**
     * @param \Magento\Framework\DataObject $account
     * @return bool
     */
    public function displayFullAccountData($account) {
        return ($this->displaySepaAccountData($account) && $this->displayNonSepaAccountData($account));
    }

    /**
     * @param \Magento\Framework\DataObject $account
     * @return bool
     */
    public function displayNonSepaAccountData($account) {
        return ($account->getAccountNumber() && $account->getSortCode());
    }

    /**
     * @param \Magento\Framework\DataObject $account
     * @return bool
     */
    public function displaySepaAccountData($account) {
        return ($account->getIban());
    }
}