<?php

namespace Meridian\BankPayment\Model\Config\Source;

/**
 * Class CmsPage
 * @package Meridian\BankPayment\Model\Config\Source
 */
class CmsPage extends \Magento\Cms\Model\Config\Source\Page
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            parent::toOptionArray();
            array_unshift(
                $this->options,
                [
                    'value' => '',
                    'label' => __('-- Please Select --')
                ]
            );
        }
        return $this->options;
    }
}
