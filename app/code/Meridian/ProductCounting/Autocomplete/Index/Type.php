<?php

/**
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Meridian\ProductCounting\Autocomplete\Index;

use Wyomind\Elasticsearch\Model\Index\TypeInterface;
use Magento\Framework\DataObject;

class Type extends \Wyomind\Elasticsearch\Autocomplete\Index\Type
{


    /**
     * {@inheritdoc}
     */
    public function validateResult(array $data)
    {

        switch ($this->code) {
            case 'product':
                return isset($data[\Wyomind\Elasticsearch\Helper\Config::PRODUCT_PRICES]) && isset($data['visibility']); // visible in catalog search
            default:
                return true;
        }
    }

}
