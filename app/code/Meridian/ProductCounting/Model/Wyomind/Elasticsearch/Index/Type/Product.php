<?php

namespace Meridian\ProductCounting\Model\Wyomind\Elasticsearch\Index\Type;


class Product extends \Wyomind\Elasticsearch\Model\Index\Type\Product
{

    public function validateResult(array $data, $catalog = false)
    {
        /**
         * @see \Magento\Catalog\Model\Product\Visibility
         */
        return isset($data[\Wyomind\Elasticsearch\Helper\Config::PRODUCT_PRICES]) && isset($data['visibility']);
    }
	
}
	
	