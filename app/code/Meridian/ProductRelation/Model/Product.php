<?php

namespace Meridian\ProductRelation\Model;

class Product extends \Magento\Catalog\Model\Product
{
    /**
     * Retrieve array of custom type products
     *
     * @return array
     */
    public function getCustomtypeProducts() 
    {
        if (!$this->hasCustomTypeProducts()) {
            $products = [];
            foreach ($this->getCustomTypeProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setCustomtypeProducts($products);
        }
        return $this->getData('customtype_products');
    }

    public function getBundletypeProducts()
    {
        if (!$this->hasBundleTypeProducts()) {
            $products = [];
            foreach ($this->getBundleTypeProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setBundletypeProducts($products);
        }
        return $this->getData('bundletype_products');
    }

    public function getCompareProducts()
    {
        if (!$this->hasCompareProducts()) {
            $products = [];
            foreach ($this->getCompareProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setCompareProducts($products);
        }
        return $this->getData('compare_products');
    }
    /**
     * Retrieve custom type products identifiers
     *
     * @return array
     */
    public function getCustomtypeIds() 
    {
        if (!$this->hasCustomtypeProductIds()) {
            $ids = [];
            foreach ($this->getCustomtypeProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setCustomtypeProductIds($ids);
        }
        return $this->getData('customtype_product_ids');
    }

    public function getBundletypeIds()
    {
        if (!$this->hasBundletypeProductIds()) {
            $ids = [];
            foreach ($this->getBundletypeProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setBundletypeProductIds($ids);
        }
        return $this->getData('bundletype_product_ids');
    }

    public function getCompareIds()
    {
        if (!$this->hasCompareProductIds()) {
            $ids = [];
            foreach ($this->getCompareProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setCompareProductIds($ids);
        }
        return $this->getData('compare_product_ids');
    }
    /**
     * Retrieve collection custom type product
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getCustomTypeProductCollection() 
    {
        $collection = $this->getLinkInstance()->useCustomtypeLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    public function getBundleTypeProductCollection()
    {
        $collection = $this->getLinkInstance()->useBundletypeLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    public function getCompareProductCollection()
    {
        $collection = $this->getLinkInstance()->useCompareLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }
    /**
     * Retrieve collection custom type link
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Collection
     */
    public function getCustomTypeLinkCollection() 
    {
        $collection = $this->getLinkInstance()->useCustomtypeLinks()->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

    public function getBundleTypeLinkCollection()
    {
        $collection = $this->getLinkInstance()->useBundletypeLinks()->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

    public function getCompareLinkCollection()
    {
        $collection = $this->getLinkInstance()->useCompareLinks()->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }
}