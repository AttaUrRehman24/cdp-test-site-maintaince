<?php

namespace Meridian\ProductRelation\Model\Catalog\Product;

class Link extends \Magento\Catalog\Model\Product\Link
{
    const LINK_TYPE_CUSTOMTYPE = 6;
    const LINK_TYPE_BUNDLETYPE = 7;
    const LINK_TYPE_COMPARE = 8;

    /**
     * @return \Magento\Catalog\Model\Product\Link $this
     */
    public function useCustomtypeLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_CUSTOMTYPE);
        return $this;
    }

    public function useBundletypeLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_BUNDLETYPE);
        return $this;
    }

    public function useCompareLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_COMPARE);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Model\Product\Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getCustomtypeData();
        if (!is_null($data)) {
            $this->_getResource()->saveProductLinks($product->getId(), $data, self::LINK_TYPE_CUSTOMTYPE);
        }
        $dataBundle = $product->getBundletypeData();
        if (!is_null($dataBundle)) {
            $this->_getResource()->saveProductLinks($product->getId(), $dataBundle, self::LINK_TYPE_BUNDLETYPE);
        }
        $dataCompare = $product->getCompareData();
        if (!is_null($dataCompare)) {
            $this->_getResource()->saveProductLinks($product->getId(), $dataCompare, self::LINK_TYPE_COMPARE);
        }
    }
}
