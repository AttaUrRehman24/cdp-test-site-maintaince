<?php

namespace Meridian\ProductRelation\Ui\DataProvider\Product\Form\Modifier;

use Magento\Ui\Component\Form\Fieldset;

class Related extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\Related
{
    const DATA_SCOPE_CUSTOMTYPE = 'customtype';
    const DATA_SCOPE_BUNDLETYPE = 'bundletype';
    const DATA_SCOPE_COMPARE = 'compare';

    /**
     * @var string
     */
    private static $previousGroup = 'search-engine-optimization';

    /**
     * @var int
     */
    private static $sortOrder = 90;
    private static $sortCustomType = 90;
    private static $sortBundleType = 100;
    private static $sortCompare = 110;

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                static::GROUP_RELATED => [
                    'children' => [
                        $this->scopePrefix . static::DATA_SCOPE_RELATED => $this->getRelatedFieldset(),
                        $this->scopePrefix . static::DATA_SCOPE_UPSELL => $this->getUpSellFieldset(),
                        $this->scopePrefix . static::DATA_SCOPE_CROSSSELL => $this->getCrossSellFieldset(),
                        $this->scopePrefix . static::DATA_SCOPE_CUSTOMTYPE => $this->getCustomTypeFieldset(),
                        $this->scopePrefix . static::DATA_SCOPE_BUNDLETYPE => $this->getBundleTypeFieldset(),
                        $this->scopePrefix . static::DATA_SCOPE_COMPARE => $this->getCompareTypeFieldset()
                    ],
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Related Products, Up-Sells, Cross-Sells, Bundle Products, Compare Products'),
                                'collapsible' => true,
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::DATA_SCOPE,
                                'sortOrder' =>
                                    $this->getNextGroupSortOrder(
                                        $meta,
                                        self::$previousGroup,
                                        self::$sortOrder
                                    ),
                            ],
                        ],

                    ],
                ],
            ]
        );

        return $meta;
    }

    /**
     * Prepares config for the Custom type products fieldset
     *
     * @return array
     */
    protected function getCustomTypeFieldset()
    {
        $content = __(
            'Bundle featured products are shown to customers in addition to the item the customer is looking at.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Featured Products'),
                    $this->scopePrefix . static::DATA_SCOPE_CUSTOMTYPE
                ),
                'modal' => $this->getGenericModal(
                    __('Add Featured Products'),
                    $this->scopePrefix . static::DATA_SCOPE_CUSTOMTYPE
                ),
                static::DATA_SCOPE_CUSTOMTYPE => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_CUSTOMTYPE),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Bundle Featured Products'),
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => self::$sortCustomType,
                    ],
                ],
            ]
        ];
    }

    protected function getBundleTypeFieldset()
    {
        $content = __(
            'Bundle products are shown to customers in addition to the item the customer is looking at.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Products'),
                    $this->scopePrefix . static::DATA_SCOPE_BUNDLETYPE
                ),
                'modal' => $this->getGenericModal(
                    __('Add Products'),
                    $this->scopePrefix . static::DATA_SCOPE_BUNDLETYPE
                ),
                static::DATA_SCOPE_BUNDLETYPE => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_BUNDLETYPE),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Bundle Products'),
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => self::$sortBundleType,
                    ],
                ],
            ]
        ];
    }

    protected function getCompareTypeFieldset()
    {
        $content = __(
            'Compare products are shown to customers in addition to the item the customer is looking at.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Products'),
                    $this->scopePrefix . static::DATA_SCOPE_COMPARE
                ),
                'modal' => $this->getGenericModal(
                    __('Add Products'),
                    $this->scopePrefix . static::DATA_SCOPE_COMPARE
                ),
                static::DATA_SCOPE_COMPARE => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_COMPARE),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Compare Products'),
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => self::$sortCompare,
                    ],
                ],
            ]
        ];
    }

    /**
     * Retrieve all data scopes
     *
     * @return array
     */
    protected function getDataScopes()
    {
        return [
            static::DATA_SCOPE_RELATED,
            static::DATA_SCOPE_CROSSSELL,
            static::DATA_SCOPE_UPSELL,
            static::DATA_SCOPE_CUSTOMTYPE,
            static::DATA_SCOPE_BUNDLETYPE,
            static::DATA_SCOPE_COMPARE
        ];
    }
}