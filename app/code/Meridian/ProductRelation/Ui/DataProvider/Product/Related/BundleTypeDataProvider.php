<?php

namespace Meridian\ProductRelation\Ui\DataProvider\Product\Related;

use Magento\Catalog\Ui\DataProvider\Product\Related\AbstractDataProvider;

/**
 * Class BundleTypeDataProvider
 */
class BundleTypeDataProvider extends AbstractDataProvider
{
    /**
     * {@inheritdoc
     */
    protected function getLinkType()
    {
        return 'bundletype';
    }
}
