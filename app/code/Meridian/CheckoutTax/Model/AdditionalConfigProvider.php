<?php
namespace Meridian\CheckoutTax\Model;
use \Magento\Framework\Session\SessionManagerInterface;
use \Magento\Framework\Registry;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Framework\Pricing\Helper\Data as PriceHelper;

class AdditionalConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var SessionManagerInterface
     */
    private $session;

    private $registry;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var PriceHelper
     */
    private $priceHelper;

    public function __construct(
        SessionManagerInterface $session,
        Registry $registry,
        CheckoutSession $checkoutSession,
        PriceHelper $priceHelper
    ) {
        $this->priceHelper = $priceHelper;
        $this->session = $session;
        $this->registry = $registry;
        $this->checkoutSession = $checkoutSession;
    }

    public function getConfig()
    {
        $output['tax_amount_onestep'] = $this->priceHelper->currency($this->checkoutSession->getQuote()->getShippingAddress()->getTaxAmount(), true, false);
        $output['tax_display'] = $this->session->getTaxDisplay();
        return $output;
    }
}