<?php

namespace Meridian\Video\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\ObserverInterface;

class Video implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    protected $_uploaderFactory;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $_logger;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    private $context;

    /**
     * Construct
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->context = $context;
        $this->request = $request;
    }

    /**
     * Generate urls for UrlRewrite and save it in storage
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();

        $files = $this->request->getFiles('product');
        $productRequest = $this->request->getParam('product');

        if (isset($productRequest['video_preview'])) {
            $videoPreview = $productRequest['video_preview'];
            if (isset($videoPreview[0]['name']) && $videoPreview[0]['name'] != $product->getVideoPreview()) {
                $product->setData('video_preview', $videoPreview[0]['name']);
                $product->getResource()->saveAttribute($product, 'video_preview');
            }
        } else {
            $product->setData('video_preview', '');
            $product->getResource()->saveAttribute($product, 'video_preview');
        }

        if (isset($productRequest['video'])) {
            $video = $productRequest['video'];
            if (isset($video[0]['name']) && $video[0]['name'] != $product->getVideo()) {
                $product->setData('video', $video[0]['name']);
                $product->getResource()->saveAttribute($product, 'video');
            }
        } else {
            $product->setData('video', '');
            $product->getResource()->saveAttribute($product, 'video');
        }

//        $attributes = ['video', 'video_preview'];
//
//        foreach ($attributes as $item) {
//            if (isset($files[$item])) {
//                $value = $files[$item];
//                if (empty($value) && empty($_FILES)) {
//                    continue;
//                }
//
//                if (is_array($value) && !empty($value['delete'])) {
//                    $product->setData($item, '');
//                    $product->getResource()->saveAttribute($product, $item);
//
//                    continue;
//                }
//
//                $path = $this->_filesystem->getDirectoryRead(
//                    DirectoryList::MEDIA
//                )->getAbsolutePath(
//                    'catalog/product/' . $item . '/'
//                );
//
//                $file = $files[$item];
//                try {
//                    /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
//                    $uploader = $this->_fileUploaderFactory->create(['fileId' => $file]);
////            $uploader->setAllowedExtensions(['pdf']);
//                    $uploader->setAllowRenameFiles(true);
//                    $result = $uploader->save($path);
//                    $this->_logger->info($result['file']);
//                    $product->setData($item, $result['file']);
//                    $product->getResource()->saveAttribute($product, $item);
//                } catch (\Exception $e) {
//                    if ($e->getCode() != \Magento\MediaStorage\Model\File\Uploader::TMP_NAME_EMPTY) {
//                        $this->_logger->critical($e);
//                    }
//                }
//            }
//        }
    }

    protected function getFileData($files,$code)
    {
        $file = [];
        foreach ($files as $key => $value) {
            if (isset($value[$code])) {
                $file[$key] = $value[$code];
            }
        }

        return $file;
    }
}
