<?php

namespace Meridian\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Slider extends Template implements BlockInterface
{

    protected $_template = "widget/slider.phtml";

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    protected $_blockModel;
    protected $_dataFilterHelper;
    protected $_layout;

    public function __construct(
        Template\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Cms\Model\Block $blockModel,
        \Meridian\Widgets\Helper\Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        // used singleton (instead factory) because there exist dependencies on \Magento\Cms\Helper\Page
        $this->_filterProvider = $filterProvider;
        $this->_blockModel = $blockModel;
        $this->_dataFilterHelper = $dataHelper;
        $this->_layout = $context->getLayout();
    }

    public function contentToHtml($html = null)
    {
        if(is_null($html)) return;
        $html = $this->_filterProvider->getPageFilter()->filter($html);
        return $html;
    }
    public function getConfig($key, $default = NULL){
        if($this->hasData($key)){
            return $this->getData($key);
        }
        return $default;
    }
    public function getDataFilterHelper() {
        return $this->_dataFilterHelper;
    }
    public function getLayout() {
        return $this->_layout;
    }

    public function getData($key = '', $index = null)
    {
        if ('' === $key) {
            $data = $this->_dataFilterHelper->decodeWidgetValues($this->_data);
        } else {
            $data = parent::getData($key, $index);
            if (is_scalar($data)) {
                $data = $this->_dataFilterHelper->decodeWidgetValues($data);
            }

            $data = $this->_filterProvider->getPageFilter()->filter($data);
        }

        return $data;
    }

    public function getMediaUrl($media){
        $url = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
        return $url.$media;
    }
}
