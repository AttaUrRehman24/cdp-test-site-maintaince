<?php

namespace Meridian\Base\Controller\Download;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class Pdf extends \Magento\Catalog\Controller\Product
{
    /**
     * @var \Magento\Catalog\Helper\Product\View
     */
    protected $viewHelper;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    public function __construct(
        Context $context,
        \Magento\Catalog\Helper\Product\View $viewHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        FileFactory $fileFactory,
        DirectoryList $directoryList
    ) {
        $this->viewHelper = $viewHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->fileFactory = $fileFactory;
        $this->directoryList    = $directoryList;
        parent::__construct($context);
    }

    public function execute()
    {
        // Get initial data from request
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId = (int) $this->getRequest()->getParam('id');
        $result = null;

        if ($productId) {
            $product = $this->_initProduct();
            if (!$product) {
                return $this->noProductRedirect();
            }

            $pdf = $product->getData('pdf_file');

            if (!empty($pdf)) {
                $link = file_get_contents($this->directoryList->getPath('media') . '/catalog/product/pdf/' . $pdf);
                $result = $this->fileFactory->create(
                    $pdf,
                    $link,
                    DirectoryList::MEDIA,
                    'application/pdf'
                );
            }
        }
        return $result;
    }
}