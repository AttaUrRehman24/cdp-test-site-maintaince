<?php

namespace Meridian\Base\Controller\Elasticsearch;

use Wyomind\Elasticsearch\Autocomplete\Config;
use Wyomind\Elasticsearch\Autocomplete\Search;
use Wyomind\Elasticsearch\Autocomplete\Index\Type;
use Wyomind\Elasticsearch\Model\Client;
use Wyomind\Elasticsearch\Model\QueryBuilder;
use Elasticsearch\ClientBuilder;
use Psr\Log\NullLogger;

class Result extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    private $config;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Config $config
    )
    {
        $this->config = $config;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = [];
        $q = $this->getRequest()->getParam('q') ? $this->getRequest()->getParam('q') : '';
        $found = false;

        $enableDebugMode = 0;
        if($q){
            try {
                $store = $this->getRequest()->getParam('store') ? $this->getRequest()->getParam('store') : '';

                try {
                    $configHandler = new Config\JsonHandler($store);
                    $config = new Config($configHandler->load());

                } catch (\Exception $e) {
                    $enableDebugMode = 1;
                    throw $e;
                }

                if (!$config->getData()) {
                    $enableDebugMode = 1;
                    throw new \Exception('Could not find config for autocomplete');
                }

                $enableDebugMode = $config->getEnableDebugMode();

                $client = new Client(new ClientBuilder, $config, new NullLogger());
                $search = new Search($client, $config->getCompatibility());

                $typesConfig = $config->getTypes();
                foreach ($typesConfig as $code => $settings) {
                    if ($code == "doyoumean") {
                        continue;
                    }
                    try {
                        if ($settings['enable_autocomplete'] == 1) {
                            $type = new Type($code, $settings);
                            $params = (new QueryBuilder($config))->build($q, $type);
                            $index = $client->getIndexAlias($store, $code);
                            list($docs, $suggests) = $search->query($index, $type, $params);
                            $limit = $settings['autocomplete_limit'];
                            $result[$code] = [
                                'count' => count($docs),
                                'docs' => array_slice($docs, 0, $limit),
                                'enabled' => $settings['enable_autocomplete'] && ($code == "product" || ($code != "product" && $settings['enable']))
                            ];
                            if ($code == "product" && $typesConfig['doyoumean']['enable_autocomplete']) {
                                $result["suggests"] = [
                                    'count' => count($suggests),
                                    'docs' => $suggests,
                                    'enabled' => $typesConfig['doyoumean']['enable_autocomplete']
                                ];
                            } else if (!$typesConfig['doyoumean']['enable_autocomplete']) {
                                $result["suggests"] = [
                                    'count' => 0,
                                    'docs' => [],
                                    'enabled' => 0
                                ];
                            }
                        }
                    } catch (\Exception $e) {
                        // Ignore results of current type if an exception is thrown when searching
                        $result[$code] = [];
                        if ($enableDebugMode) {
                            $result["error"][] = $e->getMessage();
                        }
                    }
                }

                $found = true;
                $this->getResponse()->setBody(json_encode($result));
            } catch (\Exception $e) {
                if ($enableDebugMode) {
                    $result["error"][] = $e->getMessage();
                }
                $result["suggests"] = [ 'count' => 0];
                $result["product"] = [ 'count' => 0];
                $result["category"] = [ 'count' => 0];
                $result["cms"] = [ 'count' => 0];
                $this->getResponse()->setBody(json_encode($result));
            }
        }
        $this->getResponse()->setBody(json_encode($result));
    }
}