<?php

namespace Meridian\Base\Helper;

use Magento\Framework\Filesystem\File\ReadFactory;
use Magento\Framework\Filesystem\DriverPool;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Url;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const START_RANGE_INT = 500;
    const END_RANGE_INT = 3000;
    const CATEGORY_ID_MARGUES = 122;
    const GROUP_PARTICULIER = 1;
    const GROUP_PROFESSIONAL = 5;
    const GROUP_GUEST = 6;
    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /**
     * Tax config
     *
     * @var \Magento\Tax\Model\Config
     */
    private $taxConfig;
    protected $_bundleProdcutType;
    protected $_budndleOption;
    protected $_pricehelper;
    protected $_productFactory;
    protected $_stockRegistryInterface;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_configurableType;

    protected $request;
    /**
     * @var \Magento\Framework\Registry $_registry
     */
    protected $_registry;

    /**
     * \Magento\Catalog\Model\CategoryFactory $_categoryFactory
     */
    protected $_categoryFactory;

    /**
     * Review resource model
     *
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewsColFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $_sessionManager;

    /**
     * URL instance
     *
     * @var \Magento\Framework\UrlFactory
     */
    protected $urlFactory;

    /** @var \Magento\Catalog\Model\Config */
    protected $_catalogConfig;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    protected $productUrlPathGenerator;

    protected $_cookieHelper;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Bundle\Model\Product\Type $bundleProdcutType,
        \Magento\Bundle\Model\Option $budndleOption,
        \Magento\Framework\Pricing\Helper\Data $price,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Framework\UrlFactory $urlFactory,
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Meridian\Base\Helper\Cookie $cookieHelper
    )
    {
        $this->_storeManager = $storeManager;
        $this->_bundleProdcutType = $bundleProdcutType;
        $this->_budndleOption = $budndleOption;
        $this->_pricehelper = $price;
        $this->_productFactory = $productFactory;
        $this->_stockRegistryInterface = $stockRegistryInterface;
        $this->_cartHelper = $cartHelper;
        $this->taxCalculation = $taxCalculation;
        $this->_configurableType = $configurableType;
        $this->request = $context->getRequest();
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_reviewsColFactory = $collectionFactory;
        $this->_customerSession = $customerSession;
        $this->_sessionManager = $sessionManager;
        $this->urlFactory = $urlFactory;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->_cookieHelper = $cookieHelper;
        $this->_catalogConfig = $catalogConfig;

        parent::__construct($context);
    }

    /**
     * Return brand config value by key and store
     *
     * @param string $key
     * @param \Magento\Store\Model\Store|int|string $store
     * @return string|null
     */
    public function getConfig($key, $group = "store_information", $store = null)
    {
        $store = $this->_storeManager->getStore($store);
        $websiteId = $store->getWebsiteId();

        $result = $this->scopeConfig->getValue(
            $group . '/' . $key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store);
        return $result;
    }

    /**
     * @param $path
     * @param int $website
     * @param string $scope
     * @return mixed
     */
    public function getWebsiteConfig($path, $website = 0, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE)
    {
        return $this->scopeConfig->getValue(
            $path,
            $scope,
            $website
        );
    }

    /**
     * @param $path
     * @param null $store
     * @param string $scope
     * @return mixed
     */
    public function getStoreConfig($path, $store = null, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        $store = $this->_storeManager->getStore($store);
        return $this->scopeConfig->getValue(
            $path,
            $scope,
            $store
        );
    }

    /**
     *
     * Get Current price display option option
     *
     * */

    public function currentPriceDisplayType()
    {
        $current = $this->getTaxConfig()->getPriceDisplayType();

        if ($current == \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX) {
            return 'INC_TAX';
        } elseif ($current == \Magento\Tax\Model\Config::DISPLAY_TYPE_EXCLUDING_TAX) {
            return 'EXC_TAX';
        } else {
            return 'EXC_TAX';
        }
    }

    /**
     * Get the Tax Config.
     * In a future release, will become a constructor parameter.
     *
     * @return \Magento\Tax\Model\Config
     *
     * @deprecated 100.1.0
     */
    private function getTaxConfig()
    {
        if ($this->taxConfig === null) {
            $this->taxConfig = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Tax\Model\Config::class
            );
        }
        return $this->taxConfig;
    }

    /**
     * @param null $product
     * @return array|void
     */
    public function getBrandData($product = null)
    {
        if (is_null($product)) return;
        $categories = $product->getCategoryCollection()->addAttributeToSelect('*'); //
        foreach ($categories as $category) {
            if ($category->getParentId() == self::CATEGORY_ID_MARGUES) {
                $dataArray = ['ImageUrl' => $category->getImageUrl(), 'categoryName' => $category->getName(), 'url' => $category->getUrl(), 'product_delivery_time' => $category->getData('product_delivery_time'), 'product_delivery_info_message' => $category->getData('product_delivery_info_message'), 'garantie' => $category->getData('garantie_description'), 'image_background' => $category->getData('image_background')];
                return $dataArray;
            }
        }
    }

    /**
     * @param null 
     * @return string
     */
    public function getBrandName()
    {
        $product = $this->_productFactory->create();
        $final_product = $product->loadByAttribute('sku', $this->getProductSKU());
        $brand = $final_product->getdata('manufacturer');
        $attr = $final_product->getResource()->getAttribute('manufacturer');
        if ($attr->usesSource()) 
        {
            $brand_name = $attr->getSource()->getOptionText($brand);
            return $brand_name;
        }
        else
        return;
    }


    /**
     * @param null 
     * @return string
     */
     public function getProductNameBySeo()
    {
        $product = $this->_productFactory->create();
        $final_product = $product->loadByAttribute('sku', $this->getProductSKU());
        $brand = $this->getBrandName();
        $seo_name_1 = $final_product->getdata('seo_name_1');
        $seo_name_2 = $final_product->getdata('seo_name_2');
        $sku = $final_product->getdata('sku');
        if(is_null($seo_name_1))
        {
            return 1;
        }
        else
        {
            return $seo_name_1 ." ". $brand ." ". $seo_name_2 ." ". $sku ;
        }
            
    } 

    /**
     * @param null 
     * @return string
     */
    public function getProductNameSku()
    {
        $product = $this->_productFactory->create();
        $final_product = $product->loadByAttribute('sku', $this->getProductSKU());
        $brand = $this->getBrandName();
        $product_name = $final_product->getName();
        $sku = $final_product->getdata('sku');
        $seo_name_1 = $final_product->getdata('seo_name_1');
        $seo_name_2 = $final_product->getdata('seo_name_2');
        if(is_null($seo_name_1))
        {
            return $product_name." ". $sku;
        }
        else
        {
            return $seo_name_1 ." ". $brand ." ". $seo_name_2 ." ". $sku ;
        }
            
    } 

    /**
     * @param $product
     * @return string
     */
    public function getProductDeliveryTime($product)
    {
        if ($product->getData('product_delivery_time')) {
            return $product->getData('product_delivery_time');
        } else {
            $categories = $product->getCategoryCollection()->addAttributeToSelect('*'); //
            foreach ($categories as $category) {
                if ($category->getParentId() == 122 && $category->getData('product_delivery_time')) {
                    return $category->getData('product_delivery_time');
                }
            }
        }

        return '';
    }

    /**
     * @param null $product
     * @return array|void
     */
    public function getUpSellProducts($product = null)
    {
        if (is_null($product)) return;
        $upSellProductsData = [];
        $upSellProducts = $product->getUpSellProducts();
        if (!empty($upSellProducts)) {

            foreach ($upSellProducts as $upSellProduct) {
                if ($upSellProduct->getTypeId() == 'bundle') {
                    $data = [];
                    $attributeSetData = [];
                    $data['id'] = $upSellProduct->getId();
                    $data['is_salable'] = $upSellProduct->getIsSalable();
                    $data['featured'] = $this->getProductDetail($upSellProduct->getId())->getData('featured_product_bundles');
                    $selections = $this->_bundleProdcutType->getSelectionsCollection($this->_bundleProdcutType->getOptionsIds($upSellProduct), $upSellProduct);
                    foreach ($selections as $selection) {
                        $attributeSetData[] = $selection->getData();
                    }
                    $data['attributeSetData'] = $attributeSetData;
                    $upSellProductsData[$upSellProduct->getId()] = $data;

                }
            }
        }
        usort($upSellProductsData, [$this, 'sortByFeatured']);
        return $upSellProductsData;
    }

    /**
     * @param null $product
     * @return array|void
     */
    public function getBundleProducts($product = null)
    {
        if (is_null($product)) return;
        $upSellProductsData = [];
        if ($product->getTypeId() == 'bundle') {
            $data = [];
            $attributeSetData = [];
            $data['id'] = $product->getId();
            $data['is_salable'] = $product->getIsSalable();
            $data['featured'] = $this->getProductDetail($product->getId())->getData('featured_product_bundles');
            $selections = $this->_bundleProdcutType->getSelectionsCollection($this->_bundleProdcutType->getOptionsIds($product), $product);
            foreach ($selections as $selection) {
                $attributeSetData[] = $selection->getData();
            }
            $data['attributeSetData'] = $attributeSetData;
            $upSellProductsData[$product->getId()] = $data;
        }
        usort($upSellProductsData, [$this, 'sortByFeatured']);
        return $upSellProductsData;
    }

    /**
     * @param null $product
     * @return array|void
     */
    public function getBundleSimpleProducts($product = null)
    {
        if (is_null($product)) return;
        $upSellProductsData = [];
        if ($product->getTypeId() == 'bundle') {
            $selections = $this->_bundleProdcutType->getSelectionsCollection($this->_bundleProdcutType->getOptionsIds($product), $product);
            foreach ($selections as $selection) {
                $upSellProductsData[] = $this->getProductDetail($selection->getId());
            }
        }
        return $upSellProductsData;
    }

    public function isBundleProduct($product)
    {
        return $product->getTypeId() === \Magento\Bundle\Model\Product\Type::TYPE_CODE;
    }

    /**
     * @param $selectionsId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOptionsItem($selectionsId)
    {
        $_item = $this->getProductDetail($selectionsId);
        $store_id = $this->_storeManager->getStore()->getId();
        $options = $this->_budndleOption->getResourceCollection()->setProductIdFilter($_item->getId())->setPositionOrder();
        $options->joinValues($store_id);
        $_selections = $_item->getTypeInstance(true)->getSelectionsCollection($_item->getTypeInstance(true)->getOptionsIds($_item), $_item);
        return ['options' => $options, 'selections' => $_selections, 'product_item' => $_item];
    }

    /**
     * @param $number
     * @param int $decimal
     * @return float|string
     */
    public function currencyFormate($number, $decimal = 2)
    {
        return $this->_pricehelper->currency($number, true, false);
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getProductDetail($product_id)
    {
        return $this->_productFactory->create()->load($product_id);
    }

    /**
     * @param $product
     * @return float
     */
    public function getProductQty($product)
    {
        $stockitem = $this->_stockRegistryInterface->getStockItem(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );

        return $stockitem->getQty();
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    public function sortByFeatured($a, $b)
    {
        return $b['featured'] - $a['featured'];
    }

    /**
     * @param $value
     * @return bool
     */
    public function checkInstalmentRange($value)
    {
        $start_range = self::START_RANGE_INT;
        $end_range = self::END_RANGE_INT;
        if ($start_range < $value && $value < $end_range) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cart Url helper
     * @param $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrlHelper($product, $additional = [])
    {
        $addUrlKey = \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED;
        $addUrlValue = $this->_urlBuilder->getUrl('*/*/*', ['_use_rewrite' => true, '_current' => true]);
        $additional[$addUrlKey] = $this->urlEncoder->encode($addUrlValue);

        return $this->_cartHelper->getAddUrl($product, $additional);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getPriceInclAndExclTax(\Magento\Catalog\Model\Product $product)
    {

        $idsConfigurable = $product->getAssociatedProductIds();
        if ($product->getTypeId() === 'configurable' && $idsConfigurable === null) {
            $productsConfigurable = $this->_configurableType->getUsedProducts($product);
            $configurableChild = array_shift($productsConfigurable);
            if ($configurableChild) {
                $product = $configurableChild;
            } else {
                $product = $this->getProductWithMinimalPrice($product);
            }
        } elseif ($product->getTypeId() === 'bundle') {
            return [];
        }

        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);

            if ((int)$this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                $priceExcludingTax = $product->getPrice() / (1 + ($rate / 100));
            } else {
                // Product price in catalog is excluding tax.
                if ($product->getSpecialPrice()) {
                    $priceExcludingTax = $product->getPrice();
                } else {
                    $priceExcludingTax = $product->getFinalPrice();
                }
            }

            $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));

            return [
                'incl' => $priceIncludingTax,
                'excl' => $priceExcludingTax
            ];
        }

        return [];
    }

    public function getPriceInclTax($product, $price)
    {
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);

            if ((int)$this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                $priceExcludingTax = $price / (1 + ($rate / 100));
            } else {
                // Product price in catalog is excluding tax.
                $priceExcludingTax = $price;
            }

            $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));

            return $priceIncludingTax;
        }

        return $price;
    }

    public function getProductWithMinimalPrice($product)
    {
        $resultProduct = $product;

        $relatedCollection = $product->getCompareProductCollection()
            ->setPositionOrder()
            ->addStoreFilter();
        $relatedCollection->addAttributeToSelect(
            $this->_catalogConfig->getProductAttributes()
        );
        $relatedCollection->addMinimalPrice()->addTaxPercents();
        if ($relatedCollection->count()) {
            $i = 0;
            foreach ($relatedCollection as $item) {
                if ($i == 0 && (int)$resultProduct->getPrice() == 0) {
                    $resultProduct = $item;
                }
                if (($resultProduct->getFinalPrice() == 0) || $resultProduct->getFinalPrice() > $item->getFinalPrice()) {
                    $resultProduct = $item;
                }
                $i++;
            }
        }

        return $resultProduct;
    }

    public function getRelatedCollection($product)
    {
        $relatedCollection = $product->getCompareProductCollection()
            ->setPositionOrder()
            ->addStoreFilter();
        $relatedCollection->addAttributeToSelect(
            $this->_catalogConfig->getProductAttributes()
        );
        $relatedCollection->addMinimalPrice()->addTaxPercents();
        return $relatedCollection;
    }

    public function getProductMinimalPrice($product, $currentPirceDisplay = null)
    {
        $productMinimalPrice = $this->getProductWithMinimalPrice($product);
        $priceResult = $productMinimalPrice->getFinalPrice();
        if ($productMinimalPrice->getSpecialPrice()) {
            $priceResult = $productMinimalPrice->getPrice();
        }

        if ($currentPirceDisplay == 'INC_TAX') {
            $prices = $this->getPriceInclAndExclTax($productMinimalPrice);
            if (isset($prices['incl'])) {
                $priceResult = $prices['incl'];
            }
        }

        return $priceResult;
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getMinPriceConfigurable($product)
    {

        $minPrice = $product->getPrice();

        $relatedCollection = $product->getCompareProductCollection()
            ->setPositionOrder()
            ->addStoreFilter();
        $relatedCollection->addAttributeToSelect(
            $this->_catalogConfig->getProductAttributes()
        );
        $relatedCollection->addMinimalPrice()->addTaxPercents();
        if ($relatedCollection->count()) {
            $minPrice = $relatedCollection->getMinPrice();
        }

        return $minPrice;
    }

    /**
     * @param null $product
     * @param $price
     * @return array
     */
    public function getBundlePriceInclAndExclTax($product = null, $price)
    {
        if (is_null($product)) return [];
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);

            if ((int)$this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                $priceExcludingTax = $price / (1 + ($rate / 100));
            } else {
                // Product price in catalog is excluding tax.
                $priceExcludingTax = $price;
            }

            $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));

            return [
                'incl' => $priceIncludingTax,
                'excl' => $priceExcludingTax
            ];
        }
    }

    /**
     * @param null $product
     * @return float|string
     */
    public function getBundleTaxRate($product = null)
    {
        if (is_null($product)) return '';
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            $productRateId = $taxAttribute->getValue();
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);
            return $rate;
        }
    }

    /**
     * @param $product
     * @return \Magento\Framework\Phrase|null
     */
    public function getDiscountPercent($product)
    {
        $specialPrice = floatval($product->getSpecialPrice());
        $regularPrice = floatval($product->getPrice());
        $row = NULL;
        if ($specialPrice) {
            $discount = number_format(($regularPrice - $specialPrice) * 100 / $regularPrice, 0);
            $row = $discount;
        }
        return $row;
    }

    /**
     * @return bool
     */
    public function isCurrentProduct()
    {
        if ($this->request->getFullActionName() == 'catalog_product_view') {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * @param null $product
     * @return bool
     */
    public function chechIsCurrentProduct($product = null)
    {
        if ($this->isCurrentProduct() && $this->getCurrentProduct() && !is_null($product)) {
            if ($product->getId() === $this->getCurrentProduct()->getId()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getCategoryChildrenMargues()
    {
        $catId = self::CATEGORY_ID_MARGUES;
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_categoryFactory->create();
        $category->load($catId);
        $children = $category->getChildrenCategories()->getAllIds();
        array_push($children, $catId);
        return $children;
    }

    /**
     * @param null $catId
     * @return \Magento\Catalog\Model\Category|null
     */
    public function getCategoryLoaded($catId = null)
    {
        if (is_null($catId)) return null;
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_categoryFactory->create();
        $category->load($catId);
        return $category;
    }

    /**
     * @param $product
     * @return bool|string
     */
    public function getPdfFileUrl($product)
    {
        $url = false;
        if (!$product instanceof \Magento\Catalog\Model\Product) {
            return $url;
        }
        $pdf = $product->getData('pdf_file');
        if (!empty($pdf)) {
            $url = $this->_storeManager->getStore($product->getStore())
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product/pdf/' . $pdf;
        }
        return $url;
    }

    /**
     * @param $product
     * @return bool|string
     */
    public function getDownloadPdfFileUrl($product)
    {
        $url = false;
        if (!$product instanceof \Magento\Catalog\Model\Product) {
            return $url;
        }
        $url = $this->_getUrl('webmerattach/download/pdf', ['id' => $product->getId()]);
        return $url;
    }

    /**
     * @param $product
     * @return mixed|string
     */
    public function getPdfFileName($product)
    {
        $name = '';
        if (!$product instanceof \Magento\Catalog\Model\Product) {
            return $name;
        }
        $pdf = $product->getData('pdf_file');
        if (!empty($pdf)) {
            //$name = str_replace('.pdf','',$pdf);
            $name = $pdf;
        }
        return $name;
    }

    /**
     * @param $category
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoryThumbUrl($category)
    {
        $url = false;
        $image = $category->getImageThumb();
        if ($image) {
            if (is_string($image)) {
                $url = $this->_storeManager->getStore()->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ) . 'catalog/category/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }

        return $url;
    }

    /**
     * @param $category
     * @param $attrCode
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoryImageUrl($category, $attrCode)
    {
        $url = false;
        $image = $category->getData($attrCode);
        if ($image) {
            if (is_string($image)) {
                $url = $this->_storeManager->getStore()->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ) . 'catalog/category/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }

        return $url;
    }

    /**
     * Check if current url is url for home page
     *
     * @return bool
     */
    public function isHomePage()
    {
        $currentUrl = $this->_urlBuilder->getUrl('', ['_current' => true]);
        $urlRewrite = $this->_urlBuilder->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        return $currentUrl == $urlRewrite;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReviewsCollection($id)
    {
        $reviewsCollection = $this->_reviewsColFactory->create()->addStoreFilter(
            $this->_storeManager->getStore()->getId()
        )->addStatusFilter(
            \Magento\Review\Model\Review::STATUS_APPROVED
        )->addEntityFilter(
            'product',
            $id
        )->setDateOrder();
        $reviewsCollection->load()->addRateVotes();
        return $reviewsCollection;
    }

    /**
     * @return float|int
     */
    public function getTotalCartItems()
    {
        $total = 0;
        $items = $this->_cartHelper->getQuote()->getAllItems();
        foreach ($items as $item) {
            $total += ($item->getPrice() * $item->getQty());
        }

        return $total;
    }

    public function getTotalCartItemsInclTax()
    {
        $total = 0;
        $items = $this->_cartHelper->getQuote()->getAllItems();
        foreach ($items as $item) {
            $total += ($item->getPriceInclTax() * $item->getQty());
        }

        return $total;
    }

    /**
     * @param null $categories
     * @return array
     */
    public function getCategoryCollection($categories = null)
    {
        if (is_null($categories)) return [];
        $storeId = $this->_storeManager->getStore()->getId();
        $catIds = array(0);
        $catIds = explode(', ', $categories);
        $collection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', ['in' => $catIds])
            ->addFieldToFilter('is_active', 1)
            ->setOrder('position', 'ASC');
        $collection->setStoreId($storeId);
        return $collection;
    }

    /**
     * @param null $categories
     * @return array
     */
    public function getParentCategories($categories = null)
    {
        if (is_null($categories)) return [];
        $storeId = $this->_storeManager->getStore()->getId();
        $parentIds = array(0);
        $categories = $this->getCategoryCollection($categories);
        $categoryIds = $categories->getAllIds();
        foreach ($categories as $category) {
            $parents = $category->getParentIds();
            if (count(array_intersect($parents, $categoryIds)) == 0)
                $parentIds[] = $category->getId();
        }
        $collection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', ['in' => $parentIds])
            ->addFieldToFilter('is_active', 1)
            ->setOrder('position', 'ASC');
        $collection->setStoreId($storeId);
        return $collection;
    }

    /**
     * @return bool
     */
    public function isTaxChoosed()
    {
        //return true if choosed on popup
        if ($this->_sessionManager->getTaxDisplay()) {
            $this->_cookieHelper->set('choose_tax_cookie', true);

            return true;
        }

        // if not chosen - check cookie
        if (!$this->_cookieHelper->get('choose_tax_cookie')) {
            $this->_cookieHelper->set('choose_tax_cookie', true);

            return false;
        } else {
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getTaxSession()
    {
        return $this->_customerSession->getTaxSession();
    }

    /**
     * @param $product
     * @return int
     */
    public function getCountCompareProducts($product)
    {
        $collection = $product->getCompareProductCollection();
        return $collection->count();
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $params
     * @return string
     */
    public function getFullProductUrl(\Magento\Catalog\Model\Product $product, $params = [])
    {
        $useCategoryUrl = $this->getStoreConfig(\Magento\Catalog\Helper\Product::XML_PATH_PRODUCT_URL_USE_CATEGORY);
        //if (!$useCategoryUrl) return $product->getProductUrl();
        $routePath = '';
        $routeParams = $params;

        $storeId = 0;

        $categoryId = $_category = null;
        $categoryExcl = $this->getCategoryChildrenMargues();

        $cats = $product->getCategoryIds();
        if (count($cats)) {
            if ($cats[0] == 2) {
                $categoryId = $cats[1];
            } elseif (!in_array($cats[0], $categoryExcl)) {
                $categoryId = $cats[0];
            } else {
                $categoryId = end($cats);
            }
        }

        if ($categoryId) {
            $_category = $this->_categoryFactory->create()->load($categoryId);
        }

        $requestPath = $this->productUrlPathGenerator->getUrlPathWithSuffix($product, $product->getStoreId(), $_category);

        if (isset($routeParams['_scope'])) {
            $storeId = $this->_storeManager->getStore($routeParams['_scope'])->getId();
        }

        if ($storeId != $this->_storeManager->getStore()->getId()) {
            $routeParams['_scope_to_url'] = true;
        }

        $routeParams['_direct'] = $requestPath;

        // reset cached URL instance GET query params
        if (!isset($routeParams['_query'])) {
            $routeParams['_query'] = [];
        }

        return $this->getUrlInstance()->setScope($storeId)->getUrl($routePath, $routeParams);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $params
     * @param null $categoryId
     * @return string
     */
    public function getProductUrlWithCategory(\Magento\Catalog\Model\Product $product, $params = [], $categoryId = null)
    {
        $useCategoryUrl = $this->getStoreConfig(\Magento\Catalog\Helper\Product::XML_PATH_PRODUCT_URL_USE_CATEGORY);
        if (!$useCategoryUrl) return $product->getProductUrl();
        $routePath = '';
        $routeParams = $params;

        $storeId = $product->getStoreId();

        $_category = null;

        if ($categoryId) {
            $_category = $this->_categoryFactory->create()->load($categoryId);
        }

        $requestPath = $this->productUrlPathGenerator->getUrlPathWithSuffix($product, $product->getStoreId(), $_category);

        if (isset($routeParams['_scope'])) {
            $storeId = $this->_storeManager->getStore($routeParams['_scope'])->getId();
        }

        if ($storeId != $this->_storeManager->getStore()->getId()) {
            $routeParams['_scope_to_url'] = true;
        }

        $routeParams['_direct'] = $requestPath;

        // reset cached URL instance GET query params
        if (!isset($routeParams['_query'])) {
            $routeParams['_query'] = [];
        }

        return $this->getUrlInstance()->setScope($storeId)->getUrl($routePath, $routeParams);
    }

    /**
     * Retrieve URL Instance
     *
     * @return \Magento\Framework\UrlInterface
     */
    private function getUrlInstance()
    {
        return $this->urlFactory->create();
    }

    /**
     * @return bool
     */
    public function isConfigProduct()
    {
        $type = $this->getCurrentProduct()->getTypeId();
        if ($type == "configurable") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * @return mixed
     */
    public function getProductSKU()
    {
        if ($this->isCurrentProduct()) {
            return $this->getCurrentProduct()->getSku();
        }
    }

    /**
     * @param $product
     * @return string
     */
    public function getProductImageAlt($product)
    {
        $alt = $product->getName();
        $brandData = $this->getBrandData($product);
        if (is_array($brandData) && isset($brandData['categoryName'])) {
            $alt .= ' ' . $brandData['categoryName'];
        }
        $alt .= ' ' . $product->getSku();
        return $alt;
    }

    /**
     * Retrieve product category id
     *
     * @return int
     */
    public function getCategoryId()
    {
        $category = $this->_registry->registry('current_category');
        if ($category) {
            return $category->getId();
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function checkIsCategoryMarques($id)
    {
        if (in_array($id, $this->getCategoryChildrenMargues())) {
            return true;
        }
        return false;
    }

    public function getProductImageByProductId($productId)
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $this->getProductDetail($productId)->getImage();
    }

    public function getCustomUrl($customUrl)
    {
        return $this->_storeManager->getStore()->getBaseUrl() . $customUrl;
    }

    public function translateMoth($date)
    {
        $new_date = $date;
        $new_date = str_replace("Jan",__("Jan"), $new_date);
        $new_date = str_replace("Feb",__("Feb"), $new_date);
        $new_date = str_replace("Mar",__("Mar"), $new_date);
        $new_date = str_replace("Apr",__("Apr"), $new_date);
        $new_date = str_replace("May",__("May"), $new_date);
        $new_date = str_replace("June",__("June"), $new_date);
        $new_date = str_replace("July",__("July"), $new_date);
        $new_date = str_replace("Aug",__("Aug"), $new_date);
        $new_date = str_replace("Sept",__("Sept"), $new_date);
        $new_date = str_replace("Oct",__("Oct"), $new_date);
        $new_date = str_replace("Nov",__("Nov"), $new_date);
        $new_date = str_replace("Dec",__("Dec"), $new_date);
        $new_date = str_replace("Yesterday",__("Yesterday"), $new_date);
        $new_date = str_replace("Today",__("Today"), $new_date);
        return $new_date;
    }
}
