<?php

namespace Meridian\Base\Helper\Elasticsearch;

class Data extends \Wyomind\Elasticsearch\Helper\Data
{
    /**
     * @param mixed $store
     * @return string
     */
    public function getAutocompleteUrl($store = null)
    {
        /** @var \Magento\Store\Model\Store $store */
        $store = $this->_storeManager->getStore($store);
        $url = sprintf(
            '%swebmerattach/elasticsearch/result?store=%s',
            $store->getBaseUrl(UrlInterface::URL_TYPE_WEB, $store->isCurrentlySecure()),
            $store->getCode()
        );

        return $url;
    }
}