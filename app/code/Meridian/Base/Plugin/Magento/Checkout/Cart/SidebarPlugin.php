<?php

namespace Meridian\Base\Plugin\Magento\Checkout\Cart;


class SidebarPlugin
{

    /**
     * @var \Meridian\Base\Helper\Data
     */
    private $baseHelper;

    public function __construct(
        \Meridian\Base\Helper\Data $baseHelper
    )
    {
        $this->baseHelper = $baseHelper;
    }

    public function afterGetConfig(
        \Magento\Checkout\Block\Cart\Sidebar $subject,
        $result
    ) {
        $currentPirceDisplay = $this->baseHelper->currentPriceDisplayType();
        $result['tax_display'] = ($currentPirceDisplay == 'INC_TAX') ? 2 : 1;
        $result['subtotal_excl_tax_value'] = sprintf('<span class="price">%s</span>',$this->baseHelper->currencyFormate($this->baseHelper->getTotalCartItems(),2));
        $result['subtotal_incl_tax_value'] = sprintf('<span class="price">%s</span>',$this->baseHelper->currencyFormate($this->baseHelper->getTotalCartItemsInclTax(),2));
        return $result;
    }
}