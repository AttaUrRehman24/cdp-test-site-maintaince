<?php

namespace Meridian\Base\Plugin\Magento\Tax\Checkout\CustomerData;


class CartPlugin
{
    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $_helperBase;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * @var \Magento\Tax\Block\Item\Price\Renderer
     */
    protected $itemPriceRenderer;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var array|null
     */
    protected $totals = null;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->itemPriceRenderer = $itemPriceRenderer;
    }

    /**
     * Add tax data to result
     *
     * @param \Magento\Checkout\CustomerData\Cart $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        $result
    )
    {
        $result['subtotal_incl_tax'] = $this->checkoutHelper->formatPrice($this->getSubtotalInclTax());
        $result['subtotal_excl_tax'] = $this->checkoutHelper->formatPrice($this->getSubtotalExclTax());
        $result['subtotal_excl_tax_value'] = $this->checkoutHelper->formatPrice($this->getSubtotalExclTaxValue());
        $result['subtotal_incl_tax_value'] = $this->checkoutHelper->formatPrice($this->getSubtotalInclTax());

        $currentPirceDisplay = $this->getHelper()->currentPriceDisplayType();
        $result['isTTC'] = $currentPirceDisplay == 'INC_TAX';
        $result['isHT'] = $currentPirceDisplay == 'EXC_TAX';
        if($currentPirceDisplay == 'INC_TAX') {
            $result['subtotal'] = $this->getSubtotalInclTax();
            $result['subtotalAmount'] = $this->checkoutHelper->formatPrice($this->getSubtotalInclTax());
        }
        return $result;
    }

    /**
     * Get subtotal, including tax
     *
     * @return float
     */
    protected function getSubtotalInclTax()
    {
        $subtotal = 0;
        $totals = $this->getTotals();
        if (isset($totals['subtotal'])) {
            $subtotal = $totals['subtotal']->getValueInclTax() ?: $totals['subtotal']->getValue();
        }
        $currentPirceDisplay = $this->getHelper()->currentPriceDisplayType();
        if($currentPirceDisplay == 'INC_TAX'){
            $subtotal = $this->getTotalsFromQuoteItems($this->getAllQuoteItems());
        }
        return $subtotal;
    }

    /**
     * Get subtotal, excluding tax
     *
     * @return float
     */
    protected function getSubtotalExclTax()
    {
        $subtotal = 0;
        $totals = $this->getTotals();
        if (isset($totals['subtotal'])) {
            $subtotal = $totals['subtotal']->getValueExclTax() ?: $totals['subtotal']->getValue();
        }
        $currentPirceDisplay = $this->getHelper()->currentPriceDisplayType();
        if($currentPirceDisplay == 'INC_TAX'){
            $subtotal = $this->getTotalsFromQuoteItems($this->getAllQuoteItems());
        }
        return $subtotal;
    }

    protected function getSubtotalExclTaxValue()
    {
        $subtotal = 0;
        $totals = $this->getTotals();
        if (isset($totals['subtotal'])) {
            $subtotal = $totals['subtotal']->getValueExclTax() ?: $totals['subtotal']->getValue();
        }
        return $subtotal;
    }

    /**
     * Get totals
     *
     * @return array
     */
    public function getTotals()
    {
        // TODO: TODO: MAGETWO-34824 duplicate \Magento\Checkout\CustomerData\Cart::getSectionData
        if (empty($this->totals)) {
            $this->totals = $this->getQuote()->getTotals();
        }
        return $this->totals;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    /**
     * Find item by id in items haystack
     *
     * @param int $id
     * @param array $itemsHaystack
     * @return \Magento\Quote\Model\Quote\Item | bool
     */
    protected function findItemById($id, $itemsHaystack)
    {
        if (is_array($itemsHaystack)) {
            foreach ($itemsHaystack as $item) {
                /** @var $item \Magento\Quote\Model\Quote\Item */
                if ((int)$item->getItemId() == $id) {
                    return $item;
                }
            }
        }
        return false;
    }

    /**
     * @return \Meridian\Base\Helper\Data|mixed
     */
    public function getHelper()
    {
        if($this->_helperBase === null){
            $this->_helperBase = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Meridian\Base\Helper\Data::class
            );
        }
        return $this->_helperBase;
    }

    /**
     * @param $items
     * @return float|int
     */
    protected function getTotalsFromQuoteItems($items)
    {
        $subtotalAmount = 0;
        foreach (array_reverse($items) as $quoteItem) {
            /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
            $subtotalAmount += $quoteItem->getPriceInclTax()  * ($quoteItem->getQty());
        }
        return $subtotalAmount;
    }

    /**
     * Return customer quote items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    protected function getAllQuoteItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }
}