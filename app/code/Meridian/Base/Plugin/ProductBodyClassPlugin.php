<?php

namespace Meridian\Base\Plugin;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\Page\Config;
use Magento\Store\Model\StoreManagerInterface;
use Meridian\Base\Helper\Data as BaseHelper;

class ProductBodyClassPlugin implements ObserverInterface
{
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    protected $_helperBase;

    protected $_blockCart;

    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        BaseHelper $helperBase,
        \Magento\Checkout\Block\Cart $blockCart
    ){
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->_registry = $registry;
        $this->_helperBase = $helperBase;
        $this->_blockCart = $blockCart;
    }

    public function execute(Observer $observer){
        $store = $this->storeManager->getStore();
        $storeCode = $store->getCode();
        $websiteCode = $store->getWebsite()->getCode();
        $this->config->addBodyClass('store-' . $storeCode);
        $this->config->addBodyClass('website-' . $websiteCode);
        $action = $observer->getData('full_action_name');
        if($action == 'checkout_cart_index'){
            if($this->_blockCart->getItemsCount() == 0){
                $this->config->addBodyClass('checkout-cart-empty');
            }
        }
        if ($action != 'catalog_product_view') {
            return $this;
        }
        $product = $this->_registry->registry('current_product');
        if(!$product) {
            return $this;
        }

        $brandData = $this->_helperBase->getBrandData($product);

        if($brandData){
            $categoryProduct = strtolower($brandData['categoryName']);
            $categoryProduct = str_replace(' ','-',$categoryProduct);
            $this->config->addBodyClass('category-' . $categoryProduct);
        }

        return $this;
    }
}