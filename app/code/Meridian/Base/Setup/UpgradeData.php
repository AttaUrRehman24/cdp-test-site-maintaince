<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Meridian\Base\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Setup\UpgradeWidgetData;
use Magento\Catalog\Setup\UpgradeWebsiteAttributes;

/**
 * Upgrade Data script
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * EAV setup factory
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var UpgradeWidgetData
     */
    private $upgradeWidgetData;

    /**
     * @var UpgradeWebsiteAttributes
     */
    private $upgradeWebsiteAttributes;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $_collectionFactory;
    /**
     * @var \Magento\Framework\App\State
     */
    private $_state;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param UpgradeWidgetData $upgradeWidgetData
     * @param UpgradeWebsiteAttributes $upgradeWebsiteAttributes
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        UpgradeWidgetData $upgradeWidgetData,
        UpgradeWebsiteAttributes $upgradeWebsiteAttributes,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\App\State $state
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->upgradeWidgetData = $upgradeWidgetData;
        $this->upgradeWebsiteAttributes = $upgradeWebsiteAttributes;
        $this->_collectionFactory = $collectionFactory;
        $this->_state = $state;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $setup->startSetup();
        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.1') < 0 ) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            /**
             * Add attributes to the eav/attribute
             */
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'garantie_description', [
                    'type' => 'text',
                    'label' => 'Garantie',
                    'input' => 'textarea',
                    'sort_order' => 4,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'searchable' => false,
                    'comparable' => false,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'visible_in_advanced_search' => false,
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'group' => 'General',
                ]
            );
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.2') < 0 ) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'garantie_description',
                'is_required',
                0
            );
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.3') < 0 ) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $productTypes = [
                \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE,
                \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL,
                \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE,
                \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE,
                \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE
            ];
            $productTypes = join(',', $productTypes);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'pdf_file',
                [
                    'type' => 'varchar',
                    'label' => 'Pdf File',
                    'input' => 'file',
                    'backend' => 'Meridian\Base\Model\Product\Attribute\Backend\Pdf',
                    'frontend' => 'Meridian\Base\Model\Product\Attribute\Frontend\Pdf',
                    //'input_renderer' => \Magento\Msrp\Block\Adminhtml\Product\Helper\Form\Type::class,
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'unique' => false,
                    'apply_to' => $productTypes,
                    'used_in_product_listing' => false,
                    'group' => 'Attributes'
                ]
            );
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.4') < 0 ) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);

            //$eavSetup->removeAttribute($entityTypeId, 'menu_featured_content');
            //$eavSetup->removeAttribute($entityTypeId, 'menu_footer_content');

            $groups = [
                'megamenu' => ['name' => 'Mega Menu', 'code' => 'mega-menu', 'sort' => 50, 'id' => null],
            ];

            foreach ($groups as $k => $groupProp) {
                $eavSetup->addAttributeGroup($entityTypeId, $attributeSetId, $groupProp['name'], $groupProp['sort']);
                $groups[$k]['id'] = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $groupProp['code']);
            }

            /**
             * Add attributes to the eav/attribute
             */
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'menu_featured_content', [
                    'type' => 'text',
                    'label' => 'Featured Content',
                    'input' => 'textarea',
                    'sort_order' => 4,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'searchable' => false,
                    'comparable' => false,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'visible_in_advanced_search' => false,
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'is_required' => false,
                    'required' => false,
                    'group' => 'Mega Menu',
                ]
            );

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'menu_footer_content', [
                    'type' => 'text',
                    'label' => 'Footer Content',
                    'input' => 'textarea',
                    'sort_order' => 5,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'searchable' => false,
                    'comparable' => false,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'visible_in_advanced_search' => false,
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'is_required' => false,
                    'required' => false,
                    'group' => 'Mega Menu',
                ]
            );

            $attributes = [
                'menu_featured_content' => ['group' => 'megamenu', 'sort' => 10],
                'menu_footer_content' => ['group' => 'megamenu', 'sort' => 20],
                ];

            foreach ($attributes as $attributeCode => $attributeProp) {
                $eavSetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $groups[$attributeProp['group']]['id'],
                    $attributeCode,
                    $attributeProp['sort']
                );
            }
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.5') < 0 ) {
            $productCollection= $this->_collectionFactory->create()->addAttributeToSelect('*')->load();
            foreach ($productCollection as $product) {

                $product->setData('date_available_product_i', 0);
                $product->setData('date_available_delay_i', 'null');
                $product->getResource()->saveAttribute($product, 'date_available_product_i');
                $product->getResource()->saveAttribute($product, 'date_available_delay_i');
            }
        }
        $setup->endSetup();
    }

    }
