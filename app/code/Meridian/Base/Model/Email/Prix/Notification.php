<?php

namespace Meridian\Base\Model\Email\Prix;

class Notification
{
    /**
     * Default template id email
     */
    const EMAIL_TEMPLATE_ID = 'comptoir_config_product_page_email_template';

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'comptoir_config/product_page/recipient_email';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'comptoir_config/product_page/sender_email_identity';

    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'comptoir_config/product_page/email_template';

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Meridian\Base\Helper\Data
     */
    protected $helperBase;

    protected $_configPrix;
    protected $_transportBuilder;
    protected $_storeManager;
    protected $_state;

    public function __construct(
        \Meridian\Base\Model\Config $configPrix,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Meridian\Base\Helper\Data $helperBase
    )
    {
        $this->_configPrix = $configPrix;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_state = $state;
        $this->inlineTranslation = $inlineTranslation;
        $this->helperBase = $helperBase;
    }

    public function send($replyTo, array $variables)
    {
        $replyToName = !empty($variables['data']['name']) ? $variables['data']['name'] : null;
        $configTemplateId = $this->_configPrix->emailTemplate();
        $configEmailSender = $this->_configPrix->emailSender();
        $configEmailRecipient = $this->_configPrix->emailRecipient();

        $templateId = ($configTemplateId === null) ? self::EMAIL_TEMPLATE_ID : $configTemplateId;
        $emailSender = ($configEmailSender === null) ? 'general' : $configEmailSender;
        $emailRecipient = ($configEmailRecipient === null) ? $this->_configPrix->getStoreSupportEmail() : $configEmailRecipient;

        $this->inlineTranslation->suspend();
        try {
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($templateId)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId()
                    ]
                )
                ->setTemplateVars($variables)
                ->setFrom($emailSender)
                ->addTo($emailRecipient)
                ->setReplyTo($replyTo, $replyToName)
                ->getTransport();

            $transport->sendMessage();
        } finally {
            $this->inlineTranslation->resume();
        }

        return $this;
        /*$this->_sendEmailTemplate($template, $sender, $params, $storeId, $email, $name);*/
    }

    protected function _sendEmailTemplate($template, $sender, $templateParams = [], $storeId, $recipientEmail, $recipientName)
    {
        $copyTo = $this->getEmailCopyTo($storeId);

        $transport = $this->_transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            $templateParams
        )->setFrom(
            $sender
        )->addTo(
            $recipientEmail,
            $recipientName
        );

        if (!empty($copyTo)) {
            foreach ($copyTo as $email) {
                $this->_transportBuilder->addCc($email);
            }
        }

        $transport= $this->_transportBuilder->getTransport();

        $transport->sendMessage();

        return $this;
    }

}