<?php

namespace Meridian\Base\Block;

/**
 * Class Attachment
 * @package Prince\Productattach\Block
 */
class Attachment extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Meridian\Base\Helper\Data
     */
    private $dataHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Meridian\Base\Helper\Data $dataHelper,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->customerSession =$customerSession;
        $this->dataHelper = $dataHelper;
        $this->scopeConfig = $context->getScopeConfig();
        $this->registry = $registry;
        parent::__construct(
            $context,
            $data
        );
    }
    
    /**
     * Check module is enable or not
     */
    public function isUseBrowserDownload()
    {
        return false;
    }

    /**
     * Retrive attachment url by attachment
     *
     * @return string
     */
    public function getAttachmentUrl($attachment)
    {
        $url = $this->dataHelper->getPdfFileUrl($attachment);
        return $url;
    }

    /**
     * @param $attachment
     * @return mixed|string
     */
    public function getAttachmentName($attachment)
    {
        $lenghtName = 15;
        $name = $this->dataHelper->getPdfFileName($attachment);
        if(strlen($name)>= $lenghtName){
            $f_ext = explode(".", $name);
            $f_ext_c = count($f_ext);
            $f_ext_l = strlen($f_ext[$f_ext_c-1])+1;
            $name = substr($name, 0, $lenghtName-$f_ext_l);
            $name .= ".".$f_ext[$f_ext_c-1];
        }
        return $name;
    }

    /**
     * @param $attachment
     * @return bool|string
     */
    public function getAttachmentDownloadUrl($attachment)
    {
        $url = $this->dataHelper->getDownloadPdfFileUrl($attachment);
        return $url;
    }

    /**
     * @return mixed
     */
    public function getCurrentProduct(){
        $product = $this->registry->registry('current_product');
        return $product;
    }

    /**
     * Retrive current product id
     *
     * @return number
     */
    public function getCurrentId()
    {
        $product = $this->getCurrentProduct();
        return $product->getId();
    }

    /**
     * Retrive current customer id
     *
     * @return number
     */
    public function getCustomerId()
    {
        $customerId = $this->customerSession->getCustomer()->getGroupId();
        return $customerId;
    }

    /**
     * Retrive link icon image
     *
     * @return string
     */
    public function getLinkIcon()
    {
        $iconImage = $this->getViewFileUrl('Meridian_Base::images/pdf.png');
        return $iconImage;
    }

    /**
     * Retrive config value
     */
    public function getConfig($config)
    {
        return $this->scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
