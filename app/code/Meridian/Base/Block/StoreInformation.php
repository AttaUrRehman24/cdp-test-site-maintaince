<?php

namespace Meridian\Base\Block;

use \Magento\Framework\View\Element\Template;
use \Meridian\Base\Helper\Data as MeridianBaseHelper;

class StoreInformation extends Template
{
    protected $helper;

    public function __construct(
        MeridianBaseHelper $helper,
        Template\Context $context,
        array $data = []
    )
    {
        $this->helper        = $helper;
        parent::__construct($context, $data);
    }

    public function getStoreTelephone()
    {
        return $this->helper->getWebsiteConfig(\Magento\Store\Model\Information::XML_PATH_STORE_INFO_PHONE);
    }

    public function getStoreHours()
    {
        return $this->helper->getWebsiteConfig(\Magento\Store\Model\Information::XML_PATH_STORE_INFO_HOURS);
    }
}