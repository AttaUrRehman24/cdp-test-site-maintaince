<?php

namespace Meridian\ShippingFlatrate\Plugin;

use Magento\Backend\Model\Auth\Session;

class HideFlatrateOnFrontend
{
    protected $backendSession;

    public function __construct(Session $session)
    {
        $this->backendSession = $session;
    }

    public function aroundCollectRates(
        \Magento\OfflineShipping\Model\Carrier\Flatrate $subject,
        callable $proceed,
        \Magento\Quote\Model\Quote\Address\RateRequest $request
    )
    {
        if ($this->backendSession->isLoggedIn()) {
            return $proceed($request);
        }

        return false;
    }
}