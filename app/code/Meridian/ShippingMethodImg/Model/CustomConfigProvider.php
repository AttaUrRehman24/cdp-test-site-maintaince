<?php
namespace Meridian\ShippingMethodImg\Model;
use Magento\Checkout\Model\ConfigProviderInterface;
class CustomConfigProvider implements ConfigProviderInterface
{
    protected $_storeManager;
    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    public function getConfig()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
        $config = [];
        $config['mediaUrl'] = $mediaUrl;
        return $config;
    }
}