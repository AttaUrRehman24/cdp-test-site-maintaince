var config = {
    map: {
        '*': {
            'Paybox_Epayment/js/view/payment/method-renderer/pbxep_multi-method' :
                'Meridian_Epayment/js/view/payment/method-renderer/pbxep_multi-method',
            'Paybox_Epayment/template/payment/pbxep_multi.html':
                'Meridian_Epayment/template/payment/pbxep_multi.html'
        }
    }
};