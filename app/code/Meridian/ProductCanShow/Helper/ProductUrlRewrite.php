<?php

namespace Meridian\ProductCanShow\Helper;


use Magento\Framework\App\Helper\Context;

class ProductUrlRewrite extends \Magento\Framework\Url\Helper\Data
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnectionClass;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appStateRewrite;

    public function __construct(
        Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\State $appStateRewrite
    )
    {
        $this->_resourceConnectionClass = $resourceConnection;
        $this->_appStateRewrite = $appStateRewrite;
        parent::__construct($context);
    }

    public function getConnection()
    {
        return $this->_resourceConnectionClass->getConnection();
    }

    public function getAppStateFromHelper()
    {
        return $this->_appStateRewrite;
    }
}