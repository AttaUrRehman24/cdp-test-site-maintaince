<?php
namespace Meridian\ProductCanShow\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Store\Model\Store;
use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator;
use Magento\Framework\App\State;

class RegenerateCategoryUrlCommand extends Command
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $urlRewriteFactory;

    /**
     * @var CategoryUrlRewriteGenerator
     */
    protected $categoryUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler
     */
    protected $urlRewriteHandler;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * RegenerateCategoryUrlCommand constructor.
     * @param \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param UrlPersistInterface $urlPersist
     * @param \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler $urlRewriteHandler
     * @param State $state
     */
    public function __construct(
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        UrlPersistInterface $urlPersist,
        \Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler $urlRewriteHandler,
        State $state
    )
    {
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->storeManager = $storeManager;

        $this->categoryUrlRewriteGenerator = $categoryUrlRewriteGenerator;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->urlPersist = $urlPersist;
        $this->urlRewriteHandler = $urlRewriteHandler;

        $this->_appState = $state;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('webmer:productcanshow:regenurl:category');
        $this->setDescription('Regenerate Url\'s for categories');
        return parent::configure();
    }

    public function execute(InputInterface $inp, OutputInterface $out)
    {
        // set area code if needed
        try {
            $areaCode = $this->_appState->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // if area code is not set then magento generate exception "LocalizedException"
            $this->_appState->setAreaCode('adminhtml');
        }

        if ($this->storeManager->isSingleStoreMode()) {
            $stores = [$this->storeManager->getStore(0)];
        } else {
            $stores = $this->storeManager->getStores();
        }

        $start_time = microtime(true);
        foreach ($stores as $store) {
            $step = 0;
            //$out->writeln($store->getCode().'...');
            $out->writeln("[Store ID: {$store->getId()}, Store View code: {$store->getCode()}]:");
            $categories = $this->categoryCollectionFactory->create()->setStore($store->getId())
                ->addAttributeToSelect(array('url_key', 'url_path', 'is_anchor'))
                ->addAttributeToFilter('level', 2)
                ->addAttributeToFilter('parent_id',$store->getRootCategoryId())
            ;
            foreach ($categories as $category) {
                if ($category->getUrlKey()) {
                    $category->setStoreId($store->getId());
                    $urlRewrites = array_merge(
                        $this->categoryUrlRewriteGenerator->generate($category, true)
                        , $this->urlRewriteHandler->generateProductUrlRewrites($category)
                    );
                    $this->urlPersist->replace($urlRewrites);
                }
                $step++;
                $out->write('.');
                if ($step > 19) {
                    $out->writeln('');
                    $step = 0;
                }
            }
        }
        $out->writeln('[DONE] ' . round(microtime(true) - $start_time,2) .' sec');
    }
}
