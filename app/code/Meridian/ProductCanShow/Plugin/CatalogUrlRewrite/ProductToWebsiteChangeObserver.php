<?php

namespace Meridian\ProductCanShow\Plugin\CatalogUrlRewrite;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\Store;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class ProductToWebsiteChangeObserver
{
    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param ProductRepositoryInterface $productRepository
     * @param RequestInterface $request
     */
    public function __construct(
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        ProductRepositoryInterface $productRepository,
        RequestInterface $request,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->_coreRegistry = $coreRegistry;
    }

    public function aroundExecute(
        \Magento\CatalogUrlRewrite\Observer\ProductToWebsiteChangeObserver $subject,
        \Closure $proceed,
        \Magento\Framework\Event\Observer $observer
    ) {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product->getVisibility() == Visibility::VISIBILITY_NOT_VISIBLE) {
            $product->setVisibility(Visibility::VISIBILITY_IN_CATALOG);
            $this->_coreRegistry->register('current_product_visibility', true);
        }
        $origMethod = $proceed($observer);
        if($this->_coreRegistry->registry('current_product_visibility')){
            $product->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
            $this->_coreRegistry->unregister('current_product_visibility');
        }
    }

}
