<?php

namespace Meridian\ProductAvailable\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;

use Magento\Framework\DB\Ddl\Table;

/**
 * Custom Attribute Renderer
 */
class Amount extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource

{

    /**
     * @var OptionFactory
     */

    protected $optionFactory;

    /**
     * @param OptionFactory $optionFactory
     */

    /**
     * Get all options
     *
     * @return array
     */

    public function getAllOptions()

    {
        $this->_options = [
            ['label' => '', 'value' => ''],
            ['label' => 'Delay priority', 'value' => '1'],
            ['label' => 'Date available priority', 'value' => '2'],
            ['label' => 'Sum date available and delay', 'value' => '3']
        ];
        return $this->_options;
    }

}
