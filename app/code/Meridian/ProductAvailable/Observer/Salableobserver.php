<?php

namespace Meridian\ProductAvailable\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class Salableobserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $salable = $observer->getSalable();
        $salable->setIsSalable(true);
        $_product = $observer->getProduct();
        $_product->setIsSalable(true);
        return $this;
    }
}