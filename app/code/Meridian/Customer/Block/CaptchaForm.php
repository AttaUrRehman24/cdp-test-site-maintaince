<?php

namespace Meridian\Customer\Block;


class CaptchaForm extends \Magento\Framework\View\Element\Template
{
    public function getFormAction()
    {
        return $this->getUrl('meridiancustomer/account/createpost', ['_secure' => true]);
    }
}