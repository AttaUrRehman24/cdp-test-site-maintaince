<?php

namespace Meridian\CatalogInventory\Ui\DataProvider\Product\Modifier;


class Quantity implements \Magento\Ui\DataProvider\Modifier\ModifierInterface {

    protected $arrayManager;

    public function __construct(\Magento\Framework\Stdlib\ArrayManager $arrayManager){
        $this->arrayManager =$arrayManager;
    }

    public function modifyMeta(array $meta)
    {
        if ($path = $this->arrayManager->findPath('quantity_and_stock_status_qty', $meta, null, 'children')) {
            $this->arrayManager->remove(
                $path . '/children/qty/arguments/data/config/validation/validate-digits',
                $meta
            );
            $this->arrayManager->merge(
                $path . '/children/qty/arguments/data/config/imports',
                $meta,
               array('handleChanges'=>"1")
            );
        }

        if ($path = $this->arrayManager->findPath('advanced_inventory_modal', $meta)) {
            $meta = $this->arrayManager->merge(
                $path . '/children/stock_data/children/qty/arguments/data/config',
                $meta,
                ['validation' => ['validate-digits' => false],'imports'=>['handleChanges'=>'1']]
            );
        }

        return $meta;
    }


    public function modifyData(array $data){
        return $data;
    }
}