<?php
/**
 * Anowave Magento 2 Tax Switcher
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_TaxSwitch
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\TaxSwitch\Helper;

use Magento\Store\Model\Store;
use Anowave\Package\Helper\Base;
use Anowave\Package\Helper\Package;
use Magento\Framework\Registry;

class Data extends Package
{
	/**
	 * Package name
	 * 
	 * @var string
	 */
	protected $package = 'MAGE2-TAXSWITCH';
	
	/**
	 * Config path 
	 * 
	 * @var string
	 */
	protected $config = 'taxswitch/general/license';
	
	/**
	 * Check if module is active
	 * 
	 * @return boolean
	 */
	public function isActive()
	{
		return 1 === (int) @$this->getConfig('taxswitch/general/active');
	}
	
	public function usePrecisionServices()
	{
		return 1 === (int) @$this->getConfig('taxswitch/maxmind/active');
	}
	
	public function usePrivateContent()
	{
		return 1 === (int) @$this->getConfig('taxswitch/advanced/private_content');
	}
	
	public function getTrack()
	{
		return json_encode
		(
			[
				'enable' 		=> 1 === (int) $this->getConfig('taxswitch/analytics/track'),
				'type'	 		=> $this->getConfig('taxswitch/analytics/type'),
				'fieldsObject' 	=> 
				[
					'hitType' 		=> 'event',
					'eventCategory' => __('Tax Switcher'),
					'eventAction' 	=> 'switch',
					'eventLabel' 	=> ''
				],
				'fieldsObjectDatalayer' =>
				[
					'hitType' 		=> 'event',
					'eventCategory' => __('Tax Switcher'),
					'eventAction' 	=> 'switch',
					'eventLabel' 	=> ''
				]
			]
		);
	}
}
