<?php
/**
 * Anowave Magento 2 Tax Switcher
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_TaxSwitch
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\TaxSwitch\Block;

use Magento\Framework\View\Element\Template;

class Switcher extends \Magento\Framework\View\Element\Template
{
	/**
	 * @var \Anowave\TaxSwitch\Helper\Data
	 */
	protected $helper;
	
	/**
	 * Constructor
	 *
	 * @param Template\Context $context
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Framework\View\Element\Template\Context $context,
		\Anowave\TaxSwitch\Helper\Data $helper,
		array $data = []
	)
	{
		parent::__construct($context, $data);
		
		$this->helper = $helper;
	}
	
	/**
	 * Set options
	 */
	public function getOptions()
	{
		$current = ($this->_request->getParam('tax_display') ? $this->_request->getParam('tax_display') : $this->_session->getTaxDisplay());
		
		if (!$current)
		{
			$current = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Tax\Model\Config')->getPriceDisplayType();
		}

		return 
		[
			[
				'value' 	=> \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX,
				'label' 	=> __('AFFICHER LES PRIX EN TTC'),
				'checked'	=> $current == \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX ? 1 : 0
			],
			[
				'value' 	=> \Magento\Tax\Model\Config::DISPLAY_TYPE_EXCLUDING_TAX,
				'label' 	=> __('AFFICHER LES PRIX EN HT'),
				'checked' 	=> $current == \Magento\Tax\Model\Config::DISPLAY_TYPE_EXCLUDING_TAX ? 1 : 0
			]
		];
	}

	/**
	 *
     * Get Current price display option option
     *
	 * */

    public function currentPriceDisplayType()
    {
        $current = ($this->_request->getParam('tax_display') ? $this->_request->getParam('tax_display') : $this->_session->getTaxDisplay());

        if (!$current)
        {
            $current = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Tax\Model\Config')->getPriceDisplayType();
        }

        if($current == \Magento\Tax\Model\Config::DISPLAY_TYPE_INCLUDING_TAX){
            return 'INC_TAX';
        } elseif ($current == \Magento\Tax\Model\Config::DISPLAY_TYPE_EXCLUDING_TAX) {
            return 'EXC_TAX';
        }
    }
	
	/**
	 * Get helper
	 * 
	 * @return \Anowave\TaxSwitch\Helper\Data
	 */
	public function getHelper()
	{
		return $this->helper;
	}
	
	public function _toHtml()
	{
		if (!$this->helper->isActive())
		{
			return '';	
		}
		
		return $this->helper->filter(parent::_toHtml());
	}
}