<?php

namespace Comptoirdespros\CustomerStatus\Helper;

use Magento\Framework\App\Helper\AbstractHelper;


class Brand extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Http\Context
     */

    //  private $_objectManager;
    protected $_registry;
    protected $_resourceConnection;
    protected $_productFactory;
    protected $_storeManagerInterface;
    protected $_categoryRepository;
    protected $_categoryFactory;
    protected $_category;
    protected $_catalogSession;
    protected $_categorycollectionFactory;
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        // \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Category $category,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categorycollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory

    )
    {
        parent::__construct($context);
        // $this->_objectManager = $objectmanager;
        $this->_registry = $registry;
        $this->_resourceConnection = $resourceConnection;
        $this->_productFactory = $productFactory;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_category = $category;
        $this->_catalogSession = $catalogSession;
        $this->_categorycollectionFactory = $categorycollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function brandBestSellingProducts()
    {
        $categoryId = $this->getCurrentCategory();
        $connection = $this->_resourceConnection->getConnection();
        $tableName = $this->_resourceConnection->getTableName('product_brands_relation');
        $query = $connection->select()
            ->from($tableName)
            ->where('brand_category_id = ?', $categoryId)
            ->where('created_at > ?', new \Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 24 HOUR)"))
            ->where('created_at <= ?', new \Zend_Db_Expr('NOW()'))
            ->order('id ASC')
            ->limit(8);

        return $connection->fetchAll($query);
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category')->getId();
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function getProductDetail($product_id)
    {
        return $this->_productFactory->create()->load($product_id);
    }

    public function brandCategories()
    {

        $categoryId = $this->getCurrentCategory();
        $connection = $this->_resourceConnection->getConnection();
        $tableName = $this->_resourceConnection->getTableName('catalog_brands_relation');

        $query = $connection->select()
            ->from($tableName)
            ->where('brand_category_id = ?', $categoryId)
            ->where('cat_id != ?', $categoryId)
            ->where('created_at > ?', new \Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 24 HOUR)"))
            ->where('created_at <= ?', new \Zend_Db_Expr('NOW()'))
            ->order('prodcut_sales_count DESC')
            ->limit(6);

        return $connection->fetchAll($query);
    }

    public function getBrandsCategory($parentCatId = null, $categoryTitle = null)
    {

        if ($categoryTitle) {
            $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name', $categoryTitle)->setPageSize(1);
            if ($collection->getSize()) {
                $parentCatId = $collection->getFirstItem()->getId();
            }
        }

        $childrenCategory = [];

        $parentcategories = $this->_categoryRepository->get($parentCatId);
        $childrenCategories = $parentcategories->getChildrenCategories();
        foreach ($childrenCategories as $childrenCategorie) {
            $childrenCategory[] = $childrenCategorie->getId();
        }

        return $childrenCategory;

    }

    public function getCategory($catId)
    {
        return $this->_categoryRepository->get($catId);
    }

    public function getMediaUrl()
    {
        return $this->getStoreData()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getStoreData()
    {
        return $this->_storeManagerInterface->getStore();
    }

    public function getBrandData()
    {
        $product = $this->getCurrentProduct();
        $categories = $product->getCategoryCollection()->addAttributeToSelect(array('parent_id', 'name', 'image', 'url_path', 'product_delivery_time', 'product_delivery_info_message')); //
        foreach ($categories as $category) {
            if ($category->getParentId() == 122) {
                $dataArray = ['ImageUrl' => $category->getImageUrl(), 'categoryName' => $category->getName(), 'url' => $category->getUrl(), 'product_delivery_time' => $category->getData('product_delivery_time'), 'product_delivery_info_message' => $category->getData('product_delivery_info_message')];
                return $dataArray;
            }
        }
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function homeBestSellingProducts()
    {

        $connection = $this->_resourceConnection->getConnection();
        $tablePBR = $this->_resourceConnection->getTableName('product_brands_relation');

        try {
            $results = $connection->fetchAll('SELECT Distinct (product_id) FROM ' . $tablePBR . ' WHERE  product_id IN (SELECT  product_id FROM (SELECT product_id,( SELECT  COUNT(1) FROM ' . $tablePBR . ' WHERE brand_category_id = i.brand_category_id ) CountTotal FROM ' . $tablePBR . ' i) sub WHERE   sub.CountTotal <= 3 ) ORDER by RAND() limit 4');

            $productIds = array_column($results, 'product_id');

            if (!empty($productIds)) {

                return $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addStoreFilter()->addFieldToFilter('entity_id', ['in' => $productIds]);

            }

        } catch (\Exception $e) {
            $this->writeLoger()->info('Query data exception Home page brand product' . $e->getMessage());
        }
    }

    public function writeLoger()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/query.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        return $logger;
    }


    public function homePromotionProducts()
    {
        return $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addStoreFilter()->addAttributeToFilter('promotion_home_page',1);
    }


}