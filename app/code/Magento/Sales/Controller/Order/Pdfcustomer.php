<?php
namespace Magento\Sales\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;



class Pdfcustomer extends Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    protected $_orders;
    protected $_customerSession;
    protected $_orderCollectionFactory;
    protected $_objectManager;

    /**
     * @param Context                               $context
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Sales\Model\OrderFactory  $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\ObjectManagerInterface $ObjectManager
    ) {
        $this->fileFactory = $fileFactory;
        $this->_objectManager = $ObjectManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * to generate pdf
     *
     * @return void
     */



    public function getOrders()
    {
        
        $order_id= $this->getRequest()->getParam('order_id');
        if($order_id){
            
      
        $orderdetails = $this->_orderCollectionFactory->create()->load($order_id);
          $orderdetails->getGrandTotal(); //you can get the grandtotal like this
     

        
        $customerId = $this->_customerSession->getCustomerId();

     foreach ($orderdetails->getInvoiceCollection() as $invoice)
        {
           return  $invoice_id = $invoice->getId();
        }

    }

    }


    

    public function execute()
    {


        
        if (!($this->_customerSession->isLoggedIn())) {
           
            return $this->_redirect('/');
        }

   

        $invoiceId = $this->getOrders(); //$this->getRequest()->getParam('order_id');
        if ($invoiceId) {
            $invoice = $this->_objectManager->create(
                \Magento\Sales\Api\InvoiceRepositoryInterface::class
            )->get($invoiceId);
            if ($invoice) {
                $pdf = $this->_objectManager->create(\Magento\Sales\Model\Order\Pdf\Invoice::class)->getPdf([$invoice]);

                $date = $this->_objectManager->get(
                    \Magento\Framework\Stdlib\DateTime\DateTime::class
                )->date('Y-m-d_H-i-s');

                return $this->fileFactory->create(
                    'facture_' . $this->getOrders(). '.pdf',
                    $pdf->render(),
                    \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        }else{
            return $this->_redirect('customer/account');
        }


    }
}