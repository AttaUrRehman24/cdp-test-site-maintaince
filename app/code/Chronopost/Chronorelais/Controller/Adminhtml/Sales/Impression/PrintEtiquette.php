<?php
namespace Chronopost\Chronorelais\Controller\Adminhtml\Sales\Impression;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Filesystem\DirectoryList;

use Magento\Sales\Model\OrderFactory as OrderFactory;

use Chronopost\Chronorelais\Helper\Data as HelperData;
use Chronopost\Chronorelais\Helper\Shipment as HelperShipment;

class PrintEtiquette extends AbstractImpression
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var HelperShipment
     */
    protected $_helperShipment;

    /**
     * PrintEtiquette constructor.
     * @param Context $context
     * @param DirectoryList $directoryList
     * @param PageFactory $resultPageFactory
     * @param OrderFactory $orderFactory
     * @param HelperData $helperData
     * @param HelperShipment $helperShipment
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        PageFactory $resultPageFactory,
        OrderFactory $orderFactory,
        HelperData $helperData,
        HelperShipment $helperShipment
    ) {
        parent::__construct($context,$directoryList,$resultPageFactory,$helperData);
        $this->_helperShipment = $helperShipment;
        $this->_orderFactory = $orderFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $orderId = $this->getRequest()->getParam('order_id');
        $etiquetteUrl = array();

        try {
            if($orderId) { /* pas encore d'expédition : génération de ou des étiquettes */
                $order = $this->_orderFactory->create()->load($orderId);
                if($order && $order->getId()) {
                    $shippingMethod = $order->getData('shipping_method');
                    $shippingMethodCode = explode("_", $shippingMethod);
                    $shippingMethodCode = isset($shippingMethodCode[1]) ? $shippingMethodCode[1] : $shippingMethodCode[0];
                    if(!$this->_helperData->isChronoMethod($shippingMethodCode)) { /* methode NON chronopost */
                        Throw new \Exception("Delivery option not Chronopost for order %1",$order->getIncrementId());
                    }

                    $_shipments = $order->getShipmentsCollection();

                    if($_shipments->count()) { /* expedition existe deja */

                        /* si 1 seule expédition : on recup l'url de l'étiquette */
                        if($_shipments->count() == 1) {
                            /* @TOOO récup url etiquette existante */
                            $_shipment = $_shipments->getFirstItem();
                            $etiquetteUrl[] = $this->_helperShipment->getEtiquetteUrl($_shipment->getId());

                        } else {
                            if($this->gsIsActive()) {
                                foreach($_shipments as $_shipment) {
                                    $etiquetteUrl[] = $this->_helperShipment->getEtiquetteUrl($_shipment->getId());
                                }
                            } else {
                                $this->messageManager->addNoticeMessage(__("This order contains several shipments, click the link to obtain the labels"));
                                $resultRedirect->setPath("chronopost_chronorelais/sales/impression");
                                return $resultRedirect;
                            }
                        }
                    } else { /* creation etiquette */

                        if($this->_helperData->orderIsOverLimit($order,$shippingMethod)) { /* poids commande > limite => creation 1 expédition + etiquette par produit */

                            foreach ($order->getItems() as $item) {
                                $qty = $item->getQtyOrdered();
                                for($i = 1; $i <= $qty; $i++) {
                                    $etiquetteUrl[] = $this->_helperShipment->createNewShipment($order,array($item->getId() => '1'));
                                }
                            }

                        } else { /* creation expedition + étiquette pour la commande */
                            $etiquetteUrl[] =  $this->_helperShipment->createNewShipment($order);
                        }
                    }
                }
            } else { /* expedition existante */
                $shipmentId = $this->getRequest()->getParam('shipment_id');
                if ($shipmentId) {
                    $etiquetteUrl[] = $this->_helperShipment->getEtiquetteUrl($shipmentId);
                } else {
                    $shipmentIncrementId = $this->getRequest()->getParam('shipment_increment_id');
                    $_shipment = $this->_helperShipment->getShipmentByIncrementId($shipmentIncrementId);

                    $etiquetteUrl[] = $this->_helperShipment->getEtiquetteUrl($_shipment->getId());

                }
            }

            if(count($etiquetteUrl)) {
                if(count($etiquetteUrl) === 1) {
                    $this->prepareDownloadResponse('Etiquette_chronopost.pdf',  $etiquetteUrl[0]);
                } else { /* plusieurs etiquettes générées */
                    if($this->gsIsActive()) {
                        $this->_processDownloadMass($etiquetteUrl);
                    } else {
                        $this->messageManager->addNoticeMessage(__("This order contains several shipments, click the link to obtain the labels"));
                        $resultRedirect->setPath("chronopost_chronorelais/sales/impression");
                        return $resultRedirect;
                    }
                }
            }

        } catch(\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
            $resultRedirect->setPath("chronopost_chronorelais/sales/impression");
            return $resultRedirect;
        }
    }

}