<?php
namespace Chronopost\Chronorelais\Helper;

use Chronopost\Chronorelais\Model\ResourceModel\OrderExportStatus\CollectionFactory as OrderExportStatusCollectionFactory;
use Magento\Shipping\Model\CarrierFactory;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const CHRONOPOST_REVERSE_R = '4R'; // for Chronopost Reverse 9
    const CHRONOPOST_REVERSE_S = '4S'; // for Chronopost Reverse 10
    const CHRONOPOST_REVERSE_T = '4T'; // for Chronopost Reverse 13
    const CHRONOPOST_REVERSE_U = '4U'; // for Chronopost Reverse 18
    const CHRONOPOST_REVERSE_DEFAULT = '01'; // for Chronopost Reverse 18

    const CHRONOPOST_REVERSE_R_SERVICE = '885'; // for Chronopost Reverse 9
    const CHRONOPOST_REVERSE_S_SERVICE = '180'; // for Chronopost Reverse 10
    const CHRONOPOST_REVERSE_T_SERVICE = '898'; // for Chronopost Reverse 13
    const CHRONOPOST_REVERSE_U_SERVICE = '835'; // for Chronopost Reverse 18
    const CHRONOPOST_REVERSE_DEFAULT_SERVICE = '226'; // for Chronopost Reverse 18

    /**
     * @var OrderExportStatusCollectionFactory
     */
    protected $_orderExportStatusCollectionFactory;

    /**
     * @var CarrierFactory
     */
    protected $_carrierFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        OrderExportStatusCollectionFactory $collectionFactory,
        CarrierFactory $carrierFactory
    ) {
        parent::__construct($context);
        $this->_orderExportStatusCollectionFactory = $collectionFactory;
        $this->_carrierFactory = $carrierFactory;
    }

    /**
     * @param $node
     * @return mixed
     */
    public function getConfig($node) {
        return $this->scopeConfig->getValue($node);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $shippingMethod
     * @return bool
     */
    public function orderIsOverLimit(\Magento\Sales\Model\Order $order, $shippingMethod) {
        $shippingMethod = explode("_", $shippingMethod);
        $shippingMethod = isset($shippingMethod[1]) ? $shippingMethod[1] : $shippingMethod[0];
        $weight_limit = $this->getConfig('carriers/'.$shippingMethod.'/weight_limit');

        $weightShipping = 0;
        foreach ($order->getAllItems() as $item) {
            $weightShipping += $item->getWeight()*$item->getQtyOrdered();
        }
        if($this->getConfig('chronorelais/weightunit/unit') == 'g')
        {
            $weightShipping = $weightShipping / 1000; // conversion g => kg
        }

        return $weightShipping > $weight_limit;
    }

    /**
     * return true if method is chrono
     * @param $shippingMethod
     * @return mixed
     */
    public function isChronoMethod($shippingMethod) {
        $carrier = $this->_carrierFactory->get($shippingMethod);
        return $carrier ? $carrier->getIsChronoMethod() : false;
    }


    /********************************** Gestion Livraison samedi ******************************/

    /**
     * @return bool
     */
    public function isSendingDay() {
        $shipping_days = $this->getSaturdayShippingDays();
        $current_date = $this->getCurrentTimeByZone("Europe/Paris", "Y-m-d H:i:s");
        $current_datetime = explode(' ', $current_date);

        //get timestamps
        $start_timestamp = strtotime($current_datetime[0] . " " . $shipping_days['starttime']);
        $end_timestamp = strtotime($current_datetime[0] . " " . $shipping_days['endtime']);
        $current_timestamp = strtotime($current_date);

        $sending_day = false;
        if (  $current_timestamp >= $start_timestamp && $current_timestamp <= $end_timestamp  ) {
            $sending_day = true;
        }
        return $sending_day;
    }

    /**
     * @return array
     */
    public function getSaturdayShippingDays() {

        $starday = explode(":",$this->scopeConfig->getValue("chronorelais/saturday/startday"));
        $endday = explode(":",$this->scopeConfig->getValue("chronorelais/saturday/endday"));

        $saturdayDays = array();
        $saturdayDays['startday'] = $starday[0];
        $saturdayDays['starttime'] = $starday[1].':'.$starday[2].':00';
        $saturdayDays['endday'] = $endday[0];
        $saturdayDays['endtime'] = $endday[1].':'.$endday[2].':00';

        return $saturdayDays;
    }

    /**
     * @param string $timezone
     * @param string $format
     * @return string
     */
    public function getCurrentTimeByZone($timezone="Europe/Paris", $format="l H:i") {
        $d = new \DateTime("now", new \DateTimeZone($timezone));
        return $d->format($format);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getLivraisonSamediStatus($order_id) {

        $coll = $this->_orderExportStatusCollectionFactory->create();
        $coll->addFieldToFilter("order_id",$order_id)->addFieldToSelect("livraison_le_samedi");

        $status = $coll->getFirstItem();
        return $status->getData('livraison_le_samedi');
    }

    /**
     * @return int
     */
    public function getMaxAdValoremAmount() {
        return 20000;
    }


    /*** gestion retour ***/


    /**
     * @param $productCodes
     * @return array
     */
    public function getReturnProductCodesAllowed($productCodes)
    {
        $possibleReturnProductCode = array(
            static::CHRONOPOST_REVERSE_R,
            static::CHRONOPOST_REVERSE_S,
            static::CHRONOPOST_REVERSE_T,
            static::CHRONOPOST_REVERSE_U
        );
        $returnProductCode = array();
        foreach ($productCodes as $code) {
            if (in_array($code, $possibleReturnProductCode)) {
                array_push($returnProductCode, $code);
            }
        }

        return (sizeof($returnProductCode) > 0) ? $returnProductCode : array(static::CHRONOPOST_REVERSE_DEFAULT);

    }

    /**
     * @param $code
     * @return string
     */
    public function getReturnServiceCode($code)
    {
        switch ($code) {
            case static::CHRONOPOST_REVERSE_R:
                return static::CHRONOPOST_REVERSE_R_SERVICE;
                break;
            case static::CHRONOPOST_REVERSE_S:
                return static::CHRONOPOST_REVERSE_S_SERVICE;
                break;
            case static::CHRONOPOST_REVERSE_T:
                return static::CHRONOPOST_REVERSE_T_SERVICE;
                break;
            case static::CHRONOPOST_REVERSE_U:
                return static::CHRONOPOST_REVERSE_U_SERVICE;
                break;
            case static::CHRONOPOST_REVERSE_DEFAULT:
                return static::CHRONOPOST_REVERSE_DEFAULT_SERVICE;
                break;
            default :
                return static::CHRONOPOST_REVERSE_DEFAULT_SERVICE;
                break;
        }
    }

    /**
     * @return array
     */
    public function getMatriceReturnCode()
    {
        return array(
            static::CHRONOPOST_REVERSE_R       => array(
                array(static::CHRONOPOST_REVERSE_R),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_U)
            ),
            static::CHRONOPOST_REVERSE_S       => array(
                array(static::CHRONOPOST_REVERSE_S),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_S),
                array(static::CHRONOPOST_REVERSE_S, static::CHRONOPOST_REVERSE_U),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_S, static::CHRONOPOST_REVERSE_U)
            ),
            static::CHRONOPOST_REVERSE_U       => array(
                array(static::CHRONOPOST_REVERSE_U)
            ),
            static::CHRONOPOST_REVERSE_T       => array(
                array(static::CHRONOPOST_REVERSE_T),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_T),
                array(static::CHRONOPOST_REVERSE_S, static::CHRONOPOST_REVERSE_T),
                array(static::CHRONOPOST_REVERSE_T, static::CHRONOPOST_REVERSE_U),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_S, static::CHRONOPOST_REVERSE_T),
                array(static::CHRONOPOST_REVERSE_R, static::CHRONOPOST_REVERSE_T, static::CHRONOPOST_REVERSE_U),
                array(static::CHRONOPOST_REVERSE_S, static::CHRONOPOST_REVERSE_T, static::CHRONOPOST_REVERSE_U),
                array(
                    static::CHRONOPOST_REVERSE_R,
                    static::CHRONOPOST_REVERSE_S,
                    static::CHRONOPOST_REVERSE_T,
                    static::CHRONOPOST_REVERSE_U
                )
            ),
            static::CHRONOPOST_REVERSE_DEFAULT => array(
                array(static::CHRONOPOST_REVERSE_DEFAULT)
            )
        );
    }

    /**
     * @param \Magento\sales\Model\Order $_order
     * @return bool
     */
    public function hasOptionBAL($_order) {
        $shippingMethod = explode('_', $_order->getShippingMethod());
        $shippingMethod = $shippingMethod[1];
        $carrier = $this->_carrierFactory->get($shippingMethod);
        return $carrier && $carrier->getIsChronoMethod() ? $carrier->optionBalEnable() : false;
    }

    /**
     * @param $_order
     * @return int|mixed
     */
    public function getOrderAdValorem($_order) {
        $totalAdValorem = 0;

        if($this->getConfig("chronorelais/assurance/enabled")) {
            $minAmount = $this->getConfig("chronorelais/assurance/amount");
            $maxAmount = $this->getMaxAdValoremAmount();

            $items = $_order->getAllItems();

            $totalAdValorem = 0;

            foreach($items as $item) {
                $totalAdValorem += $item->getPrice() * $item->getQtyOrdered();
            }
            $totalAdValorem = min($totalAdValorem,$maxAmount);
            /* Si montant < au montant minimum ad valorem => pas d'assurance */
            if($totalAdValorem < $minAmount) {
                $totalAdValorem = 0;
            }
        }

        return $totalAdValorem;
    }


}
