<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_RewardPoints
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\RewardPoints\Controller\Adminhtml\Mui\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Element\UiComponentFactory;

class Render extends \Magento\Ui\Controller\Adminhtml\Index\Render
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @param Context $context
     * @param UiComponentFactory $factory
     */
    public function __construct(
    	Context $context,
    	UiComponentFactory $factory,
    	\Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Framework\Registry $coreRegistry
    ) {
        parent::__construct($context, $factory);
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Action for AJAX request
     *
     * @return void
     */
    public function execute()
    {
    	$store = $this->getRequest()->getParam('store');
    	$this->coreRegistry->register('current_store', $store);
        $type = $this->getRequest()->getParam('type');
        $this->coreRegistry->register('current_type', $type);
    	parent::execute();
    }
}