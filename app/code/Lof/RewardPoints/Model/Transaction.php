<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_RewardPoints
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\RewardPoints\Model;

class Transaction extends \Magento\Framework\Model\AbstractModel
{
	/**
	 * Transaction Cache Tag
	 */
    const CACHE_TAG           = 'rewardpoints_transaction';
    
    const EARNED_CODE         = 'order_earn-';
    
    const SPENT_CODE          = 'order_spen-';
    
    const CANCEL_EARNED_CODE  = 'order_earn_cancel-';
    
    const SPENT_RESTORE       = 'order_spend-restore-';
    
    const SPENT_AMOUNT        = 'order_spend_amount-';
    
    const CANCEL_SPENT_AMOUNT = 'order_cancel_spent_amount-';

    /**
     * Transaction States
     */
    const STATE_NEW        = 'new';
    
    const STATE_PROCESSING = 'processing';
    
    const STATE_COMPLETE   = 'complete';
    
    const STATE_CLOSED     = 'closed';
    
    const STATE_CANCELED   = 'canceled';
    
    const STATE_HOLDED     = 'holded';
    
    const STATE_EXPIRED    = 'expired';
    
    const STATE_FAILED     = 'failed';

    /**
     * Transaction Actions
     */
    const EARNING_ORDER          = 'earning_order';
    
    const EARNING_CREDITMEMO     = 'earning_creditmemo';
    
    const EARNING_CANCELED       = 'earning_canceled';
    
    const EARNING_CLOSED         = 'earning_closed';
    
    const SPENDING_ORDER         = 'spending_order';
    
    const SPENDING_CREDITMEMO    = 'spending_creditmemo';
    
    const SPENDING_CANCELED      = 'spending_cancel';
    
    const SPENDING_CLOSED        = 'spending_close';
    
    const ADMIN_ADD_TRANSACTION  = 'admin_add';
    
    const CUSTOMER_NEWSLETTER    = 'customer_newsletter';
    
    const CUSTOMER_REGISTER      = 'customer_register';
    
    const CUSTOMER_LOGIN         = 'customer_login';
    
    const CUSTOMER_FACBOOK_LIKE  = 'customer_fblike';
    
    const CUSTOMER_FACBOOK_SHARE = 'customer_fblike';
    
    const CUSTOMER_GOOGLE_PLUS   = 'customer_ggplus';
    
    const CUSTOMER_PINTEREST_PIN = 'customer_pin';
    
    const CUSTOMER_BIRTHDAY      = 'customer_birthday';
    
    const CUSTOMER_REVIEW        = 'customer_review';
    
    const CUSTOMER_TWITTER_TWEET = 'customer_tweet';


	/**
	 * @var string
	 */
	protected $_cacheTag = 'rewardpoints_transaction';

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var \Lof\RewardPoints\Helper\Customer
     */
    protected $rewardsCustomer;

    /**
     * @var \Lof\RewardPoints\Model\ResourceModel\Purchase\CollectionFactory
     */
    protected $purchaseCollectionFactory;

    /**
     * @var \Lof\RewardPoints\Logger\Logger
     */
    protected $rewardsLogger;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer = null;

    /**
     * @param \Magento\Framework\Model\Context                                  $context                   
     * @param \Magento\Framework\Registry                                       $registry                  
     * @param \Lof\RewardPoints\Model\ResourceModel\Transaction|null            $resource                  
     * @param \Lof\RewardPoints\Model\ResourceModel\Transaction\Collection|null $resourceCollection        
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory  $customerCollectionFactory 
     * @param \Lof\RewardPoints\Helper\Customer                                 $rewardsCustomer           
     * @param \Lof\RewardPoints\Model\ResourceModel\Purchase\CollectionFactory  $purchaseCollectionFactory 
     * @param \Lof\RewardPoints\Logger\Logger                                   $rewardsLogger             
     * @param array                                                             $data                      
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\RewardPoints\Model\ResourceModel\Transaction $resource = null,
        \Lof\RewardPoints\Model\ResourceModel\Transaction\Collection $resourceCollection = null,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Lof\RewardPoints\Helper\Customer $rewardsCustomer,
        \Lof\RewardPoints\Model\ResourceModel\Purchase\CollectionFactory $purchaseCollectionFactory,
        \Lof\RewardPoints\Logger\Logger $rewardsLogger,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->rewardsCustomer           = $rewardsCustomer;
        $this->purchaseCollectionFactory = $purchaseCollectionFactory;
        $this->rewardsLogger             = $rewardsLogger;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Lof\RewardPoints\Model\ResourceModel\Transaction');
    }

    public function getAvailableStatuses($empty = false)
    {
        $options = [
            self::STATE_NEW        => __('New'),
            self::STATE_PROCESSING => __('Processing'),
            self::STATE_COMPLETE   => __('Complete'),
            self::STATE_CLOSED     => __('Closed'),
            self::STATE_CANCELED   => __('Canceled'),
            self::STATE_HOLDED     => __('Holded'),
            self::STATE_FAILED     => __('Failed'),
            self::STATE_EXPIRED    => __('Expired')
        ];
        if ($empty) {
            array_unshift($options, "");
        }
        return $options;
    }

    public function getStatusLabel($status = '')
    {
        $label = __('Empty');
        if ($status == '') {
            $status = $this->getData('status');
        }
            
        $availableStatuses = $this->getAvailableStatuses();
        foreach ($availableStatuses as $key => $value) {
            if ($key == $status) {
                $label = $value;
                break;
            }
        }
        return $label;
    }

    /**
     * @return bool|\Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if ($this->customer === null && $this->getCustomerId()) {
                $customer = $this->customerCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', $this->getCustomerId())
                    ->getFirstItem();
                $rewardsCustomer = $this->rewardsCustomer->getCustomer($customer->getId());
                foreach ($rewardsCustomer->getData() as $k => $v) {
                    $customer->setData($k, $v);
                }
                $this->customer = $customer;
        }
        return $this->customer;
    }

    public function getPurchase()
    {
        $collection = $this->purchaseCollectionFactory->create();
        $purchase = $collection->addFieldToFilter('order_id', $this->getOrderId())->getFirstItem();
        return $purchase;
    }

    public function getRewardsCustomer()
    {
        $rewardsCustomer = $this->rewardsCustomer->getCustomer($this->getCustomerId());
        return $rewardsCustomer;
    }

    public function getActionLabel()
    {
        $action = $this->getData('action');
        $label  = $this->getActions($action);
        return $label;
    }

    /**
     * @return int
     */
    public function getDaysLeft()
    {
        if ($expires = $this->getData('expires_at')) {
            $diff = strtotime($expires) - time();
            $days = (int) ($diff / 60 / 60 / 24);

            // Debug
            if ($days<0) {
                $this->rewardsLogger->addError('Transaction Day Left is smaller than 0');
                $days = 1;
            }

            return $days;
        }
        return;
    }

    public function getActions($code = '')
    {
        $options = [
            self::EARNING_ORDER          => __('Earn points for purchasing order'),
            self::EARNING_CREDITMEMO     => __('Retrive points for refunding order'),
            self::EARNING_CANCELED       => __('Retrive points for canceling order'),
            self::EARNING_CLOSED         => __('Retrive points for closing order'),
            self::SPENDING_ORDER         => __('Spend points to purchase order'),
            self::SPENDING_CREDITMEMO    => __('Retrive spent points on refunded order'),
            self::SPENDING_CANCELED      => __('Retrive spent points on canceled order'),
            self::SPENDING_CLOSED        => __('Retrive spent points on cloded order'),
            self::ADMIN_ADD_TRANSACTION  => __('Changed By Admin'),
            self::CUSTOMER_NEWSLETTER    => __('Receive point for subscribing to newsletter'),
            self::CUSTOMER_REGISTER      => __('Receive point for registering successfully'),
            self::CUSTOMER_LOGIN         => __('Receive point for logging in successfully'),
            self::CUSTOMER_FACBOOK_LIKE  => __('Receive points for Facebook like'),
            self::CUSTOMER_FACBOOK_SHARE => __('Receive point for sharing via Facebook'),
            self::CUSTOMER_GOOGLE_PLUS   => __('Receive point for +1 via Google'),
            self::CUSTOMER_PINTEREST_PIN => __('Receive points pin via Pinterest'),
            self::CUSTOMER_BIRTHDAY      => __('Receive points for birthday'),
            self::CUSTOMER_REVIEW        => __('Receive point for reviewing a product'),
            self::CUSTOMER_TWITTER_TWEET => __('Recieve point for tweeting via Twitter'),
        ];
        if ($code) {
            foreach ($options as $k => $v) {
                if ($k == $code ) {
                    return $v;
                }
            }
            return false;
        }
        return $options;
    }
}