<?php
namespace WebMeridian\JsonImport\Model;
class Log extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'webmeridian_jsonimport_logs';

    protected $_cacheTag = 'webmeridian_jsonimport_logs';

    protected $_eventPrefix = 'webmeridian_jsonimport_logs';

    protected function _construct()
    {
        $this->_init('WebMeridian\JsonImport\Model\ResourceModel\Log');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}