<?php
namespace WebMeridian\JsonImport\Model\ResourceModel\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'log_id';
    protected $_eventPrefix = 'webmeridian_jsonimport_log_collection';
    protected $_eventObject = 'log_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('WebMeridian\JsonImport\Model\Log', 'WebMeridian\JsonImport\Model\ResourceModel\Log');
    }

}