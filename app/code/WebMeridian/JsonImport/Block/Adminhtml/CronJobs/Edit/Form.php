<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace WebMeridian\JsonImport\Block\Adminhtml\CronJobs\Edit;

/**
 * Adminhtml staff edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    const CATEGORY_ID_MARGUES = 122;
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    protected $_status;

    /**
     * \Magento\Catalog\Model\CategoryFactory $_categoryFactory
     */
    protected $_categoryFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('cron_job_form');
        $this->setTitle(__('Cron Job Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Webmeridian\JsonImport\Model\CronJobs $model */
        $model = $this->_coreRegistry->registry('cron_job');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => [
                'id' => 'edit_form',
                'action' => $this->getData('action'),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            ]]
        );

        $form->setHtmlIdPrefix('cron_job_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Cron Job information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('cron_id', 'hidden', ['name' => 'cron_id']);
        }

        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'required' => true,
                'options' => [
                    'new' => __('New'),
                    'in_progress' => __('In progress'),
                    'done' => __('Done'),
                ]
            ]
        );

        $fieldset->addField(
            'mapping',
            'select',
            [
                'label' => __('Mapping'),
                'title' => __('Mapping'),
                'name' => 'mapping',
                'required' => true,
                'options' => [
                    'karcher' => __('Karcher Products'),
                    'karcher_accessoires' => __('Karcher Accessoires'),
                    'facom' => __('FACOM Products'),
                    'festool' => __('Festool Products'),
                    'festool_accessoires' => __('Festool Accessoires'),
                    'noirot' => __('Noirot Products'),
                    'fein' => __('Fein Products'),
                    'fein_accessories' => __('Fein Accessories'),
                    'bosch' => __('Bosch Products'),
                    'bosch_accessories' => __('Bosch Accessories'),
                    'bosch_2_accessories' => __('Bosch 2 Accessories'),
                    'chauvin_arnoux' => __('Chauvin-Arnoux Products'),
                    'metrix' => __('Metrix Products'),
                    'makita' => __('Makita Products'),
                    'dolmar' => __('Dolmar Products'),
                    'stanley' => __('Stanley Products'),
                    'dewalt' => __('Dewalt Products'),
                    'grohe' => __('Grohe Products'),
                    'hikoki' => __('Hikoki Products'),
                    'geberit' => __('Geberit Products'),
                    'porcher' => __('Porcher Products')
                ]
            ]
        );

        $fieldset->addField(
            'file_name',
            'text',
            ['name' => 'file_name', 'label' => __('File Name'), 'title' => __('File Name'), 'required' => true]
        );

        $fieldset->addField('MAX_FILE_SIZE', 'hidden', ['name' => 'MAX_FILE_SIZE', 'value' => '300000']);

        $fieldset->addField(
            'file',
            'file',
            ['name' => 'file', 'label' => __('Json File'), 'title' => __('Json File'), 'required' => true]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}