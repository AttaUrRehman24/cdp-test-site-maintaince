<?php


namespace WebMeridian\JsonImport\Cron;

class Import
{
    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var \WebMeridian\JsonImport\Model\ResourceModel\CronJobs\CollectionFactory $collectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Mapping\Karcher
     */
    protected $karcher;

    /**
     * @var Mapping\KarcherAcessories
     */
    protected $karcherAccessories;

    /**
     * @var Mapping\WrongMapping
     */
    protected $wrongMapping;

    /**
     * @var Mapping\Facon
     */
    protected $facon;

    /**
     * @var Mapping\Festool
     */
    protected $festool;

    /**
     * @var Mapping\FestoolAcesssories
     */
    protected $festoolAcesssories;

    /**
     * @var Mapping\Noirot
     */
    protected $noirot;

    /**
     * @var Mapping\Fein
     */
    protected $fein;

    /**
     * @var Mapping\Bosch
     */
    protected $bosch;

    /**
     * @var Mapping\FeinAcessories
     */
    protected $feinAccessories;

    /**
     * @var Mapping\Metrix
     */
    protected $metrix;

    /**
     * @var Mapping\BoschAcessories
     */
    protected $boschAccessories;

    /**
     * @var Mapping\Bosch2Acessories
     */
    protected $bosch2Accessories;

    /**
     * @var Mapping\Makita
     */
    protected $makita;

    /**
     * @var Mapping\Dolmar
     */
    protected $dolmar;

    /**
     * @var Mapping\Stanley
     */
    protected $stanley;

      /**
     * @var Mapping\Dewalt
     */
    protected $dewalt;

    /**
     * @var Mapping\GROHE
     */
    protected $grohe;

    /**
     * @var Mapping\HIKOKI
     */
    protected $hikoki;

    /**
     * @var Mapping\GEBERiT
     */
    protected $geberit;
    /**
     * @var Mapping\GEBERiT
     */
    protected $porcher;

    /**
     * @var Core
     */
    protected $core;

    /**
     * Import constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param Core $core
     * @param \WebMeridian\JsonImport\Model\ResourceModel\CronJobs\CollectionFactory $collectionFactory
     * @param Mapping\Karcher $karcher
     * @param Mapping\KarcherAcessories $karcherAcessories
     * @param Mapping\WrongMapping $wrongMapping
     * @param Mapping\Facon $facon
     * @param Mapping\Festool $festool
     * @param Mapping\FestoolAcesssories $festoolAcesssories
     * @param Mapping\Noirot $noirot
     * @param Mapping\Fein $fein
     * @param Mapping\Bosch $bosch
     * @param Mapping\FeinAcessories $feinAcessories
     * @param Mapping\ChauvinArnoux $chauvinArnoux
     * @param Mapping\Metrix $metrix
     * @param Mapping\BoschAcessories $boschAcessories
     * @param Mapping\Makita $makita
     * @param Mapping\Stanley $stanley
     * @param Mapping\Dewalt $dewalt
     * @param Mapping\Grohe $grohe
     * @param Mapping\Hikoki $hikoki
     * @param Mapping\GEBERIT $geberit
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \WebMeridian\JsonImport\Cron\Core $core,
        \WebMeridian\JsonImport\Model\ResourceModel\CronJobs\CollectionFactory $collectionFactory,
        \WebMeridian\JsonImport\Cron\Mapping\Karcher $karcher,
        \WebMeridian\JsonImport\Cron\Mapping\KarcherAcessories $karcherAcessories,
        \WebMeridian\JsonImport\Cron\Mapping\WrongMapping $wrongMapping,
        \WebMeridian\JsonImport\Cron\Mapping\Facon $facon,
        \WebMeridian\JsonImport\Cron\Mapping\Festool $festool,
        \WebMeridian\JsonImport\Cron\Mapping\FestoolAcesssories $festoolAcesssories,
        \WebMeridian\JsonImport\Cron\Mapping\Noirot $noirot,
        \WebMeridian\JsonImport\Cron\Mapping\Fein $fein,
        \WebMeridian\JsonImport\Cron\Mapping\Bosch $bosch,
        \WebMeridian\JsonImport\Cron\Mapping\FeinAcessories $feinAcessories,
        \WebMeridian\JsonImport\Cron\Mapping\ChauvinArnoux $chauvinArnoux,
        \WebMeridian\JsonImport\Cron\Mapping\Metrix $metrix,
        \WebMeridian\JsonImport\Cron\Mapping\BoschAcessories $boschAcessories,
        \WebMeridian\JsonImport\Cron\Mapping\Bosch2Acessories $bosch2Acessories,
        \WebMeridian\JsonImport\Cron\Mapping\Makita $makita,
        \WebMeridian\JsonImport\Cron\Mapping\Dolmar $dolmar,
        \WebMeridian\JsonImport\Cron\Mapping\Stanley $stanley,
        \WebMeridian\JsonImport\Cron\Mapping\Dewalt $dewalt,
        \WebMeridian\JsonImport\Cron\Mapping\Grohe $grohe,
        \WebMeridian\JsonImport\Cron\Mapping\Hikoki $hikoki,
        \WebMeridian\JsonImport\Cron\Mapping\Geberit $geberit,
        \WebMeridian\JsonImport\Cron\Mapping\Porcher $porcher
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
        $this->core = $core;
        $this->karcher = $karcher;
        $this->karcherAccessories = $karcherAcessories;
        $this->wrongMapping = $wrongMapping;
        $this->facon = $facon;
        $this->festool = $festool;
        $this->festoolAcesssories = $festoolAcesssories;
        $this->noirot = $noirot;
        $this->fein = $fein;
        $this->bosch = $bosch;
        $this->feinAccessories = $feinAcessories;
        $this->chauvinArnoux = $chauvinArnoux;
        $this->metrix = $metrix;
        $this->boschAccessories = $boschAcessories;
        $this->bosch2Accessories = $bosch2Acessories;
        $this->makita = $makita;
        $this->dolmar = $dolmar;
        $this->stanley = $stanley;
        $this->dewalt = $dewalt;
        $this->grohe = $grohe;
        $this->hikoki = $hikoki;
        $this->geberit = $geberit;
        $this->porcher = $porcher;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        /** @var \WebMeridian\JsonImport\Model\ResourceModel\CronJobs\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('status', 'new');
        if (count($collection)) {
            /** @var \WebMeridian\JsonImport\Model\CronJobs $item */
            foreach ($collection as $item) {
                $this->runJob($item);
            }
        }

        $this->logger->addInfo(__("Cronjob Import is executed."));
    }

    public function getImportModelByMapping($mapping){
        $importModel = null;
        switch ($mapping){
            case 'karcher' : {
                $importModel = $this->karcher;
                break;
            }
            case 'karcher_accessoires' : {
                $importModel = $this->karcherAccessories;
                break;
            }
            case 'facom' : {
                $importModel = $this->facon;
                break;
            }
            case 'festool' : {
                $importModel = $this->festool;
                break;
            }
            case 'festool_accessoires' : {
                $importModel = $this->festoolAcesssories;
                break;
            }
            case 'noirot' : {
                $importModel = $this->noirot;
                break;
            }
            case 'fein' : {
                $importModel = $this->fein;
                break;
            }
            case 'bosch' : {
                $importModel = $this->bosch;
                break;
            }
            case 'bosch_accessories' : {
                $importModel = $this->boschAccessories;
                break;
            }
            case 'bosch_2_accessories' : {
                $importModel = $this->bosch2Accessories;
                break;
            }
            case 'fein_accessories' : {
                $importModel = $this->feinAccessories;
                break;
            }
            case 'chauvin_arnoux' : {
                $importModel = $this->chauvinArnoux;
                break;
            }
            case 'metrix' : {
                $importModel = $this->metrix;
                break;
            }
            case 'makita' : {
                $importModel = $this->makita;
                break;
            }
            case 'dolmar' : {
                $importModel = $this->dolmar;
                break;
            }
            case 'stanley' : {
                $importModel = $this->stanley;
                break;
            }
            case 'dewalt' : {
                $importModel = $this->dewalt;
                break;
            }
            case 'grohe' : {
                $importModel = $this->grohe;
                break;
            }
            case 'hikoki' : {
                $importModel = $this->hikoki;
                break;
            }
            case 'geberit' : {
                $importModel = $this->geberit;
                break;
            }
            case 'porcher' : {
                $importModel = $this->porcher;
                break;
            }
            default : {
                $importModel = $this->wrongMapping;
                break;
            }
        }
        return $importModel;
    }

    /** @var \WebMeridian\JsonImport\Model\CronJobs $item */
    public function runJob($item){
        $this->getImportModelByMapping($item->getMapping())->runImport($item);
    }

    /**
     * @param bool $status
     */
    public function changePrintStatus($status = false){
        $this->core->changePrintStatus($status);
    }
}