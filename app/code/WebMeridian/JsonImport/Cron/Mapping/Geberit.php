<?php


namespace WebMeridian\JsonImport\Cron\Mapping;

use Braintree\Exception;
use Magento\Framework\App\ResourceConnection;
use WebMeridian\JsonImport\Cron\Core;

class Geberit
{

    protected $directoryList;
    protected $productFactory;
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     * @since 101.0.0
     */
    protected $mediaConfig;

    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * \Magento\Catalog\Model\CategoryFactory $_categoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @var \Magento\Framework\Filter\TranslitUrl $translitUrl
     */
    protected $translitUrl;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite
     */
    protected $urlRewrite;

    /**
     * @var Core $core
     */
    protected $core;


    /**
     * KarcherImport constructor.
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directory_list
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param Core $core
     */

    public function __construct(
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Eav\Model\Config $eavConfig,
        Core $core
    )
    {
        $this->directoryList = $directory_list;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->_resourceConnection = $resourceConnection;
        $this->_categoryFactory = $categoryFactory;
        $this->eavConfig = $eavConfig;
        $this->core = $core;
    }

    /**
     * @param $cronJobsModel \WebMeridian\JsonImport\Model\CronJobs
     */
    public function runImport($cronJobsModel)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1G');
        $cronJobsModel->setStatus('in_progress');
        $cronJobsModel->save();
        $this->core->insertLog($cronJobsModel, __('Start import'));
        $importData = $this->getFileData($cronJobsModel);
        //$isValid = $this->isValidImportData($importData, $cronJobsModel, true);

        $importData = $this->reformatJsonData($importData);


        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importData.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($importData,true));

        $this->importProducts($importData, $cronJobsModel);
        $this->core->insertLog($cronJobsModel, __('Import done'));
        /*if ($isValid) {
            $this->importProducts($importData, $cronJobsModel);
            $this->core->insertLog($cronJobsModel, __('Import done'));
        } else {
            $this->core->insertLog($cronJobsModel, __('The file is not valid'));
            $cronJobsModel->setStatus('not_valid');
            $cronJobsModel->save();
        }*/

        $cronJobsModel->setStatus('done');
        $cronJobsModel->save();

    }

    /**
     * @param $dataJson
     * @return array
     */
    protected function reformatJsonData($dataJson)
    {
        $returnData = [];

        foreach ($dataJson as $links){
            foreach ($links as $link) {               
                if($this->checkData($link,'product_ref') && !$this->checkData($link,'links')) {
                    $returnData[] = $link;
                } elseif ($this->checkData($link, 'links') && $this->checkData($link,'link')) {
                    foreach ($link['links'] as $data) {
                        if ($this->checkData($data, 'links') && $this->checkData($data,'link')) {
                            foreach ($data['links'] as $d) {
                                $returnData[] = $d;
                            }
                            unset($data['links']);
                        } elseif($this->checkData($data,'product_ref') && !$this->checkData($data,'links')) {
                            $returnData[] = $data;
                        }
                    }
                    unset($link['links']);
                }

            }
        }

        return $returnData;
    }

    /**
     * @param string $name
     * @param string $sku
     * @return mixed
     */
    public function removeSkuFromName($name, $sku){
        return preg_replace('/^'.$sku.' /', '', $name);
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function getCorrectSku($sku){

        $sku=explode('?',$sku,2);
        $skud=explode('&',$sku[1],2);
        $skudd=explode('=',$skud[0],2);
        return $skudd[1];

    }

    function getSCPrice($sku){
        parse_str(parse_url($sku, PHP_URL_QUERY),$output);
        return $output;        
   }



            

    public function GetURL($URL)
    {
        $ar = str_replace('http://','https://',$URL);
        return $ar;
    }

  

    public function importProducts($importData, $cronJobsModel = null)
    {
        $manufacturer = $this->getManufacturerMarques($cronJobsModel->getMapping());

        foreach ($importData as $data) {

            $productData = [];
            $sku = $this->getCorrectSku($data['link']);
            $correctName = $this->removeSkuFromName($data['product_title'], $sku);
            $productData['name'] = $correctName;
            $priceo=$this->getSCPrice($data['link']);
            $productData['sku'] = $sku;
            $productData['price'] = $priceo['pv']?$priceo['pv']: 1000;
            $productData['cost'] = $priceo['pa']?$priceo['pa']: 100;
            /** TODO load and save file */
            if($this->checkData($data, 'pdf_file_url')){
                $this->pdf($data['pdf_file_url'],$sku);                
                $productData['pdf_file'] ='https://test.comptoirdespros.com/pdf/GEBERIT/'.$sku.'.pdf';
                $productData['pdf_file_name']=$data['pdf_file'];                
            }

            /** TODO set default KATEGORY as Karcher */
            $productData['manufacturer'] = $manufacturer;

            //$productData['short_description'] = $this->checkData($data, 'short_description') ? $this->generateShortDescriptionHtml($data['short_description']) : '';
            //$productData['heading_desc'] = 'Description';
           // $productData['description'] = $this->checkData($data,'description_text') ? $this->generateDescriptionHtml($data['description_text']) : '';
            /** TODO in Capital letter */
            $productData['heading_1'] =  'CARACTERISTIQUES TECHNIQUES';

            $productData['heading_1']=$this->checkData($data,'description_1_title') ? $data['description_1_title'] : '';
            $productData['short_desc_1'] = $this->checkData($data,'description_1') ? $this->generateDescription1Html($data['description_1']) : '';
            $productData['heading_2'] = $this->checkData($data,'description_2_title') ? $data['description_2_title'] : '';
            $productData['short_desc_2'] = ($this->checkData($data,'description_2')) ? $this->generateDescription2Html($data['description_2']) : '';

            $productData['heading_3'] = $this->checkData($data,'description_3_title') ? $data['description_3_title'] : '';
            $productData['short_desc_3'] = $this->checkData($data,'description_3') ? $this->generateDescription3Html($data['description_3']) : '';

            $productData['heading_4'] = $this->checkData($data,'description_4_title') ? $data['description_4_title'] : '';
            $productData['short_desc_4'] = $this->checkData($data,'description_4') ? $this->generateDescription4Html($data['description_4']) : '';

            //$productData['heading_5'] = $this->checkData($data,'description_5_title') ? $data['description_5_title'] : '';
            $productData['short_desc_5'] = $this->checkData($data,'description_5') ? $this->generateDescription5Html($data['description_5']) : '';

            // $productData['heading_6'] = $this->checkData($data,'description_6_title') ? $data['description_6_title'] : '';
            // $productData['short_desc_6'] = $this->checkData($data,'description_6') ? $this->generateDescription6Html($data['description_6']) : '';

            $productData['karcher_arret_machine'] = 374;

            $productData['big_image'] = $this->checkData($data,'big_image_url') ? $data['big_image_url'] : '';
            $productData['photo2_url'] =  $this->checkData($data,'photo2_url') ? $data['photo2_url'] : '';

            $productData['thumbnails'] =  $this->checkData($data, 'thumbnails') ? $data['thumbnails']: [];


            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/pdflinks.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(print_r($productData,true));

            $this->importProduct($productData, $cronJobsModel);

            /** TODO insert BIG IMAGE url */
        }
    }


    private function pdf($fileUrl,$saveTo1){

                        //      $fileUrl = 'https://catalog.geberit.fr/fr-FR/pdf/product/PRO_101072';
                        
                        //The path & filename to save to.
                        //$saveTo = '/home/www/comptoirdespros.com/pub/pdf/GEBERIT/'.$saveTo1;
                        $fp = fopen('/home/www/comptoirdespros.com/pub/pdf/GEBERIT/'.$saveTo1.'.pdf', 'w+');
                        if($fp === false){
                            throw new Exception('Could not open: ' . $saveTo);
                        }
                        $ch = curl_init($fileUrl);
                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                        curl_exec($ch);
                        if(curl_errno($ch)){
                            throw new Exception(curl_error($ch));
                        }
                        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);                        
                        fclose($fp);
                        if($statusCode == 200){
                            echo 'Downloaded!';
                        } else{
                            echo "Status Code: " . $statusCode;
                        }

    }

    public function importProduct($productData, $cronJobsModel = null)
    {
        $category = $this->getCategoryMarques($cronJobsModel->getMapping());
        $categoryId = 0;
        if(false != $category){
            $categoryId = $category->getId();
        }
        try {

            /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
            $product = $this->productFactory->create();
            $productId = $product->getIdBySku($productData['sku']);
            $product->setName($productData['name']);
            $product->setSku($productData['sku']);
            $product->setPrice($productData['price']);
            $product->setCost($productData['cost']);
            $productKey = $this->core->getCorrectKey($productData['name'], $productData['sku'], $productId);
            $product->setUrlKey($productKey);
            $product->setStoreId(0);
            if($this->checkData($productData,'pdf_file')) {
                $product->setPdfFile($this->core->getPdfFile($productData['pdf_file'], $product));
            }
            $product->setManufacturer($productData['manufacturer']);
            //$product->setShortDescription($productData['short_description']);
            //$product->setHeadingDesc($productData['heading_desc']);
            //$product->setDescription($productData['description']);

            $product->setHeading1($productData['heading_1']);
            $product->setShortDesc1($productData['short_desc_1']);
            $product->setVisibility1(1);

            $product->setHeading2($productData['heading_2']);
            $product->setShortDesc2($productData['short_desc_2']);
            $product->setVisibility2(1);

            $product->setHeading3($productData['heading_3']);
            $product->setShortDesc3($productData['short_desc_3']);
            $product->setVisibility3(1);

            $product->setHeading4($productData['heading_4']);
            $product->setShortDesc4($productData['short_desc_4']);
            $product->setVisibility4(1);

            //$product->setHeading5($productData['heading_5']);
            $product->setShortDesc5($productData['short_desc_5']);
            $product->setVisibility5(1);

            // $product->setHeading6($productData['heading_6']);
            // $product->setShortDesc6($productData['short_desc_6']);
            // $product->setVisibility6(1);

            $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
            $product->setVisibility(4);
            $product->setAttributeSetId(4); // Default attribute set for products
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);

            $product->setStockData(['qty' => 0, 'is_in_stock' => 1]);
            $product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => 1]);

            //$product->setCustomAttribute('tax_class_id', $taxClassId);

            $product->setKarcherArretMachine($productData['karcher_arret_machine']);

            if($categoryId){
                $product->setCategoryIds([$categoryId]);
            }

            $product = $this->productRepository->save($product);

            /** remove images */
            $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
            if($existingMediaGalleryEntries){
                foreach ($existingMediaGalleryEntries as $key => $entry) {
                    unset($existingMediaGalleryEntries[$key]);
                }
            }
            $product->setMediaGalleryEntries($existingMediaGalleryEntries);
            $product = $this->productRepository->save($product);

            /** add main image */
            $imagePath = $this->core->getMainImage($productData['big_image'], $product);
            if(trim($imagePath) != ''){
                $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
                $product->save();
            }

            /**
             * Insert thumbnails
             */
            if(count($productData['thumbnails'])){
                $counter = 0;
                foreach ($productData['thumbnails'] as $thumbnail) {
                    $counter ++;
                    if($counter == 1){
                        continue;
                    }
                    if(isset($thumbnail['image']) && trim($thumbnail['image'])){
                        $imagePath = $this->core->getAdditionalImage($thumbnail['image'], $product);
                        if (trim($imagePath) != '') {
                            $product->addImageToMediaGallery($imagePath, [], false, false);
                            $product->save();
                        }
                    }
                }
            }

            if(trim($productData['photo2_url'])){
                $imagePath = $this->core->getAdditionalImage($productData['photo2_url'], $product);
                if(trim($imagePath) != ''){
                    $product->addImageToMediaGallery($imagePath, [], false, false);
                    $product->save();
                }
            }

            $product->save();


        } catch (Exception $exception) {
            if(!is_null($cronJobsModel)){
                $this->core->insertLog($cronJobsModel, $exception->getMessage());
            }
        }
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected function getConnection() {
        return $this->_resourceConnection->getConnection();
    }

    public function generateShortDescriptionHtml($shortDescription)
    {
        $html = [];
        $html[] = '<div style="text-align: justify;">';
        $html[] = $shortDescription;
        $html[] = '</div>';
        return implode('', $html);
    }

    public function generateDescriptionHtml($longDescription)
    {
        $html = [];
        $html[] = '<p style="text-align: justify">';
        $html[] = $longDescription;
        $html[] = '<p>';
        return implode('', $html);
    }
   
   
   
    public function generateDescription1Html($data)
    {
      
        $html = [];
        $html[] = '<table class="table table-striped">';

        $i=0;
        $count=[];

        $html[] = '<tbody>';         
                foreach ($data as $key1 ) 
                {  
                    $html[] ="<tr>";
                    if(isset($key1['text']))
                    {                       
                            $html[] ='<td>'.$key1['text'].'</td>';  
                       
                    }

                    if(isset($key1['data']))
                    {                       
                            $html[] ='<td>'.$key1['data'].'</td>';  
                        
                    }

                    $html[] ="</tr>";

                }
               
        $html[] = '</tbody>';
        $html[] = '</table>';
        return implode('', $html);
    }

    public function generateDescription2Html($data)
    {
        $html = [];
        $html[] = '<ul>';
        foreach ($data as $item) {
            if (isset($item['text'])) {
                $html[] = '<li>';
                $html[] = $item['text'];
                $html[] = '</li>';
            }
        }
        $html[] = '</ul>';
        return implode('', $html);
    }

    public function generateDescription3Html($data)
    {
        $html = [];
        $html[] = '<ul>';
        foreach ($data as $text) {
                $html[] = '<li>';
                $html[] = $text['text'];
                $html[] = '</li>';
        }

        $html[] = '</ul>';
        return implode('', $html);
    }

    public function generateDescription4Html($data)
    {
        $html = [];
        $html[] = '<ul>';
        foreach ($data as $text) {
                $html[] = '<li>';
                $html[] = $text['text'];
                $html[] = '</li>';
        }

        $html[] = '</ul>';
        return implode('', $html);
    }

    public function generateDescription5Html($data)
    {
        $html = [];
        $html[] = '<table class="table table-striped">';
        $i=0;
        $count=[];
        $html[] = '<tbody>';         
                foreach ($data as $key1 ) 
                {  
                    $html[] ="<tr>";
                    if(isset($key1['text']))
                    {                       
                            $html[] ='<td>'.$key1['text'].'</td>';   
                    }                 
                    $html[] ="</tr>";
                }       
        $html[] = '</tbody>';
        $html[] = '</table>';
        return implode('', $html);
    }
    public function generateDescription51Html($data)
    {
        $html = [];

        if(count($data)){
            $html[] = '<table><tbody>';
            $boldArray = [];
            foreach ($data as $item) {
                if(isset($item['description_5'])){
                    if(count($item['description_5'])){
                        foreach ($item['description_5'] as $descriptionData) {

                            if($this->checkData($descriptionData, 'bold') && in_array($descriptionData['bold'], $boldArray)){
                                continue;
                            }

                            if($this->checkData($descriptionData, 'bold')){
                                array_push($boldArray, $descriptionData['bold']);
                            }
                            $html[] = '<tr>';
                            $html[] = '<td style="width: 110px; height: 110px;">';
                            if($this->checkData($descriptionData,'image')) {
                                if(trim($descriptionData['image']) && $imgSrc = $this->core->getWysiwygImage($descriptionData['image'])) {
                                    $html[] = '<img style="border-image: initial; border: 1px solid #808b96;" src="';
                                    $html[] = $imgSrc;
                                    $html[] = '">';
                                }
                            }
                            $html[] = '</td>';

                            $html[] = '<td style="text-align: justify">';
                            if($this->checkData($descriptionData    ,'bold')) {
                                $html[] = '<strong>';
                                $html[] = $descriptionData['bold'];
                                $html[] = '</strong>';
                            }


                            $text = $this->checkData($descriptionData, 'text') ? $descriptionData['text'] : '';
                            $newLinePos = strpos($text, "\n");
                            if($newLinePos){
                                $text = substr($text, $newLinePos +1);
                            }

                            $html[] = '<br />';
                            $html[] = $text;
                            $html[] = '</td>';

                            $html[] = '</tr>';
                        }
                    }
                }
            }
            $html[] = '</tbody></table>';
        }

        return implode('', $html);
    }


    public function generateDescription6Html($data)
    {
        $html = [];
        $html[] = '<table><tbody>';

        foreach ($data as $item) {
            $html[] = '<tr>';

            $html[] = '<td style="width: 110px; height: 110px;">';
            if($this->checkData($item,'image')) {
                if(trim($item['image']) && $imgSrc = $this->core->getWysiwygImage($item['image'])) {
                    $html[] = '<img style="border-image: initial; border: 1px solid #808b96;" src="';
                    $html[] = $imgSrc;
                    $html[] = '">';
                }
            }
            $html[] = '</td>';

            $html[] = '<td style="text-align: justify">';
            if($this->checkData($item,'bold')) {
                $html[] = '<strong>';
                $html[] = $item['bold'];
                $html[] = '</strong>';
            }


            $text = $item['text'];
            $newLinePos = strpos($text, "\n");
            if($newLinePos){
                $text = substr($text, $newLinePos +1);
            }

            $html[] = '<br />';
            $html[] = $text;
            $html[] = '</td>';

            $html[] = '</tr>';
        }

        $html[] = '</tbody></table>';
        return implode('', $html);
    }


    /**
     * @param $cronJobsModel
     * @return array|mixed
     */

    public function getFileData($cronJobsModel)
    {
        $filePath = $cronJobsModel->getFile();
        $fileName = $cronJobsModel->getFileName();

        /** var directory */
        $varDirectory = $this->directoryList->getPath('var');
        $fileFullPath = $varDirectory . DIRECTORY_SEPARATOR . $filePath;

        /** check if file exist */
        if (file_exists($fileFullPath)) {
            $jsonString = file_get_contents($fileFullPath);
            $importData = json_decode($jsonString, true);
            return $importData;
        } else {
            $this->core->insertLog($cronJobsModel, __('File is not exist'));
            return [];
        }
    }

    public function isValidImportData($importData, $cronJobsModel, $isLog = false)
    {

        $hasError = false;
        if (!is_array($importData)) {
            $hasError = true;
            $this->core->addMessage(__('Json file is not valid'));
            if ($isLog) {
                $this->core->insertLog($cronJobsModel, __('Json file is not valid'));
            }
        }
        if (!count($importData)) {
            $hasError = true;
            $this->core->addMessage(__('Data is missing in the file'));
            if ($isLog) {
                $this->core->insertLog($cronJobsModel, __('Data is missing in the file'));
            }
        }

        if (!isset($importData['links'])) {
            $hasError = true;
            $this->core->addMessage(__('Links are missing in the File'));
            if ($isLog) {
                $this->core->insertLog($cronJobsModel, __('Links are missing in the File'));
            }
        }

        $linkRow = 1;
        foreach ($importData as $links) {
            if (!is_array($links) || !count($links)) {
                $hasError = true;
                $this->core->addMessage(__('Link row #' . $linkRow . ' is not valid'));
                if ($isLog) {
                    $this->core->insertLog($cronJobsModel, __('Link row #' . $linkRow . ' is not valid'));
                }

            } else {
                foreach ($links as $link) {

                    if (!is_array($link) || !count($link) || !isset($link['link']) || !isset($link['links'])) {
                        $hasError = true;
                        $this->core->addMessage(__('Data in link row #' . $linkRow . ' is not valid'));
                        if ($isLog) {
                            $this->core->insertLog($cronJobsModel, __('Data for the link row #' . $linkRow . ' is not valid'));
                        }
                    } else {
                        $linkName = $link['link'];
                        foreach ($link['links'] as $productData) {
                            if (!is_array($productData) || !count($productData)) {

                                $hasError = true;
                                $this->core->addMessage(__('Data is not valid for the link ' . $linkName));
                                if ($isLog) {
                                    $this->core->insertLog($cronJobsModel, __('Data is not valid for the link ' . $linkName));
                                }
                            } else {

                                if (!isset($productData['product_title']) || !trim($productData['product_title'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('product_title  is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('product_title is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['product_ref']) || !trim($productData['product_ref'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('product_ref is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('product_ref is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['short_description']) || !trim($productData['short_description'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('Short Description is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('Short Description is missing for the link ' . $linkName));
                                    }
                                }


                                if (!isset($productData['big_image']) || !trim($productData['big_image'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('big_image is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('big_image is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['big_image_url']) || !trim($productData['big_image_url'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('big_image_url is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('big_image_url is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['long_description']) || !trim($productData['long_description'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('long_description is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('long_description is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['notice_url']) || !trim($productData['notice_url'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('notice_url is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('notice_url is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['notice']) || !trim($productData['notice'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('notice is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('notice is missing for the link ' . $linkName));
                                    }
                                }

                                if(!isset($productData['description_1_title'])){
                                    $hasError = true;
                                    $this->core->addMessage(__('description_1_title is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_1_title is missing for the link ' . $linkName));
                                    }
                                }

                                if(!isset($productData['description_2_title'])){
                                    $hasError = true;
                                    $this->core->addMessage(__('description_2_title is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_2_title is missing for the link ' . $linkName));
                                    }
                                }

                                if(!isset($productData['description_3_title'])){
                                    $hasError = true;
                                    $this->core->addMessage(__('description_3_title is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_3_title is missing for the link ' . $linkName));
                                    }
                                }

                                if(!isset($productData['description_4_title'])){
                                    $hasError = true;
                                    $this->core->addMessage(__('description_4_title is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_4_title is missing for the link ' . $linkName));
                                    }
                                }

                                if(!isset($productData['description_5_title'])){
                                    $hasError = true;
                                    $this->core->addMessage(__('description_5_title is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_5_title is missing for the link ' . $linkName));
                                    }
                                }

                                if (!isset($productData['description_1']) || !count($productData['description_1'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_1 is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_1 is missing for the link ' . $linkName));
                                    }
                                } else {
                                    $rowId = 1;
                                    foreach ($productData['description_1'] as $description) {

                                        if (!is_array($description) || !count($description)) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_1 row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_1 row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        } else {
                                            if (!isset($description['text']) || !trim($description['text'])) {
                                                $hasError = true;
                                                $this->core->addMessage(__('description_1 row ' . $rowId . ' "text" is missing for the link ' . $linkName));
                                                if ($isLog) {
                                                    $this->core->insertLog($cronJobsModel, __('description_1 row ' . $rowId . '  "text" is missing for the link ' . $linkName));
                                                }
                                            }

                                            if (!isset($description['data']) || !trim($description['data'])) {
                                                $hasError = true;
                                                $this->core->addMessage(__('description_1 row ' . $rowId . ' "data" is missing for the link ' . $linkName));
                                                if ($isLog) {
                                                    $this->core->insertLog($cronJobsModel, __('description_1 row ' . $rowId . ' "data" is missing for the link ' . $linkName));
                                                }
                                            }
                                        }
                                        $rowId++;
                                    }
                                }

                                if (!isset($productData['description_2']) || !count($productData['description_2'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_2 is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_2 is missing for the link ' . $linkName));
                                    }
                                } else {
                                    $rowId = 1;
                                    foreach ($productData['description_2'] as $description) {
                                        if (!isset($description['data']) || !trim($description['data'])) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_2_image data row' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_2 data row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        }
                                        $rowId++;
                                    }
                                }


                                if (!isset($productData['description_2_image']) || !count($productData['description_2_image'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_2_image is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_2_image is missing for the link ' . $linkName));
                                    }
                                } else {
                                    $rowId = 1;
                                    foreach ($productData['description_2_image'] as $description) {

                                        if (!isset($description['image']) || !trim($description['image'])) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_2_image image row' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_2 image row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        }

                                        if (!isset($description['mouse_over']) || !trim($description['mouse_over'])) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_2_image mouse_over row' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_2 mouse_over row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        }
                                        $rowId++;
                                    }

                                }



                                if (!isset($productData['description_3']) || !count($productData['description_3'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_3 is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_3 is missing for the link ' . $linkName));
                                    }
                                } else {

                                    $rowId = 1;
                                    foreach ($productData['description_3'] as $description) {

                                        if (!isset($description['text']) || !trim($description['text'])) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_3 text row' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_3 text row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        }

                                        $rowId++;

                                    }

                                }


                                if (!isset($productData['description_4']) || !count($productData['description_4'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_4 is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_4 is missing for the link ' . $linkName));
                                    }
                                } else {

                                    $rowId = 1;
                                    foreach ($productData['description_4'] as $description) {

                                        if (!isset($description['text']) || !trim($description['text'])) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_4 text row' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_4 text row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            }
                                        }

                                        $rowId++;

                                    }

                                }


                                if (!isset($productData['description_5']) || !count($productData['description_5'])) {
                                    $hasError = true;
                                    $this->core->addMessage(__('description_5 is missing for the link ' . $linkName));
                                    if ($isLog) {
                                        $this->core->insertLog($cronJobsModel, __('description_5 is missing for the link ' . $linkName));
                                    }
                                } else {
                                    $rowId = 1;
                                    foreach ($productData['description_5'] as $description) {

                                        if (!is_array($description) || !count($description)) {
                                            $hasError = true;
                                            $this->core->addMessage(__('description_5 row ' . $rowId . ' is invalid for the link ' . $linkName));
                                            if ($isLog) {
                                                $this->core->insertLog($cronJobsModel, __('description_5 is invalid for the link ' . $linkName));
                                            }
                                        } else {

                                            if (!isset($description['image']) || !trim($description['image'])) {
                                                $hasError = true;
                                                $this->core->addMessage(__('description_5 row ' . $rowId . ', "image" is missing for the link ' . $linkName));
                                                if ($isLog) {
                                                    $this->core->insertLog($cronJobsModel, __('description_5 row ' . $rowId . ', "image" is missing for the link ' . $linkName));
                                                }
                                            }

                                            if (!isset($description['text_bold']) || !trim($description['text_bold'])) {
                                                $hasError = true;
                                                $this->core->addMessage(__('description_5 row ' . $rowId . ', "text_bold" is missing for the link ' . $linkName));
                                                if ($isLog) {
                                                    $this->core->insertLog($cronJobsModel, __('description_5 row ' . $rowId . ', "text_bold" is missing for the link ' . $linkName));
                                                }
                                            }


                                            if (!isset($description['text']) || !trim($description['text'])) {
                                                $hasError = true;
                                                $this->core->addMessage(__('description_5  row ' . $rowId . ', "text" is missing for the link ' . $linkName));
                                                if ($isLog) {
                                                    $this->core->insertLog($cronJobsModel, __('description_5 row ' . $rowId . ', "text" is missing for the link ' . $linkName));
                                                }
                                            }

                                        }
                                        $rowId++;
                                    }
                                }
                            }

                        }
                    }

                }
            }
            $linkRow++;
        }

        return $hasError ? false : true;
    }


    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->core->getMessages();
    }

    /**
     * @param $data
     * @param $key
     * @return bool
     */
    public function checkData($data,$key)
    {
        return array_key_exists($key,$data);
    }

    /**
     * @param $urlKey
     * @return mixed
     */
    public function getCategoryMarques($urlKey) {
        $categoryFactory = $this->_categoryFactory->create();
        $category = $categoryFactory->loadByAttribute('url_key', $urlKey);
        return $category;
    }

    public function getManufacturerMarques($mapping = '') {
        $manufacturer = '';

        $attribute = $this->eavConfig->getAttribute('catalog_product', 'manufacturer');
        $options = $attribute->getSource()->getAllOptions();
        foreach ($options as $option) {
            $label = str_replace('ä', 'a', strtolower($option['label']));
            if($label === $mapping){
                $manufacturer = $option['value'];
            }
        }

        return $manufacturer;
    }
}