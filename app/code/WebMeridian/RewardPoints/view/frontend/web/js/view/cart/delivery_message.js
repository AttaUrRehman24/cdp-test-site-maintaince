/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, quote) {

        var rewardpoints = window.checkoutConfig.rewardpoints;
        if (rewardpoints.earnpoints.value) {
            var value = rewardpoints.earnpoints.value + ' ' + rewardpoints.earnpoints.unit;
        }

        return Component.extend({
            defaults: {
                template: 'WebMeridian_RewardPoints/cart/delivery_message'
            },
            totals: quote.getTotals(),
            isDisplayed: function () {
                return true;
            },
            getValue: function () {
                // Add code to refresh rewards data
                var value = '';
                if (window.checkoutConfig.delivery_message != 'undefined') {
                    value = window.checkoutConfig.delivery_message;
                }
                return value;
            }
        });
    }
);