<?php

namespace WebMeridian\ChangeAgreements\Plugin\Magento\CheckoutAgreements\Model;

class DisableAgreementsValidator
{
    public function afterIsValid(
        \Magento\CheckoutAgreements\Model\AgreementsValidator $subject, $result
    ){
        return true;
    }
}