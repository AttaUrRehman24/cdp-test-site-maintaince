<?php
namespace WebMeridian\ChangeEmailTemplates\Plugin\Rma\Model\Rma\CustomerNotification;

use \BoostMyShop\Rma\Model\Rma\CustomerNotification;
use \BoostMyShop\Rma\Model\Config;
use \WebMeridian\ChangeEmailTemplates\BoostMyShop\Rma\Model\Pdf\RmaFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Mail\Template\TransportBuilder;
use \WebMeridian\ChangeEmailTemplates\Helper\Data;
use WebMeridian\ChangeEmailTemplates\Model\PdfRendererFactory;

class AddAttachement extends CustomerNotification
{
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var RmaFactory
     */
    protected $rmaPdfFactory;
    /**
     * @var PdfRenderer
     */
    protected $orderPdfRenderer;

    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        RmaFactory $rmaPdfFactory,
        PdfRendererFactory $pdfRenderer,
        Data $helper)
    {
        $this->helper = $helper;
        $this->rmaPdfFactory = $rmaPdfFactory;
        $this->orderPdfRenderer = $pdfRenderer;
        parent::__construct($config, $storeManager, $transportBuilder);
    }

    public function aroundNotifyForStatus(
        CustomerNotification    $subject,
        callable $proceed,
        \BoostMyShop\Rma\Model\Rma $rma
    ){
        $mediaUrl = $this->helper->getMediaUrl();
        if ($rma->getRmaStatus() == '‌requested') {
            $subject->_transportBuilder->addAttachment(
                file_get_contents($mediaUrl . 'email/pdf/' . $this->helper::PDF_RETRACTION),
                $this->helper::PDF_RETRACTION);
        } elseif ($rma->getRmaStatus() == 'accepted') {

            $rma_pdf = $this->rmaPdfFactory->create()->getPdf([$rma]);

            $subject->_transportBuilder->addAttachment($rma_pdf->render(), 'retour.pdf');
        } elseif ($rma->getRmaStatus() == 'complete') {
            $order = $rma->getOrder();

            $order_pdf = $this->orderPdfRenderer->create()->getPdf([$order]);
            $subject->_transportBuilder->addAttachment($order_pdf->render(), __("Order"). '#' . $order->getId().'.pdf');
        }
        $proceed($rma);
    }
}