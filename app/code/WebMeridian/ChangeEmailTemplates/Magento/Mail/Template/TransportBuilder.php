<?php

namespace WebMeridian\ChangeEmailTemplates\Magento\Mail\Template;

class TransportBuilder
    extends \Magento\Framework\Mail\Template\TransportBuilder
{
    public function addAttachment(
        $pdfString, $filename
    ) {
        $this->message->createAttachment(
            $pdfString,
            'application/pdf',
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $filename
        );
        return $this;
    }

    public function getTemplateIdentifier()
    {
        return $this->templateIdentifier;
    }
}