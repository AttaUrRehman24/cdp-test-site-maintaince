<?php

namespace WebMeridian\ChangeEmailTemplates\Bobcares\Quote2Sales\Model;

use Magento\Framework\Mail\Template\TransportBuilder;
use WebMeridian\ChangeEmailTemplates\Helper\Data;

class Email extends \Bobcares\Quote2Sales\Model\Email
{
    protected $countryFactory;
    protected $_helper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigObject,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Pricing\Helper\Data $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Escaper $escaper,
        \Magento\Customer\Model\Customer $customerData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        Data $helper
    ){
        $this->countryFactory = $countryFactory;
        $this->_helper = $helper;
        parent::__construct($context, $registry, $scopeConfigObject, $localeDate, $priceCurrency, $storeManager, $transportBuilder, $quoteFactory, $messageManager, $escaper, $customerData, $scopeConfig, $inlineTranslation);
    }

    public function getCountryNameByCode($countryCode)
    {
        return $this->countryFactory->create()->loadByCode($countryCode)->getName();
    }

    public function sendEmail($quoteId, $sellerComment,$customerId) {

        $quote = $this->quoteFactory->create()->load($quoteId);
        $customerInstance = $this->customerData->load($customerId);

        $customer = $customerInstance->getName();
        $customerEmail = $customerInstance->getEmail();
        //$createdAt = $this->_localeDate->date(new \DateTime($quote->getCreatedAt()));
        $createdAt = $quote->getCreatedAt();
        $currency_code = $quote->getQuoteCurrencyCode();
        $currency_symbol = $this->storeManager->getStore()->getCurrentCurrency($currency_code)->getCurrencySymbol(); //toget the currency symbol from the Currency code.
        $subTotal = $quote->getSubtotal();
        $grandTotal = $quote->getBaseGrandTotal();
        $discount = $quote->getShippingAddress()->getBaseDiscountAmount();
        $subtotalDiscount = $quote->getSubtotalWithDiscount();
        $shippingTitle = $quote->getShippingAddress()->getShippingDescription();
        $shippingAmount = $quote->getShippingAddress()->getBaseShippingAmount();
        $websiteName = $quote->getStore()->getWebsite()->getName();
        $shippingAddress = $quote->getShippingAddress();
        $billingAddress = $quote->getBillingAddress();

        $shippingDetails = '<p>'. $shippingAddress->getName() . '<br/>' . $shippingAddress->getStreet()[0];
        if(isset($shippingAddress->getStreet()[1])) {
            $shippingDetails .= $shippingAddress->getStreet()[1];
        }
        $shippingCountry = $this->getCountryNameByCode($shippingAddress->getCountry());
        $shippingDetails .= $shippingAddress->getCity() . '<br/>' . $shippingAddress->getPostcode() . $shippingAddress->getRegion() . '<br/>' . $shippingCountry . '<br/>' . $shippingAddress->getTelephone() . '</p>';

        $billingCountry = $this->getCountryNameByCode($billingAddress->getCountry());
        $billingDetails = '<p>'. $billingAddress->getName() . '<br/>' . $billingAddress->getStreet()[0];
        if(isset($billingAddress->getStreet()[1])) {
            $billingDetails .= $billingAddress->getStreet()[1];
        }
        $billingDetails .= $billingAddress->getCity() . '<br/>' . $billingAddress->getPostcode() . $billingAddress->getRegion() . '<br/>' . $billingCountry . '<br/>' . $billingAddress->getTelephone() . '</p>';

        //Get all Quote Items
        $items = $quote->getAllItems();
        $quoteDetails = null;

        foreach ($items as $item) {

            $formatedPrice = $this->priceCurrency->currency($item['price'],true,false);
            $rowTotal = $this->priceCurrency->currency($item['row_total'],true,false);

            //Quote table rows as string
            $quoteDetails .= '<tr><td align="left" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;"><strong>' . $item['name'] . '</strong>'
                . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $item['sku']
                . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $formatedPrice
                . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $item['qty']
                . '</td><td align="right" width="325"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;">' . $rowTotal . '</td></tr>';
        }

        $formattedsubTotal = $this->priceCurrency->currency($subTotal,true,false);
        $formattedDiscount = $this->priceCurrency->currency($discount,true,false);
        $formattedShippingAmount = $this->priceCurrency->currency($shippingAmount,true,false);
        $formattedGrandTotal = $this->priceCurrency->currency($grandTotal,true,false);

        //Quote totals as a table in string
        $quote_footer = '<table style="border-collapse: collapse;margin-top: 30px;">';

        $quote_footer .= '<tr><td width="650"></td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-top: 1px solid;border-left: 1px solid;" >' . __('Sous-total') . ':</td><td style="border-top: 1px solid;">&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-top: 1px solid;border-right: 1px solid;" >' . $formattedsubTotal . '</td></tr>';

        if (!$quote->isVirtual()) { // if the product is not virtual
            $quote_footer .= '<tr><td width="650"></td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-left: 1px solid;" >' . __('TVA & autres taxes') . ':</td><td>&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-right: 1px solid;" >' . $formattedShippingAmount . '</td></tr>';
        }

        $quote_footer .= '<tr><td width="650"></td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-bottom: 1px solid;border-left: 1px solid;" ><strong>' . __('Montant global') . ':</strong></td><td style="border-bottom: 1px solid;">&nbsp;</td><td align="right" width="325" style="font-size:13px;padding:5px 9px 6px 9px; line-height:1em;border-bottom: 1px solid;border-right: 1px solid;" ><strong>' . $formattedGrandTotal . '</strong></td></tr>';

        $quote_footer .= '</table>';

        $footerImages = $this->_helper->getFooterImages();

        $logoTemplate = $this->_helper->getLogoParameters();

        //Parametes the template email variable to send
        $params = array(
            'customer' => $customer,
            'quoteid' => $quoteId,
            'createdAt' => $createdAt,
            'currency_code' => $currency_code,
            'currency_symbol' => $currency_symbol,
            'subTotal' => $subTotal,
            'subTotalDiscount' => $subtotalDiscount,
            'discount' => $discount,
            'shippingTitle' => $shippingTitle,
            'shippingAmount' => $shippingAmount,
            'grandTotal' => $grandTotal,
            'website' => $websiteName,
            'sellerComment' => $sellerComment,
            'quoteDetails' => $quoteDetails,
            'quote_footer'=>$quote_footer,
            'shippingAddress'=>$shippingDetails,
            'billingAddress'=>$billingDetails,
            'footer_images_first' => $footerImages['first'],
            'footer_images_second' => $footerImages['second'],
            'footer_images_third' => $footerImages['third'],
            'logoTemplate_img' => $logoTemplate['img'],
            'logoTemplate_height' => $logoTemplate['height'],
            'logoTemplate_width' => $logoTemplate['width'],
            'logoTemplate_alt' => $logoTemplate['alt'],
        );

        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($params);

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        try {
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier('quotes_email_email_quote')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['data' => $postObject])
                ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                ->addTo($customerEmail, $customer)
                ->getTransport();

            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Sorry, we can\'t send the email right now.' . $e->getMessage()));
        }
    }
}