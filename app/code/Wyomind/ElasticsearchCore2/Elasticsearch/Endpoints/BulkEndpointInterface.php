<?php

namespace Wyomind\ElasticsearchCore\Elasticsearch\Endpoints;

use Wyomind\ElasticsearchCore\Elasticsearch\Serializers\SerializerInterface;

/**
 * Interface BulkEndpointInterface
 *
 * @category Elasticsearch
 * @package Wyomind\ElasticsearchCore\Elasticsearch\Endpoints
 * @author   Zachary Tong <zach@elastic.co>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache2
 * @link     http://elastic.co
 */
interface BulkEndpointInterface
{
    /**
     * Constructor
     *
     * @param SerializerInterface $serializer A serializer
     */
    public function __construct(SerializerInterface $serializer);
}
