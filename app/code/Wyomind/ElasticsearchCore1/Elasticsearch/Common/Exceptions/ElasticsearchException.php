<?php

namespace Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions;

/**
 * Generic Exception interface
 *
 * @category Elasticsearch
 * @package Wyomind\ElasticsearchCore\Elasticsearch\Common\Exceptions
 * @author   Zachary Tong <zach@elastic.co>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache2
 * @link     http://elastic.co
 */
interface ElasticsearchException
{
}
