<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 13/05/2019
 * Time: 14:25
 */

namespace Wyomind\ElasticsearchCore\Helper;


class Synonyms
{

    /**
     * @var mixed|string
     */
    private $path = "";

    public function __construct()
    {
        $path = BP . '/var/wyomind/elasticsearch/synonyms/';
        $this->path = str_replace('/', DIRECTORY_SEPARATOR, $path);
    }

    public function buildSynonymsPhrases($q, $storeCode)
    {
        $terms = explode(' ', $q);

        $synonyms = $this->getSynonyms($storeCode, $terms);

        $phrases = [implode(" ", $terms)];

        foreach ($synonyms as $word => $wordSynonyms) {
            foreach ($phrases as $phrase) {
                foreach ($wordSynonyms as $synonym) {
                    if ($synonym !== $word) {
                        $phrases[] = preg_replace("/\b".$word."\b/i", $synonym, $phrase);
                    }
                }
            }
        }
        return array_unique($phrases);
    }

    public function getSynonyms($storeCode, $terms)
    {
        $filepath = $this->path . $storeCode . ".json";
        if (file_exists($filepath)) {
            $allSynonyms = json_decode(file_get_contents($filepath), true);
            $selectedSynonyms = [];
            foreach ($terms as $term) {
                if (isset($allSynonyms[$term])) {
                    $selectedSynonyms[$term] = $allSynonyms[$term];
                }
            }
            return $selectedSynonyms;
        } else {
            return [];
        }
    }

    public function generateSynonymsFiles($storeCode, $synonyms)
    {
        $filepath = $this->path;
        $filename = $storeCode . ".json";
        if (!is_dir($filepath)) {
            @mkdir($filepath, 0777, true);
        }
        file_put_contents($filepath . '/' . $filename, json_encode($synonyms));
    }

}