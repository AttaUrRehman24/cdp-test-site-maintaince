<?xml version="1.0"?>
<!--
  Copyright © 2018 Wyomind. All rights reserved.
  See LICENSE.txt for license details.
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <!-- master tab -->
        <tab id="wyomind" translate="label" sortOrder="1000000">
            <label><![CDATA[<span class='wyomind-logo'>Wyomind</span>]]></label>
        </tab>
        <!-- module tab -->
        <section id="wyomind_elasticsearchcore" translate="label" sortOrder="100" showInDefault="1" showInWebsite="0" showInStore="1">
            <class>separator-top</class>
            <label><![CDATA[Elasticsearch Core]]></label>
            <tab>wyomind</tab>
            <resource>Wyomind_ElasticsearchCore::global</resource>
            <group id="license" translate="label" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>License</label>
                <field id="extension_version" translate="label" type="label" sortOrder="1" showInDefault="1"
                       showInWebsite="0" showInStore="0">
                    <label>Extension version</label>
                </field>
                <field id="activation_key" translate="label comment" type="label" sortOrder="110" showInDefault="1"
                       showInWebsite="0" showInStore="0">
                    <label>Activation key</label>
                    <frontend_model>Wyomind\Core\Block\Adminhtml\System\Config\Form\Field\ActivationKey</frontend_model>
                </field>
                <field id="license_status" translate="label comment" type="label" sortOrder="110" showInDefault="1"
                       showInWebsite="0" showInStore="0">
                    <label>License Status</label>
                    <frontend_model>Wyomind\Core\Block\Adminhtml\System\Config\Form\Field\LicenseStatus</frontend_model>
                </field>
            </group>
            <group id="configuration" translate="label" showInDefault="1" showInWebsite="0" showInStore="1" sortOrder="10">
                <label>Settings</label>
                <field id="server_version" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Elasticsearch Server version</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\Version</frontend_model>
                    <comment><![CDATA[Save the configuration to refresh this value]]></comment>
                </field>
                <field id="servers" translate="label comment" type="textarea" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Servers</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\Servers</frontend_model>
                    <comment><![CDATA[host:port separated by comma. Follow this pattern for full available parameters: <a href="http://php.net/manual/en/function.parse-url.php" target="_blank">http://php.net/manual/en/function.parse-url.php</a>]]></comment>
                    <validate>required-entry</validate>
                </field>
                <field id="verify_host" translate="label comment" type="select" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Verify Host</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[Only used for https connection. Select No here if you don't have a valid SSL certificate.]]></comment>
                </field>
                <field id="timeout" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Connect Timeout</label>
                    <comment><![CDATA[Connection timeout in seconds.]]></comment>
                    <validate>required-entry validate-greater-than-zero validate-number</validate>
                </field>
                <field id="index_prefix" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Index Prefix</label>
                    <comment><![CDATA[Used to prefix index names to avoid potential collisions.]]></comment>
                    <validate>validate-code</validate>
                </field>
                <field id="index_settings" translate="label comment" type="textarea" sortOrder="70" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Index Settings</label>
                    <comment><![CDATA[Having more shards enhances the indexing performance and allows to distribute a big index across machines.<br>The number of replicas each shard has. Having more replicas enhances the search performance and improves the cluster availability.<br /><strong><span style="color: red;">Be careful</span></strong> when modifying this parameter. Write consistency (one, quorum or all) must be considered in order to avoid timeout write action. More info here: <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html#bulk-consistency" target="_blank">https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html#bulk-consistency</a> and here <a href="https://github.com/elasticsearch/elasticsearch/issues/444" target="_blank">https://github.com/elasticsearch/elasticsearch/issues/444</a>]]></comment>
                    <tooltip><![CDATA[<strong>Default write consistency is quorum</strong> (active shards > replicas / 2 + 1).<br />For example, in a N shards with 2 replicas index, there will have to be at least 2 active shards within the relevant partition (quorum) for the operation to succeed. In a N shards with 1 replica scenario, there will need to be a single shard active (in this case, one and quorum is the same).]]></tooltip>
                </field>
                <field id="safe_reindex" translate="label comment" type="select" sortOrder="80" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Safe Reindex</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[Reindex in a temporary index and switch to it once finished. Especially useful for large product catalogs.]]></comment>
                </field>
                <field id="query_operator" translate="label comment" type="select" sortOrder="90" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Query Operator</label>
                    <source_model>Wyomind\ElasticsearchCore\Model\System\Config\Source\Query\Operator</source_model>
                    <comment><![CDATA[For example, with the OR operator, the query "digital camera" is translated to "digital OR camera", with the AND operator, the same query is translated to "digital AND camera". The default value is AND.]]></comment>
                </field>
                <field id="enable_fuzzy_query" translate="label comment" type="select" sortOrder="100" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Enable Fuzzy Query</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[Enables approximative search.]]></comment>
                </field>
                <field id="fuzzy_query_mode" translate="label comment" type="select" sortOrder="110" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Fuzzy Query Mode</label>
                    <source_model>Wyomind\ElasticsearchCore\Model\System\Config\Source\Fuzzyness\Mode</source_model>
                    <comment><![CDATA[- <b>0</b>, <b>1</b>, <b>2</b>: the maximum allowed Levenshtein Edit Distance (or number of edits)<br/>
- <b>AUTO</b>: generates an edit distance based on the length of the term. For lengths:<br/>
&nbsp;&nbsp;- 0..2: must match exactly<br/>
&nbsp;&nbsp;- 3..5: one edit allowed<br/>
&nbsp;&nbsp;- &gt;5: two edits allowed</br/>
<br/>
<b>AUTO</b> should generally be the preferred value<br/><br/>
More information: <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#fuzziness">https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#fuzziness</a>]]></comment>
                    <depends>
                        <field id="enable_fuzzy_query">1</field>
                    </depends>
                </field>
                <field id="enable_product_weight" translate="label comment" type="select" sortOrder="120" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Enable Product Weight</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[Enables product weight modifier (only available for search results).]]></comment>
                </field>
                <field id="enable_pub_folder" translate="label comment" type="select" sortOrder="130" showInDefault="1" showInWebsite="0" showInStore="1">
                    <label>Add the 'pub' folder to the images url</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[The Wyomind ElasticsearchCore index must be run after changing this option]]></comment>
                </field>
            </group>
            <group id="debug" translate="label" showInDefault="1" showInWebsite="0" showInStore="0" sortOrder="20">
                <label>Log / Debug</label>
                <field id="enable_frontend_request_log" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Enable request log</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Creates a log file for the frontend requests.<br/>Filename var/log/Wyomind_ElasticsearchCore_Frontend_Requests.log]]></comment>
                </field>
                <field id="enable_reindex_log" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Enable indexation log</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Creates a log file for the reindexation process.<br/>Filename var/log/Wyomind_ElasticsearchCore_Indexation.log]]></comment>
                </field>
                <field id="enable_elasticsearch_sever_status_log" translate="label comment" type="select" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Enable Elasticsearch Server status log</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Creates a log file for the Elasticsearch Server status.<br/>Filename var/log/Wyomind_ElasticsearchCore_Server_Status.log]]></comment>
                </field>
                <field id="backend_notification_on_elasticsearch_sever_fail" translate="label comment" type="select" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Receive a backend notification when the server fails</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <depends>
                        <field id="enable_elasticsearch_sever_status_log">1</field>
                    </depends>
                </field>
                <group id="backend_notification_on_elasticsearch_sever_fail_settings" translate="label" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Backend notification settings</label>
                    <field id="subject" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Notification subject</label>
                    </field>
                    <field id="content" translate="label comment" type="textarea" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Notification content</label>
                        <comment><![CDATA[List of variables that can be used to build the content of the message:<br/>
                        - {{store_id}} : store ID<br/>
                        - {{code}} : Store code<br/>
                        - {{name}} : Store name<br/>
                        - {{server_version}} : Elasticsearch Server version<br/>
                        - {{servers}} : Servers<br/>
]]></comment>
                    </field>
                    <depends>
                        <field id="enable_elasticsearch_sever_status_log">1</field>
                        <field id="backend_notification_on_elasticsearch_sever_fail">1</field>
                    </depends>
                </group>
                <field id="mail_notification_on_elasticsearch_sever_fail" translate="label comment" type="select" sortOrder="60" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Receive a mail notification when the server fails</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <depends>
                        <field id="enable_elasticsearch_sever_status_log">1</field>
                    </depends>
                </field>
                <group id="mail_notification_on_elasticsearch_sever_fail_settings" translate="label" sortOrder="70" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Mail notification settings</label>
                    <field id="sender_email" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Sender Email</label>
                        <validate>validate-email</validate>
                        <backend_model>Magento\Config\Model\Config\Backend\Email\Address</backend_model>
                    </field>
                    <field id="sender_name" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Sender Name</label>
                        <backend_model>Magento\Config\Model\Config\Backend\Email\Sender</backend_model>
                        <validate>validate-emailSender</validate>
                    </field>
                    <field id="emails" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Send the notification to</label>
                        <comment>Emails must be separated with a comma (,).</comment>
                    </field>
                    <field id="subject" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Email subject</label>
                    </field>
                    <field id="content" translate="label comment" type="textarea" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                        <label>Email content</label>
                        <comment><![CDATA[List of variables that can be used to build the content of the message:<br/>
                        - {{store_id}} : Store ID<br/>
                        - {{message}} : Returned message<br/>
                        - {{server_status}} : Elasticsearch Server status<br/>
                        - {{server_version}} : Elasticsearch Server version
]]></comment>
                    </field>
                    <depends>
                        <field id="enable_elasticsearch_sever_status_log">1</field>
                        <field id="mail_notification_on_elasticsearch_sever_fail">1</field>
                    </depends>
                </group>
            </group>
            <group id="design" translate="label" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="9999">
                <label>Frontend design</label>
                <field id="primary_color" translate="label comment" type="text" sortOrder="100" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Primary color</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\ColorPicker</frontend_model>
                    <comment><![CDATA[Mainly used for the main elements.<br/>
                    Default color: <span class="color-preview _0879A1">#0879A1</span>]]></comment>
                    <tooltip><![CDATA[<div class="system-config design-colors-primary"></div>]]></tooltip>
                </field>
                <field id="secondary_color" translate="label comment" type="text" sortOrder="200" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Secondary color</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\ColorPicker</frontend_model>
                    <comment><![CDATA[Mainly used for highlighting search terms in the search results.<br/>
                            Default color: <span class="color-preview _FAFFBA">#FAFFBA</span>]]></comment>
                    <tooltip><![CDATA[<div class="system-config design-colors-secondary"></div>]]></tooltip>
                </field>
                <field id="background_primary_color" translate="label comment" type="text" sortOrder="300" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Background primary color</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\ColorPicker</frontend_model>
                    <comment><![CDATA[Mainly used as the products listing background.<br/>
                            Default color: <span class="color-preview _FFFFFF">#FFFFFF</span>]]></comment>
                    <tooltip><![CDATA[<div class="system-config design-colors-bg-primary"></div>]]></tooltip>
                </field>
                <field id="background_secondary_color" translate="label comment" type="text" sortOrder="400" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Background secondary color</label>
                    <frontend_model>Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\ColorPicker</frontend_model>
                    <comment><![CDATA[Mainly used as the secondary entities listing background and layers background<br/>
                            Default color: <span class="color-preview _F9F9F9">#F9F9F9</span>]]></comment>
                    <tooltip><![CDATA[<div class="system-config design-colors-bg-secondary"></div>]]></tooltip>
                </field>

                <field id="overlay_enable" translate="label comment" type="select" sortOrder="500" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enable overlay</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Mainly used to enable a dark overlay behind the autocomplete boxes.]]></comment>
                </field>
                <field id="blur_enable" translate="label comment" type="select" sortOrder="550" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enable blur effect</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Mainly used to blur the dark overlay behind the autocomplete boxes.]]></comment>
                </field>
                <field id="transition_enable" translate="label comment" type="select" sortOrder="600" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enable transition</label>
                    <source_model>Magento\Config\Model\Config\Source\YesNo</source_model>
                    <comment><![CDATA[Mainly used to smoothly show/hide the autocomplete boxes.]]></comment>
                </field>
                <field id="transition_duration" translate="label comment" type="text" sortOrder="700" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Transition duration</label>
                    <comment><![CDATA[Transition duration in seconds<br/>
                            <i>eg: 0.2 ( = 200 ms)</i>]]></comment>
                    <depends>
                        <field id="transition_enable">1</field>
                    </depends>
                </field>

            </group>

        </section>
    </system>
</config>