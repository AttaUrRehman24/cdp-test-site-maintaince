/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * https://www.wyomind.com
 */
var config = {
    map: {
        '*': {
            'wyoea': 'Wyomind_ElasticsearchAutocomplete/js/form-mini'
        }
    }
}; 
