<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Flags
 */


namespace Amasty\Flags\Plugin\Ui\Model;

use Amasty\Flags\Model\Column;
use Amasty\Flags\Model\Flag;
use Amasty\Flags\Model\ResourceModel\Column\CollectionFactory as ColumnCollectionFactory;
use Amasty\Flags\Model\ResourceModel\Flag\CollectionFactory as FlagCollectionFactory;
use Magento\Backend\Model\UrlInterface;

class AbstractReader
{
    /**
     * @var ColumnCollectionFactory
     */
    protected $columnCollectionFactory;

    /**
     * @var FlagCollectionFactory
     */
    protected $flagCollectionFactory;

    /**
     * @var UrlInterface
     */
    protected $url;

    public function __construct(
        ColumnCollectionFactory $columnCollectionFactory,
        FlagCollectionFactory $flagCollectionFactory,
        UrlInterface $url
    ) {
        $this->columnCollectionFactory = $columnCollectionFactory;
        $this->flagCollectionFactory = $flagCollectionFactory;
        $this->url = $url;
    }

    protected function addAssignMenu(Column $column, array $applicableFlags)
    {
        $actions = [];

        /** @var Flag $flag */
        foreach ($applicableFlags as $index => $flag) {
            $actionUrl = $this->url->getUrl(
                'amasty_flags/flagAssign/massAssign',
                [
                    'column' => $column->getId(),
                    'flag' => $flag->getId(),
                ]
            );

            $actions[] = $this->addAction(
                "amflags_assign_{$column->getId()}_{$flag->getId()}",
                __($flag->getName()),
                $actionUrl,
                __('Assign the "%1" flag to the selected orders?', $flag->getName())
            );
        }

        $result = $this->addMenuItem(
            'amflags_assign_' . $column->getId(),
            __('Assign Flags To "%1" Column', $column->getName()),
            $actions
        );

        return $result;
    }

    protected function addUnassignMenu($columns)
    {
        $actions = [];

        $actions[] = $this->addAction(
            'amflags_unassign_all',
            __('All'),
            $this->url->getUrl('amasty_flags/flagAssign/massUnassign'),
            __('Unassign all the flags for the selected orders?')
        );

        /** @var Column $column */
        foreach ($columns as $column) {
            $actionUrl = $this->url->getUrl(
                'amasty_flags/flagAssign/massUnassign',
                ['column' => $column->getId()]
            );

            $actions[] = $this->addAction(
                "amflags_unassign_{$column->getId()}",
                __('For Column "%1"', $column->getName()),
                $actionUrl,
                __('Unassign all the flags from "%1" column for the selected orders?', $column->getName())
            );
        }

        $result = $this->addMenuItem(
            'amflags_unassign',
            __('Unassign Flags'),
            $actions
        );

        return $result;
    }

    protected function addMenuItem($code, $title, $actions)
    {
        $result = [
            'arguments'  => [
                'data'    => [
                    'name'     => 'data',
                    'xsi:type' => 'array',
                    'item'     => [
                        'config' => [
                            'name'     => 'config',
                            'xsi:type' => 'array',
                            'item'     => [
                                'type'      => [
                                    'name'     => 'type',
                                    'xsi:type' => 'string',
                                    'value'    => $code
                                ],
                                'label'     => [
                                    'name'      => 'label',
                                    'xsi:type'  => 'string',
                                    'translate' => 'true',
                                    'value'     => (string)$title
                                ],
                            ]
                        ]
                    ]
                ],
                'actions' => [
                    'name'     => 'actions',
                    'xsi:type' => 'array',
                    'item'     => $actions
                ]
            ],
            'attributes' => [
                'class' => 'Magento\Ui\Component\Action',
                'name'  => $code
            ],
            'children'   => []
        ];

        return $result;
    }

    protected function addAction($code, $title, $action, $confirmation)
    {
        $result = [
            'name'     => $code,
            'xsi:type' => 'array',
            'item'     => [
                'label'   => [
                    'name'      => 'label',
                    'xsi:type'  => 'string',
                    'translate' => 'true',
                    'value'     => (string)$title
                ],
                'url'     => [
                    'name'     => 'url',
                    'xsi:type' => 'url',
                    'path'     => $action
                ],
                'type'    => [
                    'name'     => 'type',
                    'xsi:type' => 'string',
                    'value'    => $code
                ],
                'confirm' => [
                    'name'     => 'confirm',
                    'xsi:type' => 'array',
                    'item'     => [
                        'title'   => [
                            'name'      => 'title',
                            'xsi:type'  => 'string',
                            'translate' => 'true',
                            'value'     => (string)__('Please Confirm')
                        ],
                        'message' => [
                            'name'      => 'message',
                            'xsi:type'  => 'string',
                            'translate' => 'true',
                            'value'     => (string)$confirmation
                        ]
                    ]
                ],
            ]
        ];

        return $result;
    }
}
