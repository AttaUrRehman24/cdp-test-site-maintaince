<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Flags
 */

namespace Amasty\Flags\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Column extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('amasty_flags_column', 'id');
    }

    public function assignFlags($object, array $ids, $deletePrevious = false)
    {
        $table = $this->getTable('amasty_flags_flag_column');

        if ($deletePrevious) {
            $this->getConnection()->delete($table, 'column_id = ' . ((int)$object->getId()));
        }

        foreach ($ids as $id) {
            $this->getConnection()->insertOnDuplicate(
                $table,
                ['column_id' => $object->getId(), 'flag_id' => $id]
            );
        }
    }

    public function getAppliedFlagIds($object)
    {
        $select = $this->getConnection()->select()
            ->from(
                $this->getTable('amasty_flags_flag_column'),
                'flag_id'
            )
            ->where('column_id = ?', $object->getId())
        ;

        return $this->getConnection()->fetchCol($select);
    }
}
