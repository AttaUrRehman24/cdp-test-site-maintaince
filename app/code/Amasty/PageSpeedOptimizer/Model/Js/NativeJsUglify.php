<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Model\Js;

/**
 * Class NativeJsUglify native js inline script
 *
 * @package Amasty\PageSpeedOptimizer
 */
class NativeJsUglify
{
    const SCRIPT = '!function(){var e=window.addEventListener||function(e,t){window.attachEvent("on"+e,t)},t=window.removeEventListener||function(e,t,n){window.detachEvent("on"+e,t)},n={cache:[],mobileScreenSize:500,addObservers:function(){window.addEventListener("load",()=>{require(["jquery","domReady!"],e=>{e(".ms-level0").on("mouseenter",n.loadVisibleImages),e(".ms-level0 .col-category .form-group i.information").parent().on("mouseenter",()=>{e(this).hasClass("am-lazzy")||e(".ms-level0 img[data-amsrc]").each((e,t)=>{t.onload=(()=>{t.className=t.className.replace(/(^|\s+)lazy-load(\s+|$)/,"$1lazy-loaded$2")});const o=t.getAttribute("data-src-mobile"),i=t.getAttribute("data-amsrc");t.src=screen.width<=n.mobileScreenSize?o:i})})})}),e("scroll",n.throttledLoad),e("resize",n.throttledLoad)},removeObservers:function(){t("scroll",n.throttledLoad,!1),t("resize",n.throttledLoad,!1)},throttleTimer:(new Date).getTime(),throttledLoad:function(){var e=(new Date).getTime();e-n.throttleTimer>=200&&(n.throttleTimer=e,n.loadVisibleImages())},loadVisibleImages:function(){for(var e=window.pageYOffset||document.documentElement.scrollTop,t=e-200,i=e+(window.innerHeight||document.documentElement.clientHeight)+200,r=0;r<n.cache.length;){var a=n.cache[r],l=o(a);if(l>=t-(a.height||0)&&l<=i){var c=a.getAttribute("data-src-mobile");a.onload=function(){this.className=this.className.replace(/(^|\s+)lazy-load(\s+|$)/,"$1lazy-loaded$2")},c&&screen.width<=n.mobileScreenSize?a.src=c:a.src=a.getAttribute("data-amsrc"),a.removeAttribute("data-amsrc"),a.removeAttribute("data-src-mobile"),n.cache.splice(r,1)}else r++}0===n.cache.length&&n.removeObservers()},init:function(){document.querySelectorAll||(document.querySelectorAll=function(e){var t=document,n=t.documentElement.firstChild,o=t.createElement("STYLE");return n.appendChild(o),t.__qsaels=[],o.styleSheet.cssText=e+"{x:expression(document.__qsaels.push(this))}",window.scrollBy(0,0),t.__qsaels});for(var e=document.querySelectorAll("img[data-amsrc]"),t=0;t<e.length;t++){var o=e[t];n.cache.push(o)}n.addObservers(),n.loadVisibleImages()}};function o(e){var t=0;if(e.offsetParent){do{t+=e.offsetTop}while(e=e.offsetParent);return t}}n.init()}(),window.onload=function(){document.querySelectorAll(\'li[role="presentation"] button\').forEach(function(e){e.addEventListener("click",function(){window.dispatchEvent(new Event("resize"))})}),document.querySelectorAll(\'div[class="slide-holder slick-slide"] div\').forEach(function(e){e.addEventListener("click",function(){window.dispatchEvent(new Event("resize"))})}),document.querySelectorAll(".partner-holder div button").forEach(function(e){e.addEventListener("click",function(){window.setTimeout(function(){window.dispatchEvent(new Event("resize"))},500)})})};'
    ;
}
