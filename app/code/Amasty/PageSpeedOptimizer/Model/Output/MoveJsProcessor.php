<?php

namespace Amasty\PageSpeedOptimizer\Model\Output;

/**
 * Class MoveJsProcessor
 *
 * @package Amasty\PageSpeedOptimizer
 */
class MoveJsProcessor implements OutputProcessorInterface
{
    /**
     * @var \Amasty\PageSpeedOptimizer\Model\ConfigProvider
     */
    private $configProvider;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Magento\Framework\Code\Minifier\Adapter\Js\JShrink
     */
    private $JShrink;

    public function __construct(
        \Amasty\PageSpeedOptimizer\Model\ConfigProvider $configProvider,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Code\Minifier\Adapter\Js\JShrink $JShrink
    ) {
        $this->configProvider = $configProvider;
        $this->registry = $registry;
        $this->JShrink = $JShrink;
    }

    /**
     * @inheritdoc
     */
    public function process(&$output)
    {
        if ($this->configProvider->isMoveJS()) {
            $requireJs = $this->registry->registry('requireJsScript');
            $scriptIgnoreList = $this->configProvider->getMoveJsIgnoreList();
            if ($requireJs) {
                $output .= '%require_js_script% %other_scripts%';

                $scripts = [];
                if (!empty($scriptIgnoreList)) {
                    $output = preg_replace_callback(
                        '/<script.*?>.*?<\/script.*?>/is',
                        function ($script) use (&$scripts, &$scriptIgnoreList) {
                            $ret = '';

                            foreach($scriptIgnoreList as $ignoreItem) {
                                if (strpos($script[0], $ignoreItem) !== false) {
                                    $ret = $script[0];
				                    break;
                                }
                            }

			                if ($ret == '') {
			                    $scripts[] = $script[0];
			                }

			                return $ret;
                        },
                        $output
                    );
                } else {
                    $output = preg_replace_callback(
                        '/<script.*?>.*?<\/script.*?>/is',
                        function ($script) use (&$scripts) {
                            $scripts[] .= $script[0];
                            return ''; 
                        },
                        $output
                    );
                }

                $scriptsOutput = '';
                foreach ($scripts as $script) {
                    try {
                        $scriptMin = $this->JShrink->minify($script);
                        if (strpos($scriptMin, '<script') === false
                            || strpos($scriptMin, '</script') === false
                        ) {
                            $scriptsOutput .= $script;
                        } else {
                            $scriptsOutput .= $scriptMin;
                        }
                    } catch (\Exception $e) {
                        $scriptsOutput .= $script;
                    }
                }

                $output = str_replace(
                    '%require_js_script% %other_scripts%',
                    '%lazy_before%' . $requireJs . '%lazy_after%' . $scriptsOutput,
                    $output
                );
            }
        }
    }
}

