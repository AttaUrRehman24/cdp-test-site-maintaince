<?php
    use \Magento\Framework\Component\ComponentRegistrar;

    ComponentRegistrar::register(ComponentRegistrar::MODULE, 'BoostMyShop_DropShip', __DIR__);