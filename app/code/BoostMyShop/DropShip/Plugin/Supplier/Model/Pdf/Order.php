<?php

namespace BoostMyShop\DropShip\Plugin\Supplier\Model\Pdf;

class Order
{

    protected $_orderFactory;
    protected $_addressRenderer;
    protected $_config;

    public function __construct(
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \BoostMyShop\DropShip\Model\Config $config
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_addressRenderer = $addressRenderer;
        $this->_config= $config;
    }

    public function aroundGetShippingAddress(\BoostMyShop\Supplier\Model\Pdf\Order $subject, $proceed, $order)
    {
        if (!$order->getpo_dropship_order_id())
            return $proceed($order);

        $salesOrder = $this->_orderFactory->create()->load($order->getpo_dropship_order_id());
        $shippingAddress = $this->_addressRenderer->format($salesOrder->getShippingAddress(), 'html');
        $shippingAddress = str_replace("\r", "", $shippingAddress);
        $shippingAddress = str_replace("<br />", "", $shippingAddress);
        $shippingAddress = str_replace("<br/>", "", $shippingAddress);
        return $shippingAddress;

    }

    public function aroundInsertAdditionnal(\BoostMyShop\Supplier\Model\Pdf\Order $subject, $proceed, $page, $order)
    {
        if (!$order->getpo_dropship_order_id())
            return $proceed($page, $order);

        //insert a new page with the packing slip
        $salesOrder = $this->_orderFactory->create()->load($order->getpo_dropship_order_id());
        $page = $subject->newPage();

        //add header
        $subject->insertLogo($page, $order->getStore());
        $subject->setFontBold($page, 14);
        $headerText = $this->_config->getSetting('packing_slip/header_text', $order->getpo_store_id());
        $headerText = explode("\n", $headerText);
        $i = 0;
        foreach($headerText as $line) {
            $line = str_replace("\r", "", $line);
            if ($line) {
                $page->drawText($line, 300, 805 - ($i * 13), 'UTF-8');
                $i++;
            }
        }
        $subject->y = 815 - ($i * 15 + 20);

        $subject->setFontBold($page, 14);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText(__('Packing slip'), 30, $subject->y - 20, 'UTF-8');
        $page->drawText(__('Shipping address'), 300, $subject->y - 20, 'UTF-8');

        $subject->setFontRegular($page, 12);
        $additionnalTxt = [];
        $additionnalTxt[] = __('Order # : %1', $salesOrder->getIncrementId());
        $additionnalTxt[] = __('Placed at : %1', $salesOrder->getCreatedAt());
        $i = 0;
        foreach($additionnalTxt as $txt)
        {
            $page->drawText($txt, 60, $subject->y - 40 - ($i * 13), 'UTF-8');
            $i++;
        }

        $shippingAddress = explode("\n", $subject->getShippingAddress($order));
        $subject->setFontRegular($page, 12);
        $i = 0;
        foreach($shippingAddress as $line) {
            $line = str_replace("\r", "", $line);
            if ($line) {
                $page->drawText($line, 300, $subject->y - 40 - ($i * 13), 'UTF-8');
                $i++;
            }
        }
        $subject->y -= ($i * 15 + 60);

        //add products
        $this->drawProductsHeader($subject, $page, $order);
        foreach($order->getAllItems() as $item)
        {
            $this->_drawItem($subject, $item, $page, $order);
        }

    }

    public function drawProductsHeader($subject, $page, $order)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $subject->y, 570, $subject->y - 15);
        $subject->y -= 10;

        //columns headers
        $lines[0][] = ['text' => __('Qty'), 'feed' => 45, 'align' => 'right'];
        $lines[0][] = ['text' => __('SKU'), 'feed' => 100, 'align' => 'left'];
        $lines[0][] = ['text' => __('Product'), 'feed' => 250, 'align' => 'left'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $subject->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $subject->y -= 20;
    }

    protected function _drawItem($subject, $item, $page, $order)
    {
        /* Add table head */
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        //columns headers
        $lines[0][] = ['text' => $item->getPopQty(), 'feed' => 45, 'align' => 'right'];

        $sku = ($item->getPopSupplierSku() ? $item->getPopSupplierSku() : $item->getPopSku());
        $lines[0][] = ['text' => $sku, 'feed' => 100, 'align' => 'left'];

        $lines[0][] = ['text' => $item->getPopName(), 'feed' => 250, 'align' => 'left'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $subject->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $subject->y -= 20;
    }

}
