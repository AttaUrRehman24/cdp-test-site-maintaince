<?php

namespace BoostMyShop\DropShip\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class SupplierOrderEditTabs implements ObserverInterface
{


    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $tabs = $observer->getEvent()->getTabs();
        $layout = $observer->getEvent()->getLayout();

        if ($order->getpo_dropship_order_id())
        {
            $tabs->addTab(
                'dropship_section',
                [
                    'label' => __('Drop ship'),
                    'title' => __('Drop ship'),
                    'content' => $layout->createBlock('BoostMyShop\DropShip\Block\Supplier\Order\Edit\Tab\DropShip')->toHtml()
                ]
            );
        }

        return $this;
    }

}
