<?php

namespace BoostMyShop\DropShip\Controller\Manage;


class Save extends AbstractFront
{

    public function execute()
    {
        try
        {
            $data = $this->getRequest()->getPost();
            $supplier = $this->loadSupplier();

            if (isset($data['po']))
                $this->updatePos($supplier, $data['po']);

            $poId = $data['po_id'];
            $token = $data['token'];
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/index', ['po_id' => $poId, 'token' => $token, 'save' => '1']);
            return $resultRedirect;
        }
        catch(\Exception $ex)
        {
            $this->_logger->logException($ex);

            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
    }

    public function updatePos($supplier, $data)
    {
        foreach($data as $poId => $poData)
        {
            $po = $this->_orderFactory->create()->load($poId);
            if ($po->getSupplier()->getId() == $supplier->getId())  //security check to prevent POST data injection (very low risk...)
            {
                if ($poData['action'])
                    $this->_logger->log('Supplier #'.$supplier->getId().' processed action '.$poData['action'].' on PO #'.$po->getId());

                switch($poData['action'])
                {
                    case 'accept':
                        $this->_dropShip->confirmPending($poId);
                        break;
                    case 'cancel':
                        $this->_dropShip->cancelDropShip($poId);
                        break;
                    case 'confirm_shipment':
                        $tracking = isset($poData['tracking']) ? $poData['tracking'] : '';
                        $this->_dropShip->confirmShipping($poId, $tracking);
                        break;
                }

            }
            else
                throw new \Exception('Invalid data');
        }
    }

}
