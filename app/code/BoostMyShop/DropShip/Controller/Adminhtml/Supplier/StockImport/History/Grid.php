<?php namespace BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport\History;

/**
 * Class Grid
 *
 * @package   BoostMyShop\DropShip\Controller\Adminhtml\Supplier\StockImport\History
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \BoostMyShop\DropShip\Controller\Adminhtml\Supplier {

    public function execute(){

        if($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)){

            $this->_coreRegistry->register('current_supplier_id', $id);

        }

        $this->_view->loadLayout(false);
        $this->_view->renderLayout();

    }

}