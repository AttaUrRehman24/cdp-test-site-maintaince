<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\Main;

class ConfirmPending extends \BoostMyShop\DropShip\Controller\Adminhtml\Main
{

    /**
     * @return void
     */
    public function execute()
    {
        $result = [];
        $poId = $this->getRequest()->getPostValue('po_id');
        $items = $this->getRequest()->getPostValue('poProducts');
        $items = $items[$poId];

        try
        {

            $this->_dropShip->confirmPending($poId, $items);

            $result['po_id'] = $poId;
            $result['success'] = true;
        }
        catch(\Exception $ex)
        {
            $result['success'] = false;
            $result['message'] = $ex->getMessage();
            $result['stack'] = $ex->getTraceAsString();
        }

        return $this->_resultJsonFactory->create()->setData($result);
    }

}
