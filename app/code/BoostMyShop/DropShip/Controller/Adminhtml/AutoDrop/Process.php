<?php

namespace BoostMyShop\DropShip\Controller\Adminhtml\AutoDrop;

class Process extends \BoostMyShop\DropShip\Controller\Adminhtml\AutoDrop
{
    /**
     * @return void
     */
    public function execute()
    {
        try
        {
            $this->_autoDrop->process();
            $this->messageManager->addSuccess(__('Autodrop processed, please check logs below for more details.'));
        }
        catch(\Exception $ex)
        {
            $this->messageManager->addError(__('An error occured : '.$ex->getMessage()));
        }
        $this->_redirect('*/*/log');
    }
}
