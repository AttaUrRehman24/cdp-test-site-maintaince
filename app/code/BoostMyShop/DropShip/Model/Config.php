<?php

namespace BoostMyShop\DropShip\Model;

class Config
{
    /**
     * Core store config
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /*
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig){
        $this->_scopeConfig = $scopeConfig;
    }

    public function getSetting($path, $storeId = 0)
    {
        return $this->_scopeConfig->getValue('dropship/'.$path, 'store', $storeId);
    }

    public function getDropShipWarehouse()
    {
        return $this->_scopeConfig->getValue('dropship/general/warehouses');
    }

    public function supplierValidationEnabled()
    {
        return $this->_scopeConfig->getValue('dropship/general/supplier_validation_step');
    }

    public function showConfirmationPopup()
    {
        return $this->_scopeConfig->getValue('dropship/general/show_confirmation_popup');
    }

    public function selectDefaultSupplier()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/select');
    }

    public function supplierMustHavePrice()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/must_have_price');
    }

    public function supplierMustHaveDropShip()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/must_have_dropship_enabled');
    }

    public function supplierMustHaveStock()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/must_have_stock');
    }

    public function priorizeCheapest()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/priorize_cheapest');
    }

    public function priorizePrimary()
    {
        return $this->_scopeConfig->getValue('dropship/default_supplier/priorize_primary');
    }

    public function getOrderStatuses()
    {
        $statuses = false;
        if ($this->_scopeConfig->getValue('dropship/general/order_statuses'))
        {
            $statuses = explode(',', $this->_scopeConfig->getValue('dropship/general/order_statuses'));
        }
        return $statuses;
    }

}