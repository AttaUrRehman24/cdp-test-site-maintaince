<?php namespace BoostMyShop\DropShip\Model\ResourceModel;

/**
 * Class StockImport
 *
 * @package   BoostMyShop\DropShip\Model\ResourceModel
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class StockImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct()
    {
        $this->_init('bms_supplier_stock_import', 'si_id');
    }

}