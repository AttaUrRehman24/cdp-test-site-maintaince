<?php namespace BoostMyShop\DropShip\Model;

/**
 * Class SupplierStockImport
 *
 * @package   BoostMyShop\DropShip\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SupplierStockImport {

    /**
     * @var \BoostMyShop\Supplier\Model\Supplier
     */
    protected $_supplier;

    /**
     * @var \BoostMyShop\DropShip\Helper\SupplierStockImport\Ftp
     */
    protected $_ftp;

    /**
     * @var \BoostMyShop\DropShip\Model\StockImportFactory
     */
    protected $_stockImportFactory;

    /**
     * @var \BoostMyShop\DropShip\Model\StockImport
     */
    protected $_stockImport;

    /**
     * @var \BoostMyShop\DropShip\Helper\SupplierStockImport\Parser
     */
    protected $_parser;

    /**
     * @var int
     */
    protected $_cptLinesProcessed = 0;

    /**
     * @var int
     */
    protected $_cptStocksUpdates = 0;

    /**
     * @var int
     */
    protected $_cptErrors = 0;

    /**
     * @var
     */
    protected $_productModel;

    /**
     * @var \BoostMyShop\Supplier\Model\ResourceModel\Supplier\Product\CollectionFactory
     */
    protected $_supplierProductCollectionFactory;

    /**
     * @var \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\Item\CollectionFactory
     */
    protected $_warehouseItemCollectionFactory;

    /**
     * @var \BoostMyShop\AdvancedStock\Model\StockMovementFactory
     */
    protected $_stockMovementFactory;

    /**
     * @var \BoostMyShop\Supplier\Model\Supplier\ProductFactory
     */
    protected $_supplierProductFactory;

    /**
     * SupplierStockImport constructor.
     * @param \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory
     * @param \BoostMyShop\AdvancedStock\Model\StockMovementFactory $stockMovementFactory
     * @param \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\Item\CollectionFactory $warehouseItemCollectionFactory
     * @param \BoostMyShop\Supplier\Model\ResourceModel\Supplier\Product\CollectionFactory $supplierProductCollectionFactory
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \BoostMyShop\DropShip\Helper\SupplierStockImport\Parser $parser
     * @param \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory
     * @param \BoostMyShop\DropShip\Helper\SupplierStockImport\Ftp $ftp
     * @param \BoostMyShop\Supplier\Model\Supplier $supplier
     */
    public function __construct(
        \BoostMyShop\Supplier\Model\Supplier\ProductFactory $supplierProductFactory,
        \BoostMyShop\AdvancedStock\Model\StockMovementFactory $stockMovementFactory,
        \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\Item\CollectionFactory $warehouseItemCollectionFactory,
        \BoostMyShop\Supplier\Model\ResourceModel\Supplier\Product\CollectionFactory $supplierProductCollectionFactory,
        \Magento\Catalog\Model\Product $productModel,
        \BoostMyShop\DropShip\Helper\SupplierStockImport\Parser $parser,
        \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory,
        \BoostMyShop\DropShip\Helper\SupplierStockImport\Ftp $ftp,
        \BoostMyShop\Supplier\Model\Supplier $supplier
    ){
        $this->_supplier = $supplier;
        $this->_ftp = $ftp;
        $this->_stockImportFactory = $stockImportFactory;
        $this->_parser = $parser;
        $this->_productModel = $productModel;
        $this->_supplierProductCollectionFactory = $supplierProductCollectionFactory;
        $this->_warehouseItemCollectionFactory = $warehouseItemCollectionFactory;
        $this->_stockMovementFactory = $stockMovementFactory;
        $this->_supplierProductFactory = $supplierProductFactory;
    }

    /**
     * @param int $supplierId
     * @throws \Exception
     */
    public function run($supplierId){

        $this->_supplier->load($supplierId);

        if($this->_supplier->getId()){

            $this->_stockImport = $this->_stockImportFactory->create();
            $this->_stockImport->setsi_supplier_id($this->_supplier->getId())
                ->setsi_date(time())
                ->setsi_status(\BoostMyShop\DropShip\Model\StockImport::STATUS_IN_PROGRESS)
                ->save();

            try {
                $this->_validateSettings();
                $filename = $this->_downloadFile();

                if (!empty($filename)) {

                    $this->_processImport($filename);

                }

                $this->_stockImport->setsi_status(\BoostMyShop\DropShip\Model\StockImport::STATUS_COMPLETE)
                    ->setsi_summary($this->_cptLinesProcessed.' lines processed, '.$this->_cptStocksUpdates.' stocks updates, '.$this->_cptErrors.' error(s)')
                    ->save();

            } catch(\Exception $e) {

                $this->_stockImport->setsi_status(\BoostMyShop\DropShip\Model\StockImport::STATUS_ERROR)
                    ->setsi_summary($e->getMessage())
                    ->save();

                throw new \Exception($e->getMessage(), $e->getCode(), $e);

            }

        } else {

            throw new \Exception('Not able to retrieve supplier with ID '.$supplierId);

        }

    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function _validateSettings(){

        $params = [
            'sup_ftp_host',
            'sup_ftp_port',
            'sup_ftp_login',
            'sup_ftp_password',
            'sup_ftp_path',
            'sup_file_import_separator',
            'sup_target_warehouse'
        ];

        foreach($params as $param){

            if($this->_supplier->getData($param) == '')
                throw new \Exception('Empty value for param : '.$param);

        }

        if($this->_supplier->getData('sup_file_import_index_sku') == '' && $this->_supplier->getData('sup_file_import_index_supplier_sku') == '')
            throw new \Exception('sku and supplier\'s sku indexes can\'t be empty together');

        return 0;

    }

    /**
     * @return string
     */
    protected function _downloadFile(){

        return $this->_ftp
            ->setHost($this->_supplier->getsup_ftp_host())
            ->setPort($this->_supplier->getsup_ftp_port())
            ->setLogin($this->_supplier->getsup_ftp_login())
            ->setPassword($this->_supplier->getsup_ftp_password())
            ->setPassiveMode($this->_supplier->getsup_ftp_passive_mode())
            ->setSftp($this->_supplier->getsup_ftp_sftp())
            ->download($this->_getFilenameToDownload());

    }

    /**
     * @return string
     */
    protected function _getFilenameToDownload() {

        $filename = $this->_supplier->getsup_ftp_path();

        $matches = [];
        $res = preg_match_all('#{{([\w\-:_\s]*)}}#i', $filename, $matches);

        if(!empty($res) && count($matches) > 0) {

            $patterns = $matches[0];
            $values = $matches[1];

            foreach($patterns as $index => $pattern) {

                if(isset($values[$index]))
                    $filename = str_replace($pattern, date($values[$index]), $filename);

            }
        }

        return $filename;

    }

    protected function _processImport($filename){

        $fp = fopen($filename, "r");
        $cptLine = 0;

        while($line = $this->_getLines($fp)){

            try {

                $line = $this->_parser->parseLine($line);

                $skuIndex = $this->_supplier->getsup_file_import_index_sku();
                $sku = isset($line[$skuIndex]) ? $line[$skuIndex] : null;

                $supplierSkuIndex = $this->_supplier->getsup_file_import_index_supplier_sku();
                $supplierSku = isset($line[$supplierSkuIndex]) ? $line[$supplierSkuIndex] : null;

                $qtyIndex = $this->_supplier->getsup_file_import_index_qty();
                $qty = isset($line[$qtyIndex]) ? $line[$qtyIndex] : null;

                $buyingPriceIndex = $this->_supplier->getsup_file_import_index_buying_price();
                $buyingPrice = isset($line[$buyingPriceIndex]) ? str_replace(',','.',$line[$buyingPriceIndex]) : null;

                $productId = $this->_retrieveProductId($sku, $supplierSku);

                if (empty($productId)) {

                    $this->_stockImport->addRow($cptLine, \BoostMyShop\DropShip\Model\StockImport\Item::STATUS_PRODUCT_NOT_FOUND);
                    $this->_cptErrors++;

                } else {

                    $this->_updateSupplierProduct($productId, $buyingPrice, $qty, $supplierSku);
                    $this->_stockImport->addRow($cptLine, \BoostMyShop\DropShip\Model\StockImport\Item::STATUS_SUCCESS);
                    $this->_cptStocksUpdates++;

                }

            }catch(\Exception $e) {

                $this->_stockImport->addRow($cptLine, \BoostMyShop\DropShip\Model\StockImport\Item::STATUS_ERROR);
                $this->_cptErrors++;

            }

            $cptLine++;
            $this->_cptLinesProcessed++;

        }

    }

    /**
     * @param $fp
     * @return array $lines
     */
    protected function _getLines($fp){

        if(empty($this->_supplier->getsup_file_import_enclosure())){

            $lines = fgetcsv($fp, 35000, $this->_supplier->getsup_file_import_separator());

        }else{

            $lines = fgetcsv($fp, 35000, $this->_supplier->getsup_file_import_separator(), $this->_supplier->getsup_file_import_enclosure());

        }

        return $lines;

    }

    /**
     * @param string $sku
     * @param string $supplierSku
     * @return mixed int|null
     */
    protected function _retrieveProductId($sku, $supplierSku) {

        $id = $this->_productModel->getIdBySku($sku);

        if(empty($id)){

            $id = $this->_supplierProductCollectionFactory->create()
                ->addFieldToFilter('sp_sup_id', $this->_supplier->getId())
                ->addFieldToFilter('sp_sku', $supplierSku)
                ->getFirstItem()
                ->getsp_product_id();

        }

        return $id;

    }

    /**
     * @param int $productId
     * @param float $buyingPrice
     * @param int $qty
     * @param string $supplierSku
     */
    protected function _updateSupplierProduct($productId, $buyingPrice, $qty, $supplierSku) {

        if(!empty($buyingPrice) && $buyingPrice > 0) {
            $supplierProduct = $this->_supplierProductCollectionFactory->create()
                ->addFieldToFilter('sp_sup_id', $this->_supplier->getId())
                ->addFieldToFilter('sp_product_id', $productId)
                ->getFirstItem();

            if ($supplierProduct->getId()) {

                $supplierProduct->setsp_price($buyingPrice)
                    ->save();

            } else {

                $this->_supplierProductFactory->create()
                    ->setsp_created_at(time())
                    ->setsp_updated_at(time())
                    ->setsp_product_id($productId)
                    ->setsp_sup_id($this->_supplier->getId())
                    ->setsp_sku($supplierSku)
                    ->setsp_price($buyingPrice)
                    ->save();

            }
        }

        if(!is_null($qty) && $qty != "" && $qty >= 0) {

            $warehouseItem = $this->_warehouseItemCollectionFactory->create()
                ->addFieldTofilter('wi_warehouse_id', $this->_supplier->getsup_target_warehouse())
                ->addFieldToFilter('wi_product_id', $productId)
                ->getFirstItem();

            $originalQty = ($warehouseItem->getId()) ? $warehouseItem->getwi_physical_quantity() : 0;

            $this->_stockMovementFactory->create()
                ->updateProductQuantity(
                    $productId,
                    $this->_supplier->getsup_target_warehouse(),
                    $originalQty,
                    $qty,
                    'Supplier stock import for '.$this->_supplier->getsup_name(),
                    null
                );

        }

    }

}