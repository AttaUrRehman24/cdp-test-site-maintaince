<?php

namespace BoostMyShop\DropShip\Model;

class DropShip
{
    protected $_purchaseOrderFactory;
    protected $_dateTime;
    protected $_backendAuthSession;
    protected $_config;
    protected $_orderFactory;
    protected $_orderPreparationFactory;
    protected $_orderItemFactory;
    protected $_notification;

    public function __construct(
        \BoostMyShop\Supplier\Model\OrderFactory $purchaseOrderFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \BoostMyShop\DropShip\Model\Config $config,
        \BoostMyShop\DropShip\Model\DropShip\Notification $notification,
        \BoostMyShop\OrderPreparation\Model\OrderPreparationFactory $orderPreparationFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\ItemFactory $orderItemFactory
    ){
        $this->_purchaseOrderFactory = $purchaseOrderFactory;
        $this->_dateTime = $dateTime;
        $this->_config = $config;
        $this->_orderFactory = $orderFactory;
        $this->_orderPreparationFactory = $orderPreparationFactory;
        $this->_orderItemFactory = $orderItemFactory;
        $this->_notification = $notification;
    }

    public function create($supplier, $order, $items, $settings = [])
    {
        $po = $this->createPo($order, $supplier, $settings);

        foreach($items as $item) {
            $additionnal = ['pop_price' => $item['price'], 'pop_price_base' => $item['price'], 'pop_dropship_order_item_id' => $item['order_item_id']];
            $po->addProduct($item['order_item']->getproduct_id(), $item['qty'], $additionnal);
        }

        $this->notifySupplier($po);

        return $po;
    }

    public function createPo($order, $supplier, $settings)
    {
        $model = $this->_purchaseOrderFactory->create();
        $model->setpo_type('ds');
        $model->applyDefaultData($supplier->getId());
        $model->setpo_eta($this->_dateTime->gmtDate());
        $model->setpo_status(\BoostMyShop\Supplier\Model\Order\Status::toConfirm);
        $model->setpo_dropship_order_id($order->getId());
        $model->setpo_warehouse_id($this->_config->getDropShipWarehouse());
        $model->setpo_manager($this->getUserId());
        $model->save();

        //todo : set manager / store

        return $model;
    }

    public function notifySupplier($po)
    {
        $this->_notification->notifyToSupplier($po);
    }

    public function cancelDropShip($poId)
    {
        $po = $this->_purchaseOrderFactory->create()->load($poId);
        $po->delete();
    }

    public function confirmPending($poId, $items = [])
    {
        $po = $this->_purchaseOrderFactory->create()->load($poId);
        $po->setpo_status(\BoostMyShop\Supplier\Model\Order\Status::expected);
        $po->save();

        foreach($po->getAllItems() as $item)
        {
            if (isset($items[$item->getId()]))
            {
                foreach($items[$item->getId()] as $k => $v)
                    $item->setData($k, $v);
                $item->save();
            }
        }

    }

    public function confirmShipping($poId, $trackingNumber = '', $notifyCustomer = true)
    {
        //receiive PO
        $po = $this->_purchaseOrderFactory->create()->load($poId);
        $this->receivePo($po);

        //ship order
        $this->shipSalesOrder($po, $trackingNumber, $notifyCustomer);

        return $po;
    }


    public function receivePo($po)
    {
        $products = [];
        foreach($po->getAllItems() as $poItem)
        {
            $products[$poItem->getpop_product_id()] = ['qty' => $poItem->getpop_qty()];
        }
        $po->processReception($this->getUserName(), $products);
        $po->setpo_status(\BoostMyShop\Supplier\Model\Order\Status::complete);
        $po->save();
    }

    public function shipSalesOrder($po, $trackingNumber = '', $notifyCustomer = true)
    {
        $order = $this->_orderFactory->create()->load($po->getpo_dropship_order_id());

        $products = [];
        foreach($po->getAllItems() as $item)
        {
            $orderItemId = $item->getpop_dropship_order_item_id();
            $products[$orderItemId] = $item->getpop_qty();
        }
        $inProgress = $this->_orderPreparationFactory->create()->addOrder($order, $products, $this->getUserId(), $this->_config->getDropShipWarehouse());
        $inProgress->pack(true, true);
        if ($trackingNumber)
            $inProgress->addTracking($trackingNumber);
        if ($notifyCustomer)
            $inProgress->notifyCustomer();
        $inProgress->delete();
    }

    public function getUserName()
    {
        $userName = '';
        if ($this->getBackendSession()->isLoggedIn())
            $userName =  $this->getBackendSession()->getUser()->getUsername();
        return $userName;
    }

    public function getUserId()
    {
        $userId = null;
        if ($this->getBackendSession()->isLoggedIn())
            $userId =  $this->getBackendSession()->getUser()->getId();
        return $userId;
    }

    public function getBackendSession()
    {
        if (!$this->_backendAuthSession) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $this->_backendAuthSession = $objectManager->get('\Magento\Backend\Model\Auth\Session');
        }
        return $this->_backendAuthSession;
    }
}
