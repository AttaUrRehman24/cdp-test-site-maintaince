<?php namespace BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport\History;

/**
 * Class Grid
 *
 * @package   BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport\History
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\DropShip\Model\ResourceModel\StockImport\CollectionFactory
     */
    protected $_stockImportCollectionFactory;

    /**
     * @var \BoostMyShop\DropShip\Model\StockImportFactory
     */
    protected $_stockImportFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Grid constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory
     * @param \BoostMyShop\DropShip\Model\ResourceModel\StockImport\CollectionFactory $stockImportCollectionFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\DropShip\Model\StockImportFactory $stockImportFactory,
        \BoostMyShop\DropShip\Model\ResourceModel\StockImport\CollectionFactory $stockImportCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        parent::__construct($context, $backendHelper, $data);
        $this->_stockImportCollectionFactory = $stockImportCollectionFactory;
        $this->_stockImportFactory = $stockImportFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('stockImportGrid');
        $this->setDefaultSort('si_date');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Stock Imports'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {

        $collection = $this->_stockImportCollectionFactory->create()
            ->addFieldToFilter('si_supplier_id', $this->_coreRegistry->registry('current_supplier_id'));
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {

        $this->addColumn('si_date',
            [
                'header' => __('Create at'),
                'index' => 'si_date',
                'type' => 'datetime'
            ]
        );

        $this->addColumn('si_status',
            [
                'header' => __('Status'),
                'index' => 'si_status',
                'type' => 'options',
                'options' => $this->_stockImportFactory->create()->getStatuses()
            ]
        );

        $this->addColumn('si_summary',
            [
                'header' => __('Summary'),
                'index' => 'si_summary',
                'filter' => false,
                'sortable' => false
            ]
        );

        $this->addColumn('dl_report',
            [
                'header' => __('Download Report'),
                'index' => 'si_id',
                'sortable' => false,
                'filter' => false,
                'renderer' => '\BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport\History\Widget\Grid\Column\Renderer\DownloadReport'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('dropship/supplier_stockimport_history/grid', ['_current' => true, 'id' => $this->_coreRegistry->registry('current_supplier_id')]);
    }

}