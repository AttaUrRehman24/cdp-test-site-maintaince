<?php namespace BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport;

/**
 * Class Settings
 *
 * @package   BoostMyShop\DropShip\Block\Supplier\Edit\Tabs\StockImport
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Settings extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory
     */
    protected $_warehouseCollectionFactory;

    /**
     * @var \BoostMyShop\Supplier\Model\Supplier
     */
    protected $_supplier;

    /**
     * Settings constructor.
     * @param \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ){
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_warehouseCollectionFactory = $warehouseCollectionFactory;
    }

    /**
     * @param \BoostMyShop\Supplier\Model\Supplier $supplier
     * @return $this
     */
    public function setSupplier(\BoostMyShop\Supplier\Model\Supplier $supplier){

        $this->_supplier = $supplier;
        return $this;

    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create();

        $fieldsetFtpConnection = $form->addFieldset(
            'ftp_connection',
            [
                'class' => 'user-defined',
                'legend' => __('FTP Connection'),
                'collapsable' => false
            ]
        );

        $fieldsetFtpConnection->addField(
            'host',
            'text',
            [
                'name' => 'sup_ftp_host',
                'value' => $this->_supplier->getsup_ftp_host(),
                'label' => __('Host'),
                'required' => false,
            ]
        );

        $fieldsetFtpConnection->addField(
            'port',
            'text',
            [
                'name' => 'sup_ftp_port',
                'value' => $this->_supplier->getsup_ftp_port(),
                'label' => __('Port'),
                'required' => false,
            ]
        );

        $fieldsetFtpConnection->addField(
            'login',
            'text',
            [
                'name' => 'sup_ftp_login',
                'value' => $this->_supplier->getsup_ftp_login(),
                'label' => __('Login'),
                'required' => false,
            ]
        );

        $fieldsetFtpConnection->addField(
            'password',
            'text',
            [
                'name' => 'sup_ftp_password',
                'value' => $this->_supplier->getsup_ftp_password(),
                'label' => __('Password'),
                'required' => false,
            ]
        );

        $fieldsetFtpConnection->addField(
            'filepath',
            'text',
            [
                'name' => 'sup_ftp_path',
                'value' => $this->_supplier->getsup_ftp_path(),
                'label' => __('File Path'),
                'required' => false,
            ]
        );

        $fieldsetFtpConnection->addField(
            'passive_mode',
            'select',
            [
                'name' => 'sup_ftp_passive_mode',
                'value' => $this->_supplier->getsup_ftp_passive_mode(),
                'label' => __('Passive Mode'),
                'required' => false,
                'options' => [0 => __('Disabled'), 1 => __('Enabled')]
            ]
        );

        $fieldsetFileSettings = $form->addFieldset(
            'file_settings',
            [
                'class' => 'user-defined',
                'legend' => __('File Settings'),
                'collaspable' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'separator',
            'text',
            [
                'name' => 'sup_file_import_separator',
                'value' => $this->_supplier->getsup_file_import_separator(),
                'label' => __('Separator'),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'enclosure',
            'text',
            [
                'name' => 'sup_file_import_enclosure',
                'value' => $this->_supplier->getsup_file_import_enclosure(),
                'label' => __('Enclosure'),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'target_warehouse',
            'select',
            [
                'name' => 'sup_target_warehouse',
                'value' => $this->_supplier->getsup_target_warehouse(),
                'label' => __('Target Warehouse'),
                'options' => $this->_getWarehousesAsOptions(),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'sku_index',
            'text',
            [
                'name' => 'sup_file_import_index_sku',
                'value' => $this->_supplier->getsup_file_import_index_sku(),
                'label' => __('Sku Index'),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'supplier_sku_index',
            'text',
            [
                'name' => 'sup_file_import_index_supplier_sku',
                'value' => $this->_supplier->getsup_file_import_index_supplier_sku(),
                'label' => __('Supplier Sku Index'),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'qty_index',
            'text',
            [
                'name' => 'sup_file_import_index_qty',
                'value' => $this->_supplier->getsup_file_import_index_qty(),
                'label' => __('Qty index'),
                'required' => false
            ]
        );

        $fieldsetFileSettings->addField(
            'buying_price_index',
            'text',
            [
                'name' => 'sup_file_import_index_buying_price',
                'value' => $this->_supplier->getsup_file_import_index_buying_price(),
                'label' => __('Buying Price Index'),
                'required' => false
            ]
        );

        $fieldsetImportNow = $form->addFieldset(
            'import_now',
            [
                'class' => 'user-defined',
                'legend' => __('Import Now'),
                'collapsable' => false
            ]
        );

        $fieldsetImportNow->addField(
            'import_now_button',
            'button',
            [
                'label' => __('Import Now'),
                'onclick' => 'window.setLocation(\''.$this->getUrl('dropship/supplier_stockimport/process', ['id' => $this->_supplier->getId()]).'\')',
                'class' => 'primary',
                'value' => __('Import Now')
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return array $warehouseOptions
     */
    protected function _getWarehousesAsOptions()
    {

        $warehouseOptions = [];

        foreach ($this->_warehouseCollectionFactory->create() as $item) {

            $warehouseOptions[$item->getId()] = $item->getw_name();

        }

        return $warehouseOptions;

    }

}