<?php
namespace BoostMyShop\DropShip\Block\AutoDrop;

class Header extends \Magento\Backend\Block\Template
{
    protected $_template = 'AutoDrop/Header.phtml';

    protected $_coreRegistry = null;
    protected $_config = null;
    protected $_request;

    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magento\Framework\Registry $registry,
                                \BoostMyShop\DropShip\Model\Config $config,
                                array $data = [],
                                \Magento\Framework\App\Request\Http $request
    )
    {
        parent::__construct($context, $data);
        $this->_config = $config;
        $this->_coreRegistry = $registry;
        $this->_request = $request;
    }

    public function getAutoDropUrl()
    {
        return $this->getUrl('*/autodrop/process');
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/main/index');
    }

}