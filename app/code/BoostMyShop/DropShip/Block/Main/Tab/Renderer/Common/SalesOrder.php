<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\Common;

use Magento\Framework\DataObject;

class SalesOrder extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $purchaseOrder)
    {
        $url = $this->getUrl('sales/order/view', ['order_id' => $purchaseOrder->getentity_id()]);
        return '<a href="'.$url.'">'.$purchaseOrder->getincrement_id().'</a>';
    }

}