<?php

namespace BoostMyShop\DropShip\Block\Main\Tab\Renderer\PendingValidation;

use Magento\Framework\DataObject;

class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $order)
    {
        $html = [];
        $html[] = '<center><input type="button" value="confirm" onclick="objDropShip.confirmPending('.$order->getId().')"></center>';
        $html[] = '<center><input type="button" value="cancel" onclick="objDropShip.cancelDropShip('.$order->getId().')"></center>';
        return implode('', $html);
    }

}