<?php
namespace BoostMyShop\DropShip\Block\Main;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected $_coreRegistry;

    protected $_template = 'Magento_Backend::widget/tabshoriz.phtml';

    protected $_config;

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_tabs');
        $this->setDestElementId('tab_container');
        $this->setTitle(__('Drop Shipping'));

    }

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \BoostMyShop\DropShip\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $jsonEncoder, $authSession, $data);
        $this->_config = $config;
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {

        $block =  $this->getLayout()->createBlock('BoostMyShop\DropShip\Block\Main\Tab\ToDropShip');
        $this->addTab(
            'tab_todropship',
            [
                'label' => __('Items to drop ship (<span id="tab_todropship_count"></span>)'),
                'title' => __('Items to drop ship'),
                'content' => $block->toHtml()
            ]
        );

        if ($this->_config->supplierValidationEnabled())
        {
            $block =  $this->getLayout()->createBlock('BoostMyShop\DropShip\Block\Main\Tab\PendingValidation');
            $this->addTab(
                'tab_pending_validation',
                [
                    'label' => __('Pending supplier validation (<span id="tab_pending_validation_count"></span>)'),
                    'title' => __('Pending supplier validation'),
                    'content' => $block->toHtml()
                ]
            );
        }

        $block =  $this->getLayout()->createBlock('BoostMyShop\DropShip\Block\Main\Tab\PendingShipping');
        $this->addTab(
            'tab_pending_shipping',
            [
                'label' => __('Pending supplier shipping (<span id="tab_pending_shipping_count"></span>)'),
                'title' => __('Pending supplier shipping'),
                'content' => $block->toHtml()
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\DropShip\Block\Main\Tab\History');
        $this->addTab(
            'tab_history',
            [
                'label' => __('History'),
                'title' => __('History'),
                'content' => $block->toHtml()
            ]
        );

        return parent::_beforeToHtml();
    }
}