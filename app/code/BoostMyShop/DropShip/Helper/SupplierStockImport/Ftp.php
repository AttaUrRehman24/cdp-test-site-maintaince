<?php namespace BoostMyShop\DropShip\Helper\SupplierStockImport;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Ftp
 *
 * @package   BoostMyShop\DropShip\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Ftp {

    /**
     * @var string
     */
    protected $_host;

    /**
     * @var int
     */
    protected $_port;

    /**
     * @var string
     */
    protected $_login;

    /**
     * @var string
     */
    protected $_password;

    /**
     * @var mixed null|boolean
     */
    protected $_passiveMode;

    /**
     * @var bool
     */
    protected $_sftp;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * Ftp constructor.
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    ){
        $this->_directoryList = $directoryList;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host){

        $this->_host = $host;
        return $this;

    }

    /**
     * @return string
     */
    public function getHost(){

        return $this->_host;

    }

    /**
     * @param int $port
     * @return $this
     */
    public function setPort($port){

        $this->_port = $port;
        return $this;

    }

    /**
     * @return int
     */
    public function getPort(){

        return $this->_port;

    }

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin($login){

        $this->_login = $login;
        return $this;

    }

    /**
     * @return string
     */
    public function getLogin(){

        return $this->_login;

    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password){

        $this->_password = $password;
        return $this;

    }

    /**
     * @return string
     */
    public function getPassword(){

        return $this->_password;

    }

    /**
     * @param int $passiveMode
     * @return $this
     */
    public function setPassiveMode($passiveMode){

        $this->_passiveMode = $passiveMode;
        return $this;

    }

    /**
     * @return mixed null|boolean
     */
    public function getPassiveMode(){

        return $this->_passiveMode;

    }

    /**
     * @param int
     * @return $this
     */
    public function setSftp($sftp){

        $this->_sftp = $sftp;
        return $this;

    }

    /**
     * @return bool
     */
    public function getSftp(){

        return $this->_sftp;

    }

    /**
     * @param string $path
     * @return string
     * @throws \Exception
     */
    public function download($path){

        if ($this->getSftp()) {

            return $this->_downloadFromSftp($path);

        } else {

            return $this->_downloadFromFtp($path);

        }

    }

    /**
     * @param string $path
     * @return string
     * @throws \Exception
     */
    protected function _downloadFromSftp($path){

        $localFilename = $this->_directoryList->getPath(DirectoryList::TMP).'/supplier_stock_import_'.date('YmdHis');
        $connId = $this->_getSftpConnection();
        $content = file_get_contents("ssh2.sftp://$connId$path", 'r');
        file_put_contents($localFilename, $content);

        return $localFilename;

    }

    /**
     * @param string $path
     * @return string
     * @throws \Exception
     */
    protected function _downloadFromFtp($path){

        $connId = $this->_getConnection();
        $path = explode('/', $path);
        $remoteFilename = array_pop($path);
        $localFilename = $this->_directoryList->getPath(DirectoryList::TMP).'/supplier_stock_import_'.date('YmdHis');

        foreach($path as $dir){

            if(!@ftp_chdir($connId, $dir))
                throw new \Exception('Not able to access to directory '.$dir);

        }

        if(!@ftp_get($connId, $localFilename, $remoteFilename, FTP_BINARY))
            throw new \Exception('Not able to download file '.$remoteFilename);

        ftp_close($connId);

        return $localFilename;

    }

    /**
     * @return resource $connId
     * @throws \Exception
     */
    protected function _getConnection(){

        $connId = @ftp_connect($this->getHost(), $this->getPort());

        if(!$connId)
            throw new \Exception('Not able to connect to ftp://'.$this->getHost().':'.$this->getPort());

        if(!@ftp_login($connId, $this->getLogin(), $this->getPassword()))
            throw new \Exception('Not able to connect to ftp://'.$this->getLogin().':'.$this->getPassword().'@'.$this->getHost().':'.$this->getPort());

        if($this->getPassiveMode())
            @ftp_pasv($connId, true);

        return $connId;

    }

    /**
     * @return resource
     * @throws \Exception
     */
    protected function _getSftpConnection(){

        $connId = @ssh2_connect($this->getHost(), $this->getPort());

        if(!$connId)
            throw new \Exception('Not able to connect to sftp://'.$this->getHost().':'.$this->getPort());

        if(!@ssh2_auth_password($connId, $this->getLogin(), $this->getPassword()))
            throw new \Exception('Not able to connect to sftp://'.$this->getLogin().':'.$this->getPassword().'@'.$this->getHost().':'.$this->getPort());

        return @ssh2_sftp($connId);

    }

}