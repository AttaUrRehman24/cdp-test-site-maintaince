<?php
namespace BoostMyShop\SupplierReturn\Model\ResourceModel;

class Supplierreturn extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bms_supplier_return', 'bsr_id');
    }
}
?>