<?php
namespace BoostMyShop\SupplierReturn\Model;

class Product extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BoostMyShop\SupplierReturn\Model\ResourceModel\Product');
    }
}
?>