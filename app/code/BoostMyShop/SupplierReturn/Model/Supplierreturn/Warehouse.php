<?php

namespace BoostMyShop\SupplierReturn\Model\Supplierreturn;

class Warehouse
{

    protected $_collectionFactory;

    public function __construct(\BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $collectionFactory)
    {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        $options = array();
        $collection = $this->_collectionFactory->create();

        foreach($collection as $item)
        {
            $options[$item->getId()] = $item->getw_name();
        }

        return $options;
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
