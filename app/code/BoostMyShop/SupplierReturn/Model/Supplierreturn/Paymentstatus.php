<?php

namespace BoostMyShop\SupplierReturn\Model\Supplierreturn;

class Paymentstatus
{
	/**
     * Options getter
     *
     * @return array
     */
    const PENDING = 1;
    const PAID = 2;

    public function getOptionArray()
    {
        return array(
            SELF::PENDING =>__('Pending'),
            SELF::PAID =>__('Paid'),
        );
    }

  /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}