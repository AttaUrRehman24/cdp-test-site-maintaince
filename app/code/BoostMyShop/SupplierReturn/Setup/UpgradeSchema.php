<?php

namespace BoostMyShop\SupplierReturn\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_return'),
                'bsr_payment_date',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'nullable' => true,
                    'comment' => 'Payment Date'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_return'),
                'bsr_payment_method',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 200,
                    'nullable' => true,
                    'comment' => 'Payment Method'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_return'),
                'bsr_payment_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'length'   => 255,
                    'default'  => '1',
                    'comment'  => 'Payment Method'
                ]
            );

		}

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('bms_supplier_return'),
                'bsr_notes',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 400,
                    'comment' => 'Notes'
                ]
            );
        }

		$installer->endSetup();			
	}

}