<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPagee;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('BoostMyShop_SupplierReturn::supplierreturn');
        $resultPage->addBreadcrumb(__('BoostMyShop'), __('BoostMyShop'));
        $resultPage->addBreadcrumb(__('Manage item'), __('Supplier Return'));
        $resultPage->getConfig()->getTitle()->prepend(__('Supplier Return'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BoostMyShop_SupplierReturn::return');
    }


}
?>