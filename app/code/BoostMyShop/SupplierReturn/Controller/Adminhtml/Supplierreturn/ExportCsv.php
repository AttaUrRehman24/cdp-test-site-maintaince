<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportCsv extends \Magento\Backend\App\Action
{
    protected $_fileFactory;

    public function execute()
    {
        try{

            $csv = $this->_view->getLayout()->createBlock('\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Grid')->getCsv();

            $date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');

            return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                'supplier_return' . $date . '.csv',
                $csv,
                DirectoryList::VAR_DIR,
                'application/csv'
            );

        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }
    }
}