<?php
namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class Save extends \Magento\Backend\App\Action
{

    protected $eventManager;
    protected $_dateFilter;

    /**
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Magento\Framework\Event\Manager $eventManager
    )
    {
        parent::__construct($context);
        $this->eventManager = $eventManager;
        $this->_dateFilter = $dateFilter;
    }

    protected function _filterPostData($data)
    {
        $inputFilter = new \Zend_Filter_Input(
            ['bsr_shipped_date' => $this->_dateFilter, 'bsr_payment_date' => $this->_dateFilter],
            [],
            $data
        );
        $data = $inputFilter->getUnescaped();
        return $data;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('BoostMyShop\SupplierReturn\Model\Supplierreturn');

            $id = (int)$this->getRequest()->getParam('bsr_id');
            if ($id) {
                $model->load($id);
                $model->setCreatedAt(date('Y-m-d H:i:s'));
            }

            $data = $this->_filterPostData($data);

            $model->setData($data);
            $model->setBsrSupplierId($data['supplier_id']);

            try {

                if (count($model->getAllItems()) > 0) {
                    foreach($model->getAllItems() as $item)
                    {
                        if (isset($data['products'][$item->getbsrp_product_id()]))
                            $this->updateSupplierReturnProduct($item, $data['products'][$item->getbsrp_product_id()]);
                    }
                }


                if (isset($data['products_to_add']) && !empty($data['products_to_add'])){
                    $this->addProducts($model, $data['products_to_add']); 
                }
                
                $model->save();

                $this->eventManager->dispatch('supplierreturn_event_save_after',['id'=>$model->getId()]);

                $this->messageManager->addSuccess(__('The Supplier return has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['bsr_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/edit', ['bsr_id' => $model->getId(), '_current' => true]);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                //$this->messageManager->addException($e, __('Something went wrong while saving the Supplierreturn.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['bsr_id' => $this->getRequest()->getParam('bsr_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }


    /**
     * 
     * @param $model
     * @param $productsToAdd
     */
    protected function addProducts($model, $productsToAdd)
    {
        $hasChanges = false;
        
        $productsToAdd = explode(';', $productsToAdd);
        foreach($productsToAdd as $item)
        {
            if (count(explode('=', $item)) == 2)
            {
                list($productId, $qty) = explode('=', $item);
                if ($qty > 0)
                {
                    $model->addProduct($productId, $qty);
                    $hasChanges = true;
                }
            }
        }
        return $hasChanges;
    }

    /**
     * Update single supplier return product from post data
     *
     * @param $returnProduct
     * @param $data
     */
    protected function updateSupplierReturnProduct($returnProduct, $data)
    {
        
        if (isset($data['remove']))
        {
            $returnProduct->delete();
        } 
        else
        {
            $returnProduct->setbsrp_qty($data['bsrp_qty']);
            $returnProduct->setbsrp_notes($data['bsrp_notes']);
            $returnProduct->save();
            $hasChanges = true;
        }

    }

}