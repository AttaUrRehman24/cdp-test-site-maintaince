<?php
namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('bsr_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('BoostMyShop\SupplierReturn\Model\Supplierreturn');
                $model->load($id);
                if(count($model->getAllItems()) > 0){
                    foreach ($model->getAllItems() as $item) {
                        $item->delete();
                    }
                }
                
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The supplier return has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['bsr_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a supplier return to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}