<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class NotifySupplier extends \BoostMyShop\SupplierReturn\Controller\Adminhtml\SpReturn
{
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');

        try
        {
            $supplierreturn = $this->_supplierreturnFactory->create()->load($id);
            $this->_notification->notifyToSupplier($supplierreturn);
            $this->messageManager->addSuccess(__('Supplier notified.'));
        }
        catch(\Exception $ex)
        {
            $this->messageManager->addError(__($ex->getMessage()));
        }

        $this->_redirect('*/*/Edit', ['bsr_id' => $id]);
    }
}
?>