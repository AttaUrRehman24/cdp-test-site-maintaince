<?php

namespace BoostMyShop\SupplierReturn\Controller\Adminhtml\Supplierreturn;

class AddProductsGrid extends \BoostMyShop\SupplierReturn\Controller\Adminhtml\SpReturn
{
    /**
     * @return void
     */
    public function execute()
    {
        
        $bsr_id = $this->getRequest()->getParam('bsr_id');
        $model = $this->_supplierreturnFactory->create();
        $model->load($bsr_id);
        $this->_coreRegistry->register('current_supplier_return', $model);
        $resultLayout = $this->_resultLayoutFactory->create();
        $block = $resultLayout->getLayout()->createBlock('\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts');
        return $resultLayout;

    }
}
