<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer;

use Magento\Framework\DataObject;

class Remove extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {
        $html = '<input  type="checkbox" name="products['.$row->getId().'][remove]" id="products['.$row->getId().'][remove]" value="1">';

        return $html;
    }
}