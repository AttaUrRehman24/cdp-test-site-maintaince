<?php 
namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer;
use Magento\Framework\DataObject;

class Suppliername extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_supplier;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \BoostMyShop\Supplier\Model\Source\Supplier $_supplier,
        array $data = []){
        $this->_supplier = $_supplier;
        parent::__construct($context, $data);
    }

    public function render(DataObject $row)
    {
        $html = '<a href="'.$this->getUrl("supplier/supplier/edit", array()) . "sup_id/" . $row->getbsr_supplier_id().'" >' . $this->getSuppliers($row->getbsr_supplier_id()) . '</a>';
        return $html;
    }

    public function renderExport(DataObject $row)
    {
        $html =  $this->getSuppliers($row->getbsr_supplier_id()) ;
        return $html; 
    }

    public function getSuppliers($supplier_id){
        $_supplierdata  = $this->_supplier->toArray();
        foreach($_supplierdata as $id => $value){
            if($id == $supplier_id){
                return $value;
            }
        }
    }

}
