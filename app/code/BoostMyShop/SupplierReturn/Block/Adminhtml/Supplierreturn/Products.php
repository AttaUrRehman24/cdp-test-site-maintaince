<?php

namespace BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn;

use Magento\Backend\Block\Widget\Grid\Column;

class Products extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;
    protected $_supplierReturnFactory = null;
    protected $_supplierConfig = null;
    protected $_productFactory = null;
    protected $_productCollectionFactory = null;
    protected $_resource;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productFactory,
        \BoostMyShop\SupplierReturn\Model\Product $supplierReturnFactory,
        \BoostMyShop\SupplierReturn\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\Supplier\Model\Config $supplierConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_productFactory = $productFactory;
        $this->_supplierReturnFactory = $supplierReturnFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_supplierConfig = $supplierConfig;
        $this->_resource = $resource;

        parent::__construct($context, $backendHelper, $data);

        $this->setPagerVisibility(false);
        $this->setMessageBlockVisibility(false);
        $this->setDefaultLimit(200);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('supplierReturnProductsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setTitle(__('Products'));
        $this->setUseAjax(true);
    }

    protected function getSupplierReturn()
    {
        return $this->_coreRegistry->registry('supplierreturn');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        //product collection
        $collection = $this->_productFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('image');
        $collection->addAttributeToSelect('thumbnail');
        if ($this->_supplierConfig->getBarcodeAttribute())
            $collection->addAttributeToSelect($this->_supplierConfig->getBarcodeAttribute());
        $collection->addFieldToFilter('type_id', array('in' => array('simple')));

        //link to return
        $connection  = $this->_resource->getConnection();
        $tableName   = $connection->getTableName('bms_supplier_return_product');
        $collection->getSelect()->join(
                    $tableName,
                    'e.entity_id = bsrp_product_id AND  bsrp_return_id ='.$this->getSupplierReturn()->getId(),
                    array('bsrp_notes','bsrp_qty','bsrp_product_id','bsrp_id')
                );
        $alreadyAddedProducts = $this->_productCollectionFactory->create()->getAlreadyAddedProductIds($this->getSupplierReturn()->getId());
        $collection->addFieldToFilter('entity_id', array('in' => $alreadyAddedProducts));

        //link to product / supplier
        $tableName   = $connection->getTableName('bms_supplier_product');
        $collection->getSelect()->joinLeft(
            $tableName,
            'sp_product_id = bsrp_product_id and sp_sup_id = '.$this->getSupplierReturn()->getbsr_supplier_id(),
            array('*')
        );

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('image', ['header' => __('Image'),'filter' => false, 'sortable' => false, 'type' => 'renderer', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Image']);

        if ($this->_supplierConfig->getBarcodeAttribute())
            $this->addColumn('barcode', ['header' => __('Barcode'), 'filter' => false, 'index' => $this->_supplierConfig->getBarcodeAttribute(), 'type' => 'text']);

        $this->addColumn('sku', ['header' => __('Sku'), 'filter' => false, 'index' => 'sku', 'type' => 'text', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\AddProducts\Renderer\Sku']);
        $this->addColumn('sp_sku', ['header' => __('Supplier Sku'), 'filter' => false, 'index' => 'sp_sku']);
        $this->addColumn('name', ['header' => __('Name'), 'filter' => false, 'index' => 'name', 'type' => 'text']);
        $this->addColumn('bsrp_qty', ['header' => __('Qty'), 'filter' => false, 'index' => 'bsrp_qty', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer\Qty', 'align' => 'center']);
        $this->addColumn('bsrp_notes', ['header' => __('Notes'), 'filter' => false, 'sortable' => false, 'index' => 'bsrp_notes', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer\Notes', 'align' => 'center']);
        $this->addColumn('bsrp_remove', ['header' => __('Remove'), 'filter' => false, 'sortable' => false, 'index' => 'bsrp_id', 'renderer' => '\BoostMyShop\SupplierReturn\Block\Adminhtml\Supplierreturn\Renderer\Remove', 'align' => 'center']);

        return parent::_prepareColumns();
    }

    public function getMainButtonsHtml()
    {
        //nothing, hide buttons
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/productsGrid', ['bsr_id' => $this->getSupplierReturn()->getId()]);
    }

}
