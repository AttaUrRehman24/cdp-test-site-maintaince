<?php

namespace BoostMyShop\Rma\Model\Rma;

use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;

class Refund
{
    protected $_creditMemoFactory;
    protected $_creditMemoManagement;
    protected $_creditmemoSender;
    protected $_transaction;

    public function __construct(
        \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory,
        \Magento\Sales\Api\CreditmemoManagementInterface $creditMemoManagement,
        CreditmemoSender $creditmemoSender,
        \Magento\Framework\DB\Transaction $transaction
    ){
        $this->_creditMemoManagement = $creditMemoManagement;
        $this->_transaction = $transaction;
        $this->_creditmemoSender = $creditmemoSender;
        $this->_creditMemoFactory = $creditmemoFactory;
    }

    public function process($rma, $data)
    {
        $order = $rma->getOrder();
        $invoice = false;
        foreach($order->getInvoiceCollection() as $item)
            $invoice = $item;

        if (!$invoice)
            $creditmemo = $this->_creditMemoFactory->createByOrder($order, $data);
        else
            $creditmemo = $this->_creditMemoFactory->createByInvoice($invoice, $data);

        if (!$creditmemo)
            throw new \Exception('Unable to create credit memo.');

        $creditmemo->addComment($data['comment_text'], false, false);

        $refundOffline = false;
        $creditmemo->addComment('Refund method : '.($refundOffline ? 'offline' : 'online'), false, false);

        $this->_creditMemoManagement->refund($creditmemo, $refundOffline);

        $this->_creditmemoSender->send($creditmemo);

        return $creditmemo;
    }

}