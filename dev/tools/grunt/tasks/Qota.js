/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
module.exports = function(grunt) {
    'use strict';

    var exec = require('child_process').execSync,
        spawn = require('child_process').spawn,
        log = grunt.log.write,
        ok = grunt.log.ok,
        error = grunt.log.error;

    grunt.registerTask('mtn', function() {
        var production,
            done = this.async();
        production = spawn('php', ['bin/magento', 'maintenance:enable']);
        production.stdout.on('data', function(data) {
            log(data);
        });

        log('Site is on Maintenance');
        production.on('close', done);
    });
    grunt.registerTask('production', function() {
        var production,
            done = this.async();
        production = spawn('php', ['bin/magento', 'deploy:mode:set', 'production']);
        production.stdout.on('data', function(data) {
            log(data);
        });

        log('Site is on Production');


        production.on('close', done);
    });
    grunt.registerTask('upgrade', function() {
        var production,
            done = this.async();
        production = spawn('php', ['bin/magento', 'setup:upgrade']);
        production.stdout.on('data', function(data) {
            log(data);
        });
        log('Site is on upgrade');


        production.on('close', done);
    });
    grunt.registerTask('cache', function() {
        var production,
            done = this.async();
        production = spawn('php', ['bin/magento', 'cache:flush']);
        production.stdout.on('data', function(data) {
            log(data);
        });
        log('Cache Flushed');


        production.on('close', done);
    });

    grunt.registerTask('pdeploy', function() {
        grunt.task.run('mtn', 'upgrade', 'production', 'cache');
    });

};