<?php

/**
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
define('DS', DIRECTORY_SEPARATOR);
define('BP', __DIR__);

require BP . DS . 'vendor' . DS . 'autoload.php';

use Wyomind\Elasticsearch\Autocomplete\Config;
use Wyomind\Elasticsearch\Autocomplete\Search;
use Wyomind\Elasticsearch\Autocomplete\Index\Type;
use Wyomind\Elasticsearch\Model\Client;
use Wyomind\Elasticsearch\Model\QueryBuilder;
use Elasticsearch\ClientBuilder;
use Psr\Log\NullLogger;
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';

header('Content-Type: application/json; charset=UTF-8');
header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
header('Pragma: no-cache');

$result = [];
$q = isset($_GET['q']) ? $_GET['q'] : '';
$found = false;

$enableDebugMode = 0;

if ('' !== $q) {
    $bootstrap = Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('frontend');
    try {
        $store = isset($_GET['store']) ? $_GET['store'] : '';
        
        try {
            $configHandler = new Config\JsonHandler($store);
            $config = new Config($configHandler->load());   
            
        } catch (\Exception $e) {
            $enableDebugMode = 1;
            throw $e;
        }

        if (!$config->getData()) {
            $enableDebugMode = 1;
            throw new \Exception('Could not find config for autocomplete');
        }

        $enableDebugMode = $config->getEnableDebugMode();

        $client = new Client(new ClientBuilder, $config, new NullLogger());
        $search = new Search($client, $config->getCompatibility());
        /** @var \Meridian\Base\Helper\Data $helperBase */
        $helperBase = $objectManager->create('Meridian\Base\Helper\Data');

        $typesConfig = $config->getTypes();
        foreach ($typesConfig as $code => $settings) {
            if ($code == "doyoumean") {
                continue;
            }
            try {
                if ($settings['enable_autocomplete'] == 1) {
                    $type = new Type($code, $settings);
                    $params = (new QueryBuilder($config))->build($q, $type);
                    $index = $client->getIndexAlias($store, $code);
                    list($docs, $suggests) = $search->query($index, $type, $params);
                    foreach ($docs as $key => $doc) {
                        if(isset($doc['type_id'])) {
                            if ($doc['type_id'] === 'configurable') {
                                $_product = $helperBase->getProductDetail($doc['id']);
                                $priceAmount = $helperBase->getMinPriceConfigurable($_product);
                                /*$currentPirceDisplay = $helperBase->currentPriceDisplayType();
                                if($currentPirceDisplay == 'INC_TAX') {
                                    $priceAmount = $helperBase->getPriceInclTax($_product, $priceAmount);
                                }*/
                                $doc['price'] = $priceAmount;
                                $doc['prices']['price'] = $priceAmount;
                                $doc['prices']['final_price'] = $priceAmount;
                                $doc['prices']['max_price'] = $priceAmount;
                                $doc['prices']['min_price'] = $priceAmount;
                                $doc['prices']['minimal_price'] = $priceAmount;
                                $docs[$key] = $doc;
                            }
                        }
                    }
                    $limit = $settings['autocomplete_limit'];
                    $result[$code] = [
                        'count' => count($docs),
                        'docs' => array_slice($docs, 0, $limit),
                        'enabled' => $settings['enable_autocomplete'] && ($code == "product" || ($code != "product" && $settings['enable']))
                    ];
                    if ($code == "product" && $typesConfig['doyoumean']['enable_autocomplete']) {
                        $result["suggests"] = [
                            'count' => count($suggests),
                            'docs' => $suggests,
                            'enabled' => $typesConfig['doyoumean']['enable_autocomplete']
                        ];
                    } else if (!$typesConfig['doyoumean']['enable_autocomplete']) {
                        $result["suggests"] = [
                            'count' => 0,
                            'docs' => [],
                            'enabled' => 0
                        ];
                    }
                }
            } catch (\Exception $e) {
                // Ignore results of current type if an exception is thrown when searching
                $result[$code] = [];
                if ($enableDebugMode) {
                    $result["error"][] = $e->getMessage();
                }
            }
        }

        $found = true;
    } catch (\Exception $e) {
        if ($enableDebugMode) {
            $result["error"][] = $e->getMessage();
        }
        $result["suggests"] = [ 'count' => 0];
        $result["product"] = [ 'count' => 0];
        $result["category"] = [ 'count' => 0];
        $result["cms"] = [ 'count' => 0];
        header('Fast-Autocomplete-error: ' . $e->getMessage());
    }
}

header('Fast-Autocomplete: ' . ($found ? 'HIT' : 'MISS'));

echo json_encode($result);
