#!/bin/bash
#rm -rf generated
rm -rf var/di/* var/generation/* var/cache/* var/page_cache/* var/view_preprocessed/* var/composer_home/cache/* pub/static/frontend/* pub/static/adminhtml/* pub/static/_requirejs/*
composer update
php bin/magento maintenance:enable 
php bin/magento setup:upgrade
#php bin/magento indexer:reindex
php bin/magento indexer:reindex design_config_grid
php bin/magento indexer:reindex customer_grid
php bin/magento indexer:reindex catalog_category_product
php bin/magento indexer:reindex catalog_product_category
php bin/magento indexer:reindex catalog_product_price
php bin/magento indexer:reindex catalog_product_attribute
php bin/magento indexer:reindex catalogrule_rule
php bin/magento indexer:reindex catalogrule_product
php bin/magento indexer:reindex cataloginventory_stock
php bin/magento indexer:reindex catalogsearch_fulltext
php bin/magento -f setup:static-content:deploy en_US
php bin/magento -f setup:static-content:deploy fr_FR
php bin/magento setup:di:compile
php bin/magento cache:flush
php bin/magento cache:clean
php bin/magento maintenance:disable
#php bin/magento ok:urlrewrites:regenerate
chmod -R 777 var/ app/etc/ pub/
